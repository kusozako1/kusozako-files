# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.overlay_item.button.Button import AlfaButton


class DeltaInvertSelection(AlfaButton):

    LABEL = _("Invert Selection")

    def _on_clicked(self, button):
        model, _ = self._enquiry("delta > selection model")
        for index in range(0, len(model)):
            if model.is_selected(index):
                model.unselect_item(index)
            else:
                model.select_item(index, False)
