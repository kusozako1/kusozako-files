# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity


class DeltaLabel(Gtk.Box, DeltaEntity):

    def bind(self, desktop_app_info):
        self._name.set_label(desktop_app_info.get_display_name())
        generic_name = desktop_app_info.get_description()
        if generic_name is not None:
            self._generic_name.set_label(generic_name)
        commandline = desktop_app_info.get_commandline()
        self._commandline.set_label(commandline)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(
            self,
            orientation=Gtk.Orientation.VERTICAL,
            vexpand=True,
            )
        self._name = Gtk.Label(xalign=0, yalign=0.8)
        self.append(self._name)
        self._generic_name = Gtk.Label(xalign=0)
        self._generic_name.add_css_class("dim-label")
        self.append(self._generic_name)
        self._commandline = Gtk.Label(xalign=0, yalign=0.2)
        self._commandline.add_css_class("dim-label")
        self.append(self._commandline)
        self._raise("delta > add to container", self)
