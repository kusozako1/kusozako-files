# (c) copyright 2022-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.const import MainWindowSignals as Signals
from kusozako1.widget.overlay_item.Spacer import DeltaSpacer
from kusozako_files.const import ExtraOverlayPages
from .content_area.ContentArea import DeltaContentArea


class AlfaSortOrder(Gtk.Box, DeltaEntity):

    TARGET_PAGE = ExtraOverlayPages.SORT_ORDER

    def _delta_call_loopback_add_extra_sort_types(self, user_data):
        # parent, proxy = user_data
        pass

    def _delta_info_target_page(self):
        return self.TARGET_PAGE

    def _delta_call_add_to_container(self, widget):
        self.append(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.HORIZONTAL)
        DeltaSpacer(self)
        DeltaContentArea(self)
        signal_param = self, self.TARGET_PAGE
        user_data = Signals.ADD_EXTRA_OVERLAY, signal_param
        self._raise("delta > main window signal", user_data)
