# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako1.const import MainWindowSignals


class DeltaShowExtraOverlay(DeltaEntity):

    def receive_transmission(self, user_data):
        signal, signal_param = user_data
        if signal != MainWindowSignals.SHOW_EXTRA_OVERLAY:
            return
        page_name, proxy = signal_param
        if page_name == self._target_page:
            self._raise("delta > reset", proxy)

    def __init__(self, parent):
        self._parent = parent
        self._target_page = self._enquiry("delta > target page")
        self._raise("delta > register main window signal object", self)
