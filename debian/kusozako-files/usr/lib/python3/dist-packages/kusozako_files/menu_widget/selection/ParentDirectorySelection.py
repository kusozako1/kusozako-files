# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.widget.overlay_item.Separator import DeltaSeparator
from .SelectAll import DeltaSelectAll
from .InvertSelection import DeltaInvertSelection
from .UnselectAll import DeltaUnselectAll


class EchoParentDirectorySelection:

    def __init__(self, parent):
        DeltaSeparator(parent)
        DeltaSelectAll(parent)
        DeltaInvertSelection(parent)
        DeltaUnselectAll(parent)
