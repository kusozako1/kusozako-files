# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .Image import DeltaImage
from .Label import DeltaLabel


class DeltaChildWidget(Gtk.Box, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.append(widget)

    def bind(self, desktop_app_info):
        self._image.bind(desktop_app_info)
        self._label.bind(desktop_app_info)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(
            self,
            orientation=Gtk.Orientation.HORIZONTAL,
            spacing=8,
            margin_top=4,
            margin_bottom=4,
            margin_start=4,
            margin_end=4,
            )
        self._image = DeltaImage(self)
        self._label = DeltaLabel(self)
        self.add_css_class("kusozako-primary-widget")
        self.set_size_request(-1, 64)
