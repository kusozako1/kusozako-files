# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.overlay_item.button.Button import AlfaButton


class DeltaUnselectAll(AlfaButton):

    LABEL = _("Unselect All")
    SHORTCUT = _("Esc")

    def _on_clicked(self, button):
        model, _ = self._enquiry("delta > selection model")
        model.unselect_all()
