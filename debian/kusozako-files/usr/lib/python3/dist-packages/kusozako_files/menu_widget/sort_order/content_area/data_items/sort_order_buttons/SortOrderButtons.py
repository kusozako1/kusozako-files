# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .Ascending import DeltaAscending
from .Descending import DeltaDescending


class EchoSortOrderButtons:

    @classmethod
    def new_for_proxy(cls, parent, proxy):
        DeltaAscending.new_for_proxy(parent, proxy)
        DeltaDescending.new_for_proxy(parent, proxy)
