# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako1.const import MainWindowSignals


class DeltaProxySetter(DeltaEntity):

    def receive_transmission(self, user_data):
        signal, signal_param = user_data
        if signal != MainWindowSignals.SHOW_EXTRA_PRIMARY_MENU_WITH_PARAM:
            return
        page_name, gfile = signal_param
        if page_name == self._target_page:
            self._raise("delta > proxy changed", gfile)

    def __init__(self, parent):
        self._parent = parent
        self._target_page = self._enquiry("delta > target page")
        self._raise("delta > register main window signal object", self)
