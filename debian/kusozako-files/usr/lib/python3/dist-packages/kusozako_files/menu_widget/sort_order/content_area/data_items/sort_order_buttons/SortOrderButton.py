# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.overlay_item.button.Button import AlfaButton
from kusozako_files.const import PageSettingsKeys as Keys


class AlfaSortOrderButton(AlfaButton):

    LABEL = "define label here."
    MATCH = "define matching value here."

    @classmethod
    def new_for_proxy(cls, parent, proxy):
        button = cls(parent)
        button.construct(proxy)

    def _on_clicked(self, button):
        self._proxy.change_settings(Keys.SORT_ORDER, self.MATCH)

    def _on_changed(self, user_data):
        key, value = user_data
        if key == Keys.SORT_ORDER:
            self._reset_icon(value)

    def _reset_icon(self, current_setting):
        if self.MATCH == current_setting:
            image = "radio-checked-symbolic"
        else:
            image = "radio-symbolic"
        self._start_image.set_from_icon_name(image)

    def construct(self, proxy):
        self._reset_icon(proxy[Keys.SORT_ORDER])
        self._proxy = proxy
        self._proxy.set_callback(self._on_changed)
