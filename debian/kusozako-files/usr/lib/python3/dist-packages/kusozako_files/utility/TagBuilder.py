# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Pango


class FoxtrotTagBuilder(Gtk.Box):

    @classmethod
    def new(cls, tag_type, icon_name, text, css):
        instance = cls()
        instance.construct(tag_type, icon_name, text, css)
        return instance

    def get_tag(self):
        return self._tag_type

    def construct(self, tag_type, icon_name, text, css):
        self._tag_type = tag_type
        image = Gtk.Image(
            icon_name=icon_name,
            margin_start=8,
            margin_top=2,
            margin_bottom=2
            )
        self.append(image)
        label = Gtk.Label(
            label=text,
            xalign=0,
            margin_end=8,
            ellipsize=Pango.EllipsizeMode.MIDDLE,
            )
        self.append(label)
        self.add_css_class(css)

    def __init__(self):
        Gtk.Box.__init__(
            self,
            orientation=Gtk.Orientation.HORIZONTAL,
            spacing=4,
            )
