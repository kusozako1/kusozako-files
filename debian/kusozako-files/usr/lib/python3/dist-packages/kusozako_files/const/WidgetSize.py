# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later


ACTION_BAR_HEIGHT = 48
STATUS_LABEL_HEIGHT = 32
LIST_VIEW_IMAGE_SIZE = 36
