# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

GREETER = "greeter"
CURRENT_DIRECTORY_LOST = "current-directory-lost"
FILE_NOT_FOUND = "file-not-found"
LIST_VIEW = "list-view"
ICON_VIEW = "icon-view"
