# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

FILES = "files"
SIDE_PANE = "side-pane"
OPEN_FILE_STANDALONE = "open-file-standalone"
