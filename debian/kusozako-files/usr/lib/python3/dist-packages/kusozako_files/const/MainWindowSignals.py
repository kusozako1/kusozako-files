# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

ADD_FILE_MANAGER = "add-file-manager"           # gfile for new directory
ENSURE_FILE_MANAGER = "ensure-file-manager"     # gfile for new directory
ENSURE_RECENT_FILES = "ensure-recent-files"     # None
ENSURE_TRASH_BIN = "ensure-trash-bin"           # None
SHOW_FILE_PROPERTY = "show-file-property"       # file_info
BACK_TO_BOOKMARK = "back-to-bookmark"           # None
XDG_USER_DIRS_CHANGED = "xdg-user-dirs-changed"     # remove_path, append_path
