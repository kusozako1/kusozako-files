# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

BINDED = "binded"                           # selected, file_info, gfile
SELECTION_CHANGED = "selection-changed"     # selected
PIXBUF_LOADED = "pixbuf-loaded"             # pixbuf
