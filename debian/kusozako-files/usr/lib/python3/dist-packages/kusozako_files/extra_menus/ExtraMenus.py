# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .FilesButton import DeltaFilesButton
from .files_page.FilesPage import DeltaFilesPage
from .side_pane_page.SidePanePage import DeltaSidePanePage
from .file_manager.FileManager import EchoFileManager
from .trash_bin.TrashBin import EchoTrashBin
from .recent.Recent import EchoRecent
from .stand_alone.StandAlone import EchoStandAlone


class EchoExtraMenus:

    def __init__(self, parent):
        DeltaFilesButton(parent)
        DeltaFilesPage(parent)
        DeltaSidePanePage(parent)
        EchoFileManager(parent)
        EchoTrashBin(parent)
        EchoRecent(parent)
        EchoStandAlone(parent)
