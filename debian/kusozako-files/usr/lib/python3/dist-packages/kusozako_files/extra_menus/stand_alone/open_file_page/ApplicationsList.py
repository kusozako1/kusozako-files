# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files.const import ExtraMenuPages
from kusozako_files.menu_widget.applications_list.ApplicationsList import (
    AlfaApplicationsList
    )


class DeltaApplicationsList(AlfaApplicationsList):

    TARGET_PAGE = ExtraMenuPages.OPEN_FILE_STANDALONE
