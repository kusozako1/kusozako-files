# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.overlay_item.sub_page.SubPage import AlfaSubPage
from kusozako_files.const import ExtraMenuPages
from .SidePaneButton import DeltaSidePaneButton


class DeltaFilesPage(AlfaSubPage):

    PAGE_NAME = ExtraMenuPages.FILES

    def _on_initialize(self):
        DeltaSidePaneButton(self)
