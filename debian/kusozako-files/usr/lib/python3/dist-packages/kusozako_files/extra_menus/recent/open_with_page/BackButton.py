# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.const import MainWindowSignals
from kusozako1.overlay_item.button.Button import AlfaButton
from kusozako_files.const import ExtraOverlayPages


class DeltaBackButton(AlfaButton):

    START_ICON = "go-previous-symbolic"
    LABEL = _("Back")

    def _on_clicked(self, button):
        back_to = ExtraOverlayPages.FILE_CONTEXT_MENU_RECENT
        user_data = MainWindowSignals.SHOW_EXTRA_PRIMARY_MENU, back_to
        self._raise("delta > main window signal", user_data)

    def _on_initialize(self):
        pass
