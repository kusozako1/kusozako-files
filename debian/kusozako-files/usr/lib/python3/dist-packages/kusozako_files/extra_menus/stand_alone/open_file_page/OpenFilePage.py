# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.const import MainWindowSignals
from kusozako1.overlay_item.Separator import DeltaSeparator
from kusozako_files.const import ExtraMenuPages
from kusozako_files.menu_widget.ExitButton import DeltaExitButton
from .ApplicationsList import DeltaApplicationsList


class DeltaOpenFilePage(Gtk.Box, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.append(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL)
        self.add_css_class("kusozako-osd")
        self.set_opacity(0.9)
        self.set_size_request(360, -1)
        DeltaExitButton(self)
        DeltaSeparator(self)
        DeltaApplicationsList(self)
        param = self, ExtraMenuPages.OPEN_FILE_STANDALONE
        user_data = MainWindowSignals.ADD_EXTRA_PRIMARY_MENU, param
        self._raise("delta > main window signal", user_data)
