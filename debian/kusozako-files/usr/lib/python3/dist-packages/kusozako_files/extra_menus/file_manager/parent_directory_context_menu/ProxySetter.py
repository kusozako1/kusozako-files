# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako1.const import MainWindowSignals
from kusozako_files.const import ExtraOverlayPages


class DeltaProxySetter(DeltaEntity):

    def receive_transmission(self, user_data):
        signal, signal_param = user_data
        if signal != MainWindowSignals.SHOW_EXTRA_PRIMARY_MENU_WITH_PARAM:
            return
        page_name, proxy = signal_param
        if page_name == ExtraOverlayPages.PARENT_DIRECTORY_CONTEXT_MENU:
            self._raise("delta > proxy changed", proxy)

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register main window signal object", self)
