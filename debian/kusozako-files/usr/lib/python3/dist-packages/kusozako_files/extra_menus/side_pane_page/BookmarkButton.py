# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files.const import SidePaneTypes
from .SidePaneButton import AlfaSidePaneButton


class DeltaBookmarkButton(AlfaSidePaneButton):

    LABEL = _("Bookmark")
    MATCH_VALUE = SidePaneTypes.BOOKMARK
