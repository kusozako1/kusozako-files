# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.overlay_item.CheckButton import AlfaCheckButton


class DeltaShowFilesButton(AlfaCheckButton):

    LABEL = _("Show Files")
    GROUP = "side_pane_tree_view"
    KEY = "show_files"
    MATCH_VALUE = True
