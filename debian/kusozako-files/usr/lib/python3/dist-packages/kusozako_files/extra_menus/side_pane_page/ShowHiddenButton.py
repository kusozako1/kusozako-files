# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.overlay_item.CheckButton import AlfaCheckButton


class DeltaShowHiddenButton(AlfaCheckButton):

    LABEL = _("Show Hidden")
    GROUP = "side_pane_tree_view"
    KEY = "show_hidden"
    MATCH_VALUE = True
