# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako1.Entity import DeltaEntity
from .Regular import DeltaRegular
from .Directory import DeltaDirectory
from .SymbolicLink import DeltaSymbolicLink


class DeltaCopy(DeltaEntity):

    @classmethod
    def get_default(cls):
        if "_default" not in dir(cls):
            cls._default = cls()
        return cls._default

    def _delta_call_operation_finished(self, user_data):
        pass
        """
        directory_gfile, file_infos = user_data
        if len(file_infos) == 0:
            print("Whole Operation Finished")
        else:
            self.copy_file_infos(directory_gfile, file_infos)
        """

    def _timeout(self, directory_gfile, file_infos):
        file_info = file_infos.pop()
        content_type = file_info.get_content_type()
        args = directory_gfile, file_info, file_infos
        if file_info.get_is_symlink():
            self._symbolic_link.copy_async(*args)
        elif content_type == "inode/directory":
            self._directory.copy_async(*args)
        else:
            self._regular.copy_async(*args)
        return GLib.SOURCE_REMOVE if not file_infos else GLib.SOURCE_CONTINUE

    def copy_file_infos(self, directory_gfile, file_infos):
        GLib.timeout_add(10, self._timeout, directory_gfile, file_infos)

    def __init__(self):
        self._parent = None
        self._regular = DeltaRegular(self)
        self._directory = DeltaDirectory(self)
        self._symbolic_link = DeltaSymbolicLink(self)
