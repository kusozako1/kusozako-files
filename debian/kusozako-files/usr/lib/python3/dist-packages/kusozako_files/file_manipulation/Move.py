# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib
from . import GFile


def _on_move_finished(source_gfile, task, user_data=None):
    successful = source_gfile.move_finish(task)
    if not successful:
        print("something goes wrong...")


def _get_destination_gfile(directory_gfile, source_gfile):
    if directory_gfile.equal(source_gfile.get_parent()):
        return None
    file_info = source_gfile.query_info("standard::copy-name", 0)
    copy_name = file_info.get_attribute_string("standard::copy-name")
    destination_gfile = directory_gfile.get_child(copy_name)
    return GFile.get_safe_destination_gfile(destination_gfile)


def move_gfiles(directory_gfile, source_gfiles):
    for source_gfile in source_gfiles:
        destination_gfile = _get_destination_gfile(
            directory_gfile,
            source_gfile
            )
        if destination_gfile is None:
            continue
        source_gfile.move_async(
            destination_gfile,
            Gio.FileCopyFlags.ALL_METADATA,     # copy flags
            GLib.PRIORITY_DEFAULT_IDLE,         # async priority
            None,                               # progress callback
            None,                               # progress callback data
            _on_move_finished,                  # callback on finish
            None,                               # data for callback on fnish
            )
