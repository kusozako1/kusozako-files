# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository.Gio import FileCopyFlags
from gi.repository import GLib
from .Operation import AlfaOperation


class DeltaRegular(AlfaOperation):

    def _on_finished(self, gfile, task, user_data):
        success = gfile.copy_finish(task)
        if not success:
            print("copy-failed", gfile.get_path())
        self._raise("delta > operation finished", user_data)

    def copy_async(self, directory_gfile, file_info, file_infos):
        source_gfile = file_info.get_attribute_object("standard::file")
        copy_name = file_info.get_attribute_string("standard::copy-name")
        unsafe_gfile = directory_gfile.get_child(copy_name)
        destination_gfile = self._get_safe_gfile(unsafe_gfile)
        user_data = directory_gfile, file_infos
        source_gfile.copy_async(
            destination_gfile,
            FileCopyFlags.ALL_METADATA | FileCopyFlags.NOFOLLOW_SYMLINKS,
            GLib.PRIORITY_DEFAULT_IDLE,
            None,                       # gio cancellable
            None,                       # progress callback
            ("",),                      # user data for progress call back
            self._on_finished,          # callback
            user_data,                  # user data for _on_copy_finished
            )
