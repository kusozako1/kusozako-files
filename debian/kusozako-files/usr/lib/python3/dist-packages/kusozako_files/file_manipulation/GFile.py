# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from kusozako1.util import SafePath


def get_safe_destination_gfile(unsafe_destination_gfile):
    if not unsafe_destination_gfile.query_exists(None):
        return unsafe_destination_gfile
    unsafe_path = unsafe_destination_gfile.get_path()
    safe_path = SafePath.get_path(unsafe_path)
    return Gio.File.new_for_path(safe_path)
