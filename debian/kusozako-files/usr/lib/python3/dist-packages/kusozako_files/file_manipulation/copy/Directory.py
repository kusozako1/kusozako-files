# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from .Operation import AlfaOperation


class DeltaDirectory(AlfaOperation):

    def _on_finished(self, gfile, task, user_data):
        success = gfile.wait_finish(task)
        if not success:
            print("copy-failed", gfile.get_path())
        self._raise("delta > operation finished", user_data)

    def copy_async(self, directory_gfile, file_info, file_infos):
        source_gfile = file_info.get_attribute_object("standard::file")
        source_path = source_gfile.get_path()
        copy_name = file_info.get_attribute_string("standard::copy-name")
        unsafe_gfile = directory_gfile.get_child(copy_name)
        destination_gfile = self._get_safe_gfile(unsafe_gfile)
        destination_path = destination_gfile.get_path()
        command = ["cp", "-r", source_path, destination_path]
        user_data = directory_gfile, file_infos
        subprocess = Gio.Subprocess.new(command, Gio.SubprocessFlags.NONE)
        subprocess.wait_async(None, self._on_finished, user_data)
