# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from .Operation import AlfaOperation


class DeltaSymbolicLink(AlfaOperation):

    def _on_finished(self, gfile, task, user_data):
        success = gfile.make_symbolic_link_finish(task)
        if not success:
            print("copy-failed", gfile.get_path())
        self._raise("delta > operation finished", user_data)

    def _get_absolute_symlink_target(self, file_info):
        source_gfile = file_info.get_attribute_object("standard::file")
        symlink_target = file_info.get_symlink_target()
        target_gfile = source_gfile.get_parent()
        for element in symlink_target.split("/"):
            if element == "..":
                target_gfile = target_gfile.get_parent()
            else:
                target_gfile = target_gfile.get_child(element)
        return target_gfile.get_path()

    def copy_async(self, directory_gfile, file_info, file_infos):
        copy_name = file_info.get_attribute_string("standard::copy-name")
        unsafe_gfile = directory_gfile.get_child(copy_name)
        destination_gfile = self._get_safe_gfile(unsafe_gfile)
        symlink_target = self._get_absolute_symlink_target(file_info)
        user_data = directory_gfile, file_infos
        destination_gfile.make_symbolic_link_async(
            symlink_target,                 # symlink target
            GLib.PRIORITY_DEFAULT_IDLE,     # priority
            None,                           # cancellable
            self._on_finished,              # callback
            user_data                       # calback data
            )
