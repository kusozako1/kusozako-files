# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import GObject
from kusozako_files.alfa.file_item.FileItem import AlfaFileItem

CONTEXT_MENU_CALLED = (GObject.SIGNAL_RUN_FIRST, GObject.TYPE_NONE, ())


class AlfaGridViewFileItem(AlfaFileItem, Gtk.AspectFrame):

    __gsignals__ = {
        "context-menu-called": CONTEXT_MENU_CALLED
        }

    def _on_setup(self):
        raise NotImplementedError()

    def _on_initialize(self):
        Gtk.AspectFrame.__init__(
            self,
            margin_start=4,
            margin_end=4,
            margin_top=4,
            margin_bottom=4,
            )
        self._on_setup()
