# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako_files.const import FileManagerSignals as Signals


class DeltaTogglePageSetting(DeltaEntity):

    def _toggle_setting(self, key):
        current_setting = self._enquiry("delta > page settings", key)
        user_data = key, not current_setting
        self._raise("delta > change setting", user_data)

    def receive_transmission(self, user_data):
        signal, key = user_data
        if signal == Signals.TOGGLE_PAGE_SETTING:
            self._toggle_setting(key)

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register file manager object", self)
