# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity


class AlfaSelectionModel(Gtk.MultiSelection, DeltaEntity):

    @classmethod
    def new(cls, parent, parent_model):
        instance = cls(parent)
        instance.construct(parent_model)
        return instance

    def get_selected_file_infos(self):
        selection = self.get_selection()
        return [
            self[selection.get_nth(nth)]
            for nth
            in range(0, selection.get_size())
            ]

    def get_valid_selection_size(self):
        raise NotImplementedError()

    def construct(self, parent_model):
        self.set_model(parent_model)

    def __init__(self, parent):
        self._parent = parent
        Gtk.MultiSelection.__init__(self)
