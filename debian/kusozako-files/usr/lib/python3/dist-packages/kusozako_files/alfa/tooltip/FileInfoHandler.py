# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Pango
from kusozako1.Entity import DeltaEntity
from kusozako1.util import HomeDirectory
from kusozako_files.const import FileItemSignals


class DeltaFileInfoHandler(DeltaEntity):

    def _get_filename(self):
        inscription = Gtk.Inscription(
            min_lines=3,
            wrap_mode=Pango.WrapMode.WORD_CHAR,
            vexpand=True,
            )
        inscription.set_size_request(256, -1)
        inscription.set_text(self._file_info.get_name())
        return inscription

    def _get_link_to(self, file_info):
        path = file_info.get_attribute_byte_string("standard::symlink-target")
        if path is None:
            return None
        parent_gfile = self._gfile.get_parent()
        target_gfile = parent_gfile.resolve_relative_path(path)
        target_path = target_gfile.get_path()
        shorten_path = HomeDirectory.shorten(target_path)
        text = "  link to : {}  ".format(shorten_path)
        label = Gtk.Label(label=text, xalign=0)
        label.add_css_class("kusozako-green-tag")
        return label

    def get_widget(self):
        if self._file_info is None:
            return None
        box = Gtk.Box(
            orientation=Gtk.Orientation.VERTICAL,
            spacing=8,
            margin_top=8,
            margin_bottom=8,
            )
        file_name = self._get_filename()
        box.append(file_name)
        link_to = self._get_link_to(self._file_info)
        if link_to is not None:
            box.append(link_to)
        return box

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal != FileItemSignals.BINDED:
            return
        _, file_info, gfile = param
        self._file_info = file_info
        self._gfile = gfile

    def __init__(self, parent):
        self._parent = parent
        self._file_info = None
        self._gfile = None
        self._raise("delta > register file item object", self)
