# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako_files.const import FileManagerSignals as Signals


class DeltaChangePageSetting(DeltaEntity):

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal == Signals.CHANGE_PAGE_SETTING:
            self._raise("delta > change setting", param)

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register file manager object", self)
