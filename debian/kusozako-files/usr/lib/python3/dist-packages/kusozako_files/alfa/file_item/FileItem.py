# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako1.Transmitter import FoxtrotTransmitter
from .model.Model import DeltaModel


class AlfaFileItem(DeltaEntity):

    def bind(self, selected, file_info):
        self._model.bind(selected, file_info)

    def unbind(self):
        self._model.unbind()

    def _delta_call_request_context_menu(self):
        self.emit("context-menu-called")

    def _delta_call_register_file_item_object(self, object_):
        self._transmitter.register_listener(object_)

    def _delta_call_file_item_signal(self, user_data):
        self._transmitter.transmit(user_data)

    def _delta_call_add_to_container(self, widget):
        self.set_child(widget)

    def _on_initialize(self):
        raise NotImplementedError()

    def __init__(self, selection_model):
        self._parent = None
        self._transmitter = FoxtrotTransmitter()
        self._model = DeltaModel.new(self, selection_model)
        self._on_initialize()
