# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako_files.const import FileItemSignals


class DeltaSelectionModelWatcher(DeltaEntity):

    @classmethod
    def new(cls, parent, selection_model):
        instance = cls(parent)
        instance.construct(selection_model)
        return instance

    def _is_changed(self, selection_model, position, length):
        if position+length > self._position >= position:
            return True, selection_model.is_selected(self._position)
        return False, None

    def _on_selection_changed(self, selection_model, position, length):
        changed, selected = self._is_changed(selection_model, position, length)
        if not changed:
            return
        user_data = FileItemSignals.SELECTION_CHANGED, selected
        self._raise("delta > file item signal", user_data)

    def bind(self, selected, file_info):
        self._position = file_info.get_attribute_int32("kusozako1::position")
        user_data = FileItemSignals.SELECTION_CHANGED, selected
        self._raise("delta > file item signal", user_data)

    def unbind(self):
        self._position = -1
        user_data = FileItemSignals.SELECTION_CHANGED, False
        self._raise("delta > file item signal", user_data)

    def construct(self, model):
        model.connect("selection-changed", self._on_selection_changed)

    def __init__(self, parent):
        self._parent = parent
        self._position = -1
