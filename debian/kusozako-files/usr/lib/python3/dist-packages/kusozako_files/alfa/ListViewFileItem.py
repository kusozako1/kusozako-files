# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import GObject
from kusozako_files.alfa.file_item.FileItem import AlfaFileItem
from .Tooltip import DeltaTooltip

CONTEXT_MENU_CALLED = (GObject.SIGNAL_RUN_FIRST, GObject.TYPE_NONE, ())


class AlfaListViewFileItem(AlfaFileItem, Gtk.Box):

    __gsignals__ = {
        "context-menu-called": CONTEXT_MENU_CALLED
        }

    def _on_released(self, gesture_click, *args):
        self.emit("context-menu-called")

    def _delta_call_add_to_container(self, widget):
        self.append(widget)

    def _delta_info_list_view_file_item(self):
        return self

    def _on_setup(self):
        raise NotImplementedError()

    def _on_initialize(self):
        Gtk.Box.__init__(
            self,
            orientation=Gtk.Orientation.HORIZONTAL,
            margin_top=4,
            margin_bottom=4,
            margin_start=4,
            margin_end=4,
            spacing=8,
            has_tooltip=True,
            )
        gesture_click = Gtk.GestureClick(button=3)
        gesture_click.connect("released", self._on_released)
        self.add_controller(gesture_click)
        DeltaTooltip(self)
        self._on_setup()
