# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Gdk
from kusozako1.Entity import DeltaEntity
from kusozako_files.const import FileItemSignals


class DeltaPixbufHandler(DeltaEntity):

    def get_widget(self):
        if self._pixbuf is None:
            return None
        texture = Gdk.Texture.new_for_pixbuf(self._pixbuf)
        picture = Gtk.Picture.new_for_paintable(texture)
        return picture

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal == FileItemSignals.PIXBUF_LOADED:
            self._pixbuf = param

    def __init__(self, parent):
        self._parent = parent
        self._pixbuf = None
        self._raise("delta > register file item object", self)
