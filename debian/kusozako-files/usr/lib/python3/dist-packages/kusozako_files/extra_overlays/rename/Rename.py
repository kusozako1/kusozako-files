# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.const import MainWindowSignals as Signals
from kusozako1.widget.overlay_item.VerticalSpacer import DeltaVerticalSpacer
from kusozako_files.const import ExtraOverlayPages as Pages
from .model.Model import DeltaModel
from .content_area.ContentArea import DeltaContentArea


class DeltaRename(Gtk.Box, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.append(widget)

    def _delta_call_change_name(self, name):
        self._model.change_name(name)

    def _delta_call_try_rename(self):
        self._model.try_rename()

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(
            self,
            orientation=Gtk.Orientation.VERTICAL,
            )
        self._model = DeltaModel(self)
        DeltaVerticalSpacer(self)
        DeltaContentArea(self)
        DeltaVerticalSpacer(self)
        signal_param = self, Pages.RENAME
        user_data = Signals.ADD_EXTRA_OVERLAY, signal_param
        self._raise("delta > main window signal", user_data)
