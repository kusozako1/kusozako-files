# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako1.const import MainWindowSignals
from kusozako_files.const import ExtraOverlayPages


class DeltaCurrentFile(DeltaEntity):

    def get_path(self):
        return self._gfile.get_path()

    def get_gfile(self):
        return self._gfile

    def receive_transmission(self, user_data):
        signal, signal_param = user_data
        if signal != MainWindowSignals.SHOW_EXTRA_OVERLAY:
            return
        page_name, gfile = signal_param
        if page_name != ExtraOverlayPages.CHANGE_VIDEO_THUMBNAIL:
            return
        self._gfile = gfile.dup()
        self._raise("delta > reload")

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register main window signal object", self)
