# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako1.const import MainWindowSignals as Signals
from kusozako_files.const import ExtraOverlayPages as Pages


class DeltaSourceFile(DeltaEntity):

    def get_child_gfile(self, basename):
        parent_gfile = self._gfile.get_parent()
        child_gfile = parent_gfile.get_child(basename)
        return child_gfile

    def rename(self, target_gfile):
        target_path = target_gfile.get_path()
        basename = target_gfile.get_basename()
        source_path = self._gfile.get_path()
        user_data = source_path, target_path
        self._raise("delta > try update xdg user dir", user_data)
        self._gfile.set_display_name(basename)

    def receive_transmission(self, user_data):
        signal, signal_param = user_data
        if signal != Signals.SHOW_EXTRA_PRIMARY_MENU_WITH_PARAM:
            return
        page_name, proxy = signal_param
        if page_name == Pages.FILE_CONTEXT_MENU:
            self._gfile = proxy.get_selected_gfile()

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register main window signal object", self)
