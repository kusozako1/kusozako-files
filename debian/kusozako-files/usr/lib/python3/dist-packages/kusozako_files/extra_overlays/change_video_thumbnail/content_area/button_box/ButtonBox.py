# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .CancelButton import DeltaCancelButton
from .ReloadButton import DeltaReloadButton


class DeltaButtonBox(Gtk.CenterBox, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self._inner_box.append(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.CenterBox.__init__(
            self,
            orientation=Gtk.Orientation.HORIZONTAL,
            margin_top=32,
            margin_bottom=32,
            )
        self._inner_box = Gtk.Box(
            orientation=Gtk.Orientation.HORIZONTAL,
            homogeneous=True,
            spacing=16,
            )
        DeltaCancelButton(self)
        DeltaReloadButton(self)
        self.set_center_widget(self._inner_box)
        self._raise("delta > add to container", self)
