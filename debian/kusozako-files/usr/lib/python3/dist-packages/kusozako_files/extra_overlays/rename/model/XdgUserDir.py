# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib
from kusozako1.Entity import DeltaEntity
from kusozako_files.const import MainWindowSignals

COMMAND_OPTION = {
    GLib.UserDirectory.DIRECTORY_DESKTOP: "DESKTOP",
    GLib.UserDirectory.DIRECTORY_DOCUMENTS: "DOCUMENTS",
    GLib.UserDirectory.DIRECTORY_DOWNLOAD: "DOWNLOAD",
    GLib.UserDirectory.DIRECTORY_MUSIC: "MUSIC",
    GLib.UserDirectory.DIRECTORY_PICTURES: "PICTURES",
    GLib.UserDirectory.DIRECTORY_PUBLIC_SHARE: "PUBLIC_SHARE",
    GLib.UserDirectory.DIRECTORY_TEMPLATES: "TEMPLATES",
    GLib.UserDirectory.DIRECTORY_VIDEOS: "VIDEOS",
}


class DeltaXdgUserDir(DeltaEntity):

    def _get_options(self, source_path):
        for key in range(0, GLib.UserDirectory.N_DIRECTORIES):
            path = GLib.get_user_special_dir(key)
            if path == source_path:
                yield COMMAND_OPTION[key]

    def _update(self, directory, source_path, target_path):
        command = ["xdg-user-dirs-update", "--set", directory, target_path]
        subprocess = Gio.Subprocess.new(command, Gio.SubprocessFlags.NONE)
        subprocess.wait()
        GLib.reload_user_special_dirs_cache()
        self._paths = source_path, target_path

    def queue_signal_hook(self):
        if self._paths is not None:
            user_data = MainWindowSignals.XDG_USER_DIRS_CHANGED, self._paths
            self._raise("delta > main window signal", user_data)

    def try_update(self, source_path, target_path):
        self._paths = None
        GLib.reload_user_special_dirs_cache()
        for option in self._get_options(source_path):
            self._update(option, source_path, target_path)

    def __init__(self, parent):
        self._parent = parent
        self._paths = None
