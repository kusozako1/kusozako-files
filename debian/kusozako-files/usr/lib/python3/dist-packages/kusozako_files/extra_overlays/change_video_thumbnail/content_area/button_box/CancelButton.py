# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.const import MainWindowSignals


class DeltaCancelButton(Gtk.Button, DeltaEntity):

    def _on_clicked(self, button):
        user_data = MainWindowSignals.CLOSE_OVERLAY, None
        self._raise("delta > main window signal", user_data)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(
            self,
            label=_("Cancel"),
            )
        self.connect("clicked", self._on_clicked)
        self.add_css_class("kusozako-primary-widget")
        self._raise("delta > add to container", self)
