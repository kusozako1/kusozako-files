# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako1.const import MainWindowSignals as Signals
from .SourceFile import DeltaSourceFile
from .XdgUserDir import DeltaXdgUserDir


class DeltaModel(DeltaEntity):

    def _get_child_gfile(self, basename):
        if "/" in basename:
            return None
        child_gfile = self._source_file.get_child_gfile(basename)
        if child_gfile.query_exists():
            return None
        return child_gfile

    def _delta_call_try_update_xdg_user_dir(self, user_data):
        source_path, target_path = user_data
        self._xdg_user_dir.try_update(source_path, target_path)

    def change_name(self, basename):
        self._child_gfile = self._get_child_gfile(basename)

    def try_rename(self):
        if self._child_gfile is None:
            return
        self._source_file.rename(self._child_gfile)
        self._xdg_user_dir.queue_signal_hook()
        user_data = Signals.CLOSE_OVERLAY, None
        self._raise("delta > main window signal", user_data)

    def __init__(self, parent):
        self._parent = parent
        self._child_gfile = None
        self._source_file = DeltaSourceFile(self)
        self._xdg_user_dir = DeltaXdgUserDir(self)
