# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Gdk


class DeltaDrawingArea(Gtk.DrawingArea):

    def _draw_func(self, drawing_area, cairo_context, width, height, pixbuf):
        x = (width-pixbuf.get_width())/2
        y = min(16, (height-pixbuf.get_height())/2)
        Gdk.cairo_set_source_pixbuf(cairo_context, pixbuf, x, y)
        cairo_context.paint()

    def bind(self, pixbuf):
        self.set_draw_func(self._draw_func, pixbuf)
        self.queue_draw()

    def __init__(self):
        self._pixbuf = None
        Gtk.DrawingArea.__init__(self)
        self.set_size_request(128, 128)
