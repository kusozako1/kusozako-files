# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .SortOrder import DeltaSortOrder
from .sort_order_recent.SortOrderRecent import DeltaSortOrderRecent
from .sort_order_trash_bin.SortOrderTrashBin import DeltaSortOrderTrashBin
from .rename.Rename import DeltaRename
from .make_directory.MakeDirectory import DeltaMakeDirectory
from .change_video_thumbnail.ChangeVideoThumbnail import (
    DeltaChangeVideoThumbnail
    )


class EchoExtraOverlays:

    def __init__(self, parent):
        DeltaSortOrder(parent)
        DeltaSortOrderRecent(parent)
        DeltaSortOrderTrashBin(parent)
        DeltaRename(parent)
        DeltaMakeDirectory(parent)
        DeltaChangeVideoThumbnail(parent)
