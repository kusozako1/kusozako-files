# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files.const import ExtraOverlayPages
from kusozako_files.menu_widget.sort_order.SortOrder import AlfaSortOrder


class DeltaSortOrder(AlfaSortOrder):

    TARGET_PAGE = ExtraOverlayPages.SORT_ORDER
