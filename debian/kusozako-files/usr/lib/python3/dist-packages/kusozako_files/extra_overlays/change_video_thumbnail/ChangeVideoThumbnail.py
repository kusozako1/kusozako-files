# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.const import MainWindowSignals
from kusozako_files.const import ExtraOverlayPages
from .model.Model import DeltaModel
from .content_area.ContentArea import DeltaContentArea


class DeltaChangeVideoThumbnail(Gtk.Box, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.append(widget)

    def _delta_call_loopback_model_ready(self, parent):
        DeltaContentArea(parent)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(
            self,
            orientation=Gtk.Orientation.VERTICAL,
            )
        DeltaModel(self)
        signal_param = self, ExtraOverlayPages.CHANGE_VIDEO_THUMBNAIL
        user_data = MainWindowSignals.ADD_EXTRA_OVERLAY, signal_param
        self._raise("delta > main window signal", user_data)
