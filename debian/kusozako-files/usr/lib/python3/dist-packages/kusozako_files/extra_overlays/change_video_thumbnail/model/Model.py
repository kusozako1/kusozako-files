# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import Gtk
from gi.repository import GdkPixbuf
from kusozako1.Entity import DeltaEntity
from .PixbufLoader import DeltaPixbufLoader
from .PixbufSaver import DeltaPixbufSaver
from .CurrentFile import DeltaCurrentFile


class DeltaModel(Gio.ListStore, DeltaEntity):

    def _delta_call_pixbuf_loaded(self, pixbuf):
        self.append(pixbuf)

    def _delta_info_model(self):
        return self._selection_model

    def _delta_info_current_path(self):
        return self._current_file.get_path()

    def _delta_info_current_gfile(self):
        return self._current_file.get_gfile()

    def _delta_call_change_thumbnail(self, pixbuf):
        self._pixbuf_saver.save(pixbuf)

    def _delta_call_reload(self):
        self.remove_all()
        self._pixbuf_loader.load_async()

    def __init__(self, parent):
        self._parent = parent
        self._pixbuf_loader = DeltaPixbufLoader(self)
        self._pixbuf_saver = DeltaPixbufSaver(self)
        Gio.ListStore.__init__(self, item_type=GdkPixbuf.Pixbuf)
        self._selection_model = Gtk.SingleSelection.new(self)
        self._current_file = DeltaCurrentFile(self)
        self._raise("delta > loopback model ready", self)
