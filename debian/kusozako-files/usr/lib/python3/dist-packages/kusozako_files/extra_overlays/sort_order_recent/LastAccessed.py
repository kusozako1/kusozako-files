# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files.const import SortType
from kusozako_files.menu_widget.SortTypeButton import AlfaSortTypeButton


class DeltaLastAccessed(AlfaSortTypeButton):

    LABEL = _("Last Accessed")
    MATCH = SortType.LAST_ACCESSED
