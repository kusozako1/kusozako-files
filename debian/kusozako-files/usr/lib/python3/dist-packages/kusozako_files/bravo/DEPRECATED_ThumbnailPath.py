# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib

NAMES = [GLib.get_user_cache_dir(), "kusozako1", "thumbnail", "128"]
DIRECTORY = GLib.build_filenamev(NAMES)
NEEDS_THUMBNAIL = (
    "image/png",
    "image/webp",
    "image/jpeg",
    "image/svg",
    "image/gif",
    "video/",
    "audio/mp4",
    "application/pdf"
    )


class BravoThumbnailPath:

    def _needs_thumbnail(self, mime):
        for check in NEEDS_THUMBNAIL:
            if mime.startswith(check):
                return True
        return False

    def _get_thumbnail_path(self, uri):
        uri_as_bytes = bytes(uri, encoding="utf-8")
        self._checksum.update(uri_as_bytes)
        md5_checksum = self._checksum.get_string()
        self._checksum.reset()
        basename = md5_checksum+".png"
        return GLib.build_filenamev([DIRECTORY, basename])

    def _set_thumbnail_attributes(self, gfile, file_info):
        if not self._needs_thumbnail(file_info.get_content_type()):
            return
        thumbnail_path = self._get_thumbnail_path(gfile.get_uri())
        file_info.set_attribute_string(
            "kusozako1::thumbnail-path",
            thumbnail_path
            )

    def __init__(self):
        self._checksum = GLib.Checksum.new(GLib.ChecksumType.MD5)
