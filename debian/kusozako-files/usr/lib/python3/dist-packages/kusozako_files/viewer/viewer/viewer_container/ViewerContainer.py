# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .VAdjustmentTop import DeltaVAdjustmentTop
from .VAdjustmentBottom import DeltaVAdjustmentBottom


class DeltaViewerContainer(Gtk.ScrolledWindow, DeltaEntity):

    def set_viewer(self, viewer):
        self.set_child(viewer)
        DeltaVAdjustmentTop(self)
        DeltaVAdjustmentBottom(self)

    def __init__(self, parent):
        self._parent = parent
        Gtk.ScrolledWindow.__init__(self, hexpand=True, vexpand=True)
        self.add_css_class("kusozako-surface")
