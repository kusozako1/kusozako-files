# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako_files.const import ViewerTypes
from kusozako_files.viewer.viewer.Viewer import AlfaViewer


class AlfaListView(AlfaViewer):

    __viewer_type__ = ViewerTypes.LIST_VIEW
    __css__ = "kusozako-list-view"

    def _get_header_factory(self):
        return None

    def _get_viewer(self, factory):
        list_view = Gtk.ListView(
            model=self._enquiry("delta > viewer model"),
            show_separators=True,
            single_click_activate=True,
            factory=factory,
            vexpand=True
            )
        header_factory = self._get_header_factory()
        if header_factory is not None:
            list_view.set_header_factory(header_factory)
        return list_view
