# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from .viewer_container.ViewerContainer import DeltaViewerContainer
from .watchers.Watchers import EchoWatchers
from .factory.Factory import DeltaFactory


class AlfaViewer(DeltaEntity):

    __viewer_type__ = "define viewer type"
    __css__ = "define css class"
    __context_menu_signal__ = "define context menu signal"

    def _delta_info_viewer(self):
        return self._viewer

    def _delta_call_add_controller(self, controller):
        self._viewer.add_controller(controller)

    def _context_menu(self, list_item_widget):
        user_data = self.__context_menu_signal__, None
        self._raise("delta > file manager signal", user_data)

    def _get_list_item_widget(self, selection_model):
        raise NotImplementedError()

    def _delta_info_list_item_widget(self):
        selection_model = self._enquiry("delta > selection model")
        list_item_widget = self._get_list_item_widget(selection_model)
        list_item_widget.connect("context-menu-called", self._context_menu)
        return list_item_widget

    def _get_viewer(self, factory):
        raise NotImplementedError()

    def _setup_controllers(self):
        pass

    def __init__(self, parent):
        self._parent = parent
        self._viewer_container = DeltaViewerContainer(self)
        factory = DeltaFactory(self)
        self._viewer = self._get_viewer(factory)
        self._viewer.__viewer_type__ = self.__viewer_type__
        self._setup_controllers()
        EchoWatchers(self)
        self._viewer.add_css_class(self.__css__)
        self._viewer_container.set_viewer(self._viewer)
        user_data = self._viewer_container, self.__viewer_type__
        self._raise("delta > add to stack", user_data)
