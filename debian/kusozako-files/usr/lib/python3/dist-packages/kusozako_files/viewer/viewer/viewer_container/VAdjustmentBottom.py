# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako_files.const import WidgetSize


class DeltaVAdjustmentBottom(DeltaEntity):

    def _on_value_changed(self, vadjustment):
        bottom_edge = vadjustment.props.value+vadjustment.props.page_size
        to_bottom = vadjustment.props.upper - bottom_edge
        margin = max(0, WidgetSize.ACTION_BAR_HEIGHT-to_bottom)
        viewer = self._enquiry("delta > viewer")
        viewer.set_margin_bottom(margin)

    def __init__(self, parent):
        self._parent = parent
        viewer = self._enquiry("delta > viewer")
        vadjustment = viewer.get_vadjustment()
        vadjustment.connect("value-changed", self._on_value_changed)
