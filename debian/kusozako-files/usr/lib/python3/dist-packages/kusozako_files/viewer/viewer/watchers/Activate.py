# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako_files.const import FileManagerSignals


class DeltaActivate(DeltaEntity):

    def _on_event(self, grid_view, index):
        user_data = FileManagerSignals.TOGGLE_INDEX, index
        self._raise("delta > file manager signal", user_data)

    def __init__(self, parent):
        self._parent = parent
        grid_view = self._enquiry("delta > viewer")
        grid_view.connect("activate", self._on_event)
