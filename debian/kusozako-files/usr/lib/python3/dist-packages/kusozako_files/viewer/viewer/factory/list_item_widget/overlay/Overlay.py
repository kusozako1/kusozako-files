# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from .StatusLabel import FoxtrotStatusLabel
from .NameLabel import FoxtrotNameLabel
from .DrawingArea import FoxtrotDrawingArea


class FoxtrotOverlay(Gtk.Overlay):

    def _on_get_child_position(self, overlay, widget, rectangle):
        if isinstance(widget, FoxtrotStatusLabel):
            rectangle.y = 0
            rectangle.x = 0
            rectangle.height = 32
            return True, rectangle

    def set_selected(self, selected):
        self._status_label.set_selected(selected)

    def bind(self, file_info):
        self._status_label.bind(file_info)
        self._drawing_area.bind(file_info)
        self._name_label.bind(file_info)

    def __init__(self):
        Gtk.Overlay.__init__(self, overflow=Gtk.Overflow.HIDDEN)
        self.add_css_class("card")
        self._drawing_area = FoxtrotDrawingArea()
        self.set_child(self._drawing_area)
        self._status_label = FoxtrotStatusLabel()
        self.add_overlay(self._status_label)
        self._name_label = FoxtrotNameLabel()
        self.add_overlay(self._name_label)
        self.connect("get-child-position", self._on_get_child_position)
