# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako1.Entity import DeltaEntity


class DeltaVerticalAdjustment(DeltaEntity):

    def _adjust_top(self, list_item_widget, viewer, rect):
        widget_top = rect.get_y()*-1
        if widget_top >= 52:
            return
        vadjustment = viewer.get_vadjustment()
        vadjustment.props.value -= (52 - widget_top)

    def _adjust_bottom(self, list_item_widget, viewer, rect):
        # widget_margin = list_item_widget.get_margin_bottom()
        widget_bottom = rect.get_y()*-1 + list_item_widget.get_height()
        bottom_margin = viewer.get_height() - widget_bottom
        if bottom_margin >= 52:
            return
        vadjustment = viewer.get_vadjustment()
        vadjustment.props.value += (52 - bottom_margin)

    def _adjust(self, model, list_item):
        list_item_widget = list_item.get_child()
        if list_item_widget is None:
            print("no widget ?")
            return
        viewer = self._enquiry("delta > viewer")
        success, rect = viewer.compute_bounds(list_item_widget)
        if not success:
            return
        self._adjust_top(list_item_widget, viewer, rect)
        self._adjust_bottom(list_item_widget, viewer, rect)

    def _timeout(self, datetime, model, list_item):
        if 100000 > datetime.difference(self._datetime):
            return
        self._datetime = datetime
        self._adjust(model, list_item)

    def _on_selected(self, model, param_spec, list_item):
        if not list_item.get_selected():
            return
        datetime = GLib.DateTime.new_now_local()
        GLib.timeout_add(10, self._timeout, datetime, model, list_item)

    def connect(self, list_item):
        viewer_model = self._enquiry("delta > viewer model")
        viewer_model.connect("notify::selected", self._on_selected, list_item)

    def __init__(self, parent):
        self._parent = parent
        self._datetime = GLib.DateTime.new_now_local()
