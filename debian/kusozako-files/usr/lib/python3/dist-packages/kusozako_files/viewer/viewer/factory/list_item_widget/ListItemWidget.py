# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import GObject
from .SelectionModelWatcher import FoxtrotSelectionModelWatcher
from .overlay.Overlay import FoxtrotOverlay

CONTEXT_MENU_CALLED = (GObject.SIGNAL_RUN_FIRST, GObject.TYPE_NONE, ())


class FoxtrotListItemWidget(Gtk.AspectFrame):

    __gsignals__ = {
        "context-menu-called": CONTEXT_MENU_CALLED
        }

    def bind(self, selected, file_info):
        gfile = file_info.get_attribute_object("standard::file")
        if not gfile.query_exists():
            return
        self._watcher.bind(selected, file_info)
        self._overlay.bind(file_info)

    def _on_released(self, gesture_click, *args):
        self.emit("context-menu-called")

    def _on_selection_changed(self, watcher, selected):
        self._overlay.set_selected(selected)

    def __init__(self, selection_model):
        Gtk.AspectFrame.__init__(
            self,
            margin_start=4,
            margin_end=4,
            margin_top=4,
            margin_bottom=4,
            )
        self._watcher = FoxtrotSelectionModelWatcher(selection_model)
        self._watcher.connect("selection-changed", self._on_selection_changed)
        self._overlay = FoxtrotOverlay()
        gesture_click = Gtk.GestureClick(button=3)
        gesture_click.connect("released", self._on_released)
        self._overlay.add_controller(gesture_click)
        self.set_child(self._overlay)
