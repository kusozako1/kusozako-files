# (c) copyright 2022-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako1.main_loop.MainLoop import AlfaMainLoop
from . import APPLICATION_DATA
from .DefaultSettings import DeltaDefaultSettigs
from .extra_menus.ExtraMenus import EchoExtraMenus
from .extra_overlays.ExtraOverlays import EchoExtraOverlays
from .paned.Paned import DeltaPaned


class DeltaMainLoop(AlfaMainLoop):

    def _delta_call_loopback_application_window_ready(self, parent):
        DeltaDefaultSettigs(parent)
        DeltaPaned(parent)
        EchoExtraMenus(parent)
        EchoExtraOverlays(parent)

    def _delta_info_resource_directory(self):
        directory = GLib.path_get_dirname(__file__)
        path = GLib.build_filenamev([directory, "resources"])
        return path

    def _delta_info_data(self, key):
        return APPLICATION_DATA.get(key, None)
