# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .toggle_show_hidden.ToggleShowHidden import DeltaToggleShowHidden
from .icon_view_button.IconViewButton import DeltaIconViewButton
from .list_view_button.ListViewButton import DeltaListViewButton
from .sort_order.SortOrder import DeltaSortOrder


class EchoViewButtons:

    def __init__(self, parent):
        DeltaIconViewButton(parent)
        DeltaListViewButton(parent)
        DeltaToggleShowHidden(parent)
        DeltaSortOrder(parent)
