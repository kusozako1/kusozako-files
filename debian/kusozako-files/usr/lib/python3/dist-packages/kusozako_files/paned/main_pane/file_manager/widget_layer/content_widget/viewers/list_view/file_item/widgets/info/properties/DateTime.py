# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.util.ReadableTime import FoxtrotReadableTime
from .Label import AlfaLabel


class DeltaDateTime(AlfaLabel):

    def _bind(self, file_info):
        date_time = file_info.get_modification_date_time()
        local_date_time = date_time.to_local()
        readable_time = self._readable_time.get_label(local_date_time)
        self.set_label(readable_time)

    def _on_initialize(self):
        self._readable_time = FoxtrotReadableTime.get_default()
