# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako_files.const import FileManagerSignals
from kusozako_files.const import PageSettingsKeys


class DeltaVisibleChildName(DeltaEntity):

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal != FileManagerSignals.PAGE_SETTINGS_CHANGED:
            return
        key, visible_name = param
        if key != PageSettingsKeys.VIEWER_TYPE:
            return
        self._raise("delta > switch stack to", visible_name)

    def __init__(self, parent):
        self._parent = parent
        query = "view", PageSettingsKeys.VIEWER_TYPE
        visible_name = self._enquiry("delta > settings", query)
        self._raise("delta > switch stack to", visible_name)
        self._raise("delta > register file manager object", self)
