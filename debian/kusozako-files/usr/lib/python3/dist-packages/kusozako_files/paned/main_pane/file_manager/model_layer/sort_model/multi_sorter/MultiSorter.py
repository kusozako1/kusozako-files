# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako_files.const import FileManagerSignals
from .CollateKey import DeltaCollateKey
from .LastModified import DeltaLastModified
from .MimeType import DeltaMimeType


class DeltaMultiSorter(Gtk.MultiSorter, DeltaEntity):

    def _delta_call_append(self, sorter):
        self.append(sorter)

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal != FileManagerSignals.PAGE_SETTINGS_CHANGED:
            return
        param_type, order = param
        if param_type in ("sort-type", "sort-order"):
            self.changed(Gtk.SorterChange.INVERTED)

    def __init__(self, parent):
        self._parent = parent
        Gtk.MultiSorter.__init__(self)
        DeltaCollateKey(self)
        DeltaLastModified(self)
        DeltaMimeType(self)
        self._raise("delta > register file manager object", self)
