# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .Label import AlfaLabel


class DeltaType(AlfaLabel):

    def _bind(self, file_info):
        content_type = file_info.get_content_type()
        self.set_label(content_type)
