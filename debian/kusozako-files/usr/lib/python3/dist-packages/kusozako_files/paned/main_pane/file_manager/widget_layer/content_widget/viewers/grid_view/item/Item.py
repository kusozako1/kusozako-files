# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files.alfa.GridViewFileItem import AlfaGridViewFileItem
from .overlay.Overlay import DeltaOverlay


class TangoItem(AlfaGridViewFileItem):

    def _on_setup(self):
        DeltaOverlay(self)
