# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk


class FoxtrotHeaderFactory(Gtk.SignalListItemFactory):

    def _on_setup(self, factory, list_header):
        label = Gtk.Label()
        label.add_css_class("heading")
        list_header.set_child(label)

    def _get_header_label_text(self, header_item, n_items):
        content_type = header_item.get_content_type()
        if content_type == "inode/symlink":
            return _("BROKEN SYMLINK : {} items").format(n_items)
        elif content_type == "inode/directory":
            return _("DIRECTORY : {} items").format(n_items)
        return _("FILE : {} items").format(n_items)

    def _on_bind(self, factory, list_header):
        header_item = list_header.get_item()
        if header_item is None:
            return
        n_items = list_header.get_n_items()
        text = self._get_header_label_text(header_item, n_items)
        label = list_header.get_child()
        label.set_label(text)

    def __init__(self):
        Gtk.SignalListItemFactory.__init__(self)
        self.connect("setup", self._on_setup)
        self.connect("bind", self._on_bind)
