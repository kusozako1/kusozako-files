# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files.alfa.SelectionModel import AlfaSelectionModel


class DeltaSelectionModel(AlfaSelectionModel):

    def get_valid_selection_size(self):
        current_gfile = self._enquiry("delta > current gfile")
        if current_gfile is None:
            return 0
        uri_scheme = current_gfile.get_uri_scheme()
        if uri_scheme != "trash":
            return 0
        selection = self.get_selection()
        return selection.get_size()
