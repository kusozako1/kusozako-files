# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Pango
from kusozako1.Entity import DeltaEntity
from kusozako_files.const import FileManagerSignals


class DeltaLabel(Gtk.Label, DeltaEntity):

    def _on_selection_changed(self, model, position, n_items):
        selection = model.get_selection()
        size = selection.get_size()
        if size == 0:
            directory_model = self._enquiry("delta > directory model")
            n_items = directory_model.get_n_items()
            label = _("{} items found.".format(n_items))
        else:
            label = _("{} items selected.".format(size))
        self.set_label(label)

    def receive_transmission(self, user_data):
        signal, n_items = user_data
        if signal == FileManagerSignals.LOAD_FINISHED:
            label = _("{} items found.".format(n_items))
            self.set_label(label)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(
            self,
            hexpand=True,
            ellipsize=Pango.EllipsizeMode.END
            )
        self._raise("delta > add to container", self)
        model = self._enquiry("delta > selection model")
        model.connect("selection-changed", self._on_selection_changed)
        self._raise("delta > register file manager object", self)
