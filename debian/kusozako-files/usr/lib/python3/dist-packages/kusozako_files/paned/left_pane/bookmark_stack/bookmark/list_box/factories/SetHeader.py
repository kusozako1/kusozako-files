# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity


class DeltaSetHeader(DeltaEntity):

    def _header_func(self, bravo, alfa, user_data=None):
        if alfa is None:
            return
        alfa_type = alfa.get_child().get_type()
        bravo_type = bravo.get_child().get_type()
        if alfa_type == bravo_type:
            return
        separator = Gtk.Separator(
            margin_start=8,
            margin_end=8,
            margin_top=8,
            margin_bottom=8,
            )
        bravo.set_header(separator)

    def __init__(self, parent):
        self._parent = parent
        list_box = self._enquiry("delta > list box")
        list_box.set_header_func(self._header_func, None)
