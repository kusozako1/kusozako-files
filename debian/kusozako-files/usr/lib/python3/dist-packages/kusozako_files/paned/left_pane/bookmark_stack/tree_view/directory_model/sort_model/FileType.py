# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity


class DeltaFileType(Gtk.CustomSorter, DeltaEntity):

    def _sort_func(self, alfa, bravo, user_data=None):
        alfa_type = alfa.get_file_type()
        bravo_type = bravo.get_file_type()
        return bravo_type - alfa_type

    def __init__(self, parent):
        self._parent = parent
        Gtk.CustomSorter.__init__(self)
        self.set_sort_func(self._sort_func)
        self._raise("delta > add sorter", self)
