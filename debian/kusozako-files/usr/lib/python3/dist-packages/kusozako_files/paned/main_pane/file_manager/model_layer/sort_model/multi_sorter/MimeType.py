# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files.const import SortType
from kusozako_files.alfa.Sorter import AlfaSorter


class DeltaMimeType(AlfaSorter):

    SORT_TYPE = SortType.MIME_TYPE

    def _get_compared(self, alfa, bravo):
        result = alfa.get_content_type() > bravo.get_content_type()
        return 1 if result else -1
