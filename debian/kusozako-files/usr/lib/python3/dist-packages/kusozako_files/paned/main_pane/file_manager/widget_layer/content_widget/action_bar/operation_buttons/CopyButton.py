# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files.const import FileManagerSignals
from .OperationButton import AlfaOperationButton


class DeltaCopyButton(AlfaOperationButton):

    __icon_name__ = "edit-copy-symbolic"
    __tooltip_text__ = _("Copy Files to...")
    __file_namager_signal__ = FileManagerSignals.COPY_SELECTION

    def _get_sensitive(self, selection_model):
        for file_info in selection_model.get_selected_file_infos():
            if file_info.get_attribute_boolean("access::can-read"):
                return True
        return False
