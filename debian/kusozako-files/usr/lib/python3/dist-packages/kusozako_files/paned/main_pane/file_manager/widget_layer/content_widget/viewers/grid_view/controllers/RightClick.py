# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako_files.const import FileManagerSignals


class DeltaRightClick(Gtk.GestureClick, DeltaEntity):

    def _on_event(self, gesture, n_press, x, y):
        user_data = FileManagerSignals.CALL_DIRECTORY_CONTEXT_MENU, None
        self._raise("delta > file manager signal", user_data)

    def __init__(self, parent):
        self._parent = parent
        Gtk.GestureClick.__init__(self, button=3)
        self.connect("released", self._on_event)
        self._raise("delta > add controller", self)
