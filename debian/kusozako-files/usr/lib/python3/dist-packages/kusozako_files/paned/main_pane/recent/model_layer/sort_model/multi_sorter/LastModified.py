# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files.const import SortType
from kusozako_files.alfa.Sorter import AlfaSorter


class DeltaLastModified(AlfaSorter):

    SORT_TYPE = SortType.LAST_MODIFIED

    def _get_compared(self, alfa, bravo):
        # alfa_time = alfa.get_modification_date_time().to_unix()
        # bravo_time = bravo.get_modification_date_time().to_unix()
        alfa_time = alfa.get_access_date_time().to_unix()
        bravo_time = bravo.get_access_date_time().to_unix()
        result = alfa_time-bravo_time
        return result * -1
