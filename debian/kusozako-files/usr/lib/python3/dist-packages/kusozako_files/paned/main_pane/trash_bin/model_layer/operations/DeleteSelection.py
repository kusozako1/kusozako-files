# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib
from kusozako1.const import MainWindowSignals
from kusozako_files.const import FileManagerSignals
from kusozako_files.alfa.Observer import AlfaObserver

PRIORITY = GLib.PRIORITY_DEFAULT


class DeltaDeleteSelection(AlfaObserver):

    REGISTRATION = "delta > register file manager object"
    SIGNAL = FileManagerSignals.DELETE_SELECTION

    def _wait(self, subprocess, task, file_infos):
        success = subprocess.wait_finish(task)
        if not success:
            print("can't delete.")
        self._delete(file_infos)

    def _delete(self, file_infos):
        if not file_infos:
            print("finished ?")
            return
        file_info = file_infos.pop()
        target_uri = file_info.get_attribute_string("standard::target-uri")
        gfile = Gio.File.new_for_uri(target_uri)
        target_path = gfile.get_path()
        command = ["rm", "-r", target_path]
        subprocess = Gio.Subprocess.new(command, Gio.SubprocessFlags.NONE)
        subprocess.wait_async(None, self._wait, file_infos)

    def _on_confirmed(self):
        user_data = MainWindowSignals.CLOSE_OVERLAY, None
        self._raise("delta > main window signal", user_data)
        model = self._enquiry("delta > selection model")
        files_infos = model.get_selected_file_infos()
        self._delete(files_infos)
        model.unselect_all()

    def _on_received(self, param=None):
        user_data = MainWindowSignals.SHOW_DIALOG, self._dialog_model
        self._raise("delta > main window signal", user_data)

    def _on_initialize(self):
        buttons = [
            (_("Cancel"), None),
            (_("Remove"), self._on_confirmed)
            ]
        self._dialog_model = {
            "heading": _("Delete Selected Files ?"),
            "description": _("You can't undo this operation."),
            "buttons": buttons
            }
