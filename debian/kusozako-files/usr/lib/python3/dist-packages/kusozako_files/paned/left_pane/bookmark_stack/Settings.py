# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity


class DeltaSettings(DeltaEntity):

    def receive_transmission(self, user_data):
        group, key, value = user_data
        if group != "view" or key != "side_pane_type":
            return
        self._raise("delta > switch stack to", value)

    def __init__(self, parent):
        self._parent = parent
        query = "view", "side_pane_type", "bookmark"
        default = self._enquiry("delta > settings", query)
        self._raise("delta > switch stack to", default)
        self._raise("delta > register settings object", self)
