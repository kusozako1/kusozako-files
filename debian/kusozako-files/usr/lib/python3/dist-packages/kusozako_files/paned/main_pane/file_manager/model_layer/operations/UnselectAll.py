# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files.const import FileManagerSignals
from kusozako_files.alfa.Observer import AlfaObserver


class DeltaUnselectAll(AlfaObserver):

    REGISTRATION = "delta > register file manager object"
    SIGNAL = FileManagerSignals.UNSELECT_ALL

    def _on_received(self, param=None):
        model = self._enquiry("delta > selection model")
        model.unselect_all()
