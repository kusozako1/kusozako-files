# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import Poppler


class FoxtrotModel(Gio.ListStore):

    def _get_uri(self, file_info):
        gfile = file_info.get_attribute_object("standard::file")
        if gfile.get_uri_scheme() == "file":
            return gfile.get_uri()
        target_uri = file_info.get_attribute_string("standard::target-uri")
        gfile = Gio.new_for_uri(target_uri)
        return gfile.get_uri()

    def set_file_info(self, file_info):
        self.remove_all()
        uri = self._get_uri(file_info)
        document = Poppler.Document.new_from_file(uri, None)
        for page_index in range(0, min(5, document.get_n_pages())):
            page = document.get_page(page_index)
            self.append(page)

    def __init__(self):
        Gio.ListStore.__init__(self, item_type=Poppler.Page)
