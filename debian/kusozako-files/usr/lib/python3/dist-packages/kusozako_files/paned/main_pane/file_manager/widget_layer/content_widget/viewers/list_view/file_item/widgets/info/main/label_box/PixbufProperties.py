# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Pango
from kusozako1.const import PixbufOptionKeys
from kusozako1.Entity import DeltaEntity


class DeltaPixbufProperties(DeltaEntity):

    def _size(self, options):
        width = options.get(PixbufOptionKeys.WIDTH, None)
        height = options.get(PixbufOptionKeys.HEIGHT, None)
        if width is None or height is None:
            return
        label = "Size : {}x{}".format(width, height)
        self._add_label(label)

    def _pages(self, options):
        pages = options.get(PixbufOptionKeys.PAGES, None)
        if pages is None:
            return
        label = "Pages : {}".format(pages)
        self._add_label(label)

    def _get_readable(self, total_seconds):
        total_seconds = total_seconds
        hours = total_seconds // 3600
        minutes = total_seconds % 3600 // 60
        seconds = total_seconds % 60
        if hours == 0:
            return "Duration : {}:{:02}".format(minutes, seconds)
        return "Duration : {}:{:02}:{:02}".format(hours, minutes, seconds)

    def _length(self, pixbuf_options):
        length = pixbuf_options.get(PixbufOptionKeys.LENGTH, None)
        if length is None:
            return
        total_seconds = int(float(length)/1000)
        readable = self._get_readable(total_seconds)
        self._add_label(readable)

    def _add_label(self, label):
        label = Gtk.Label(
            label="  "+label+"  ",
            ellipsize=Pango.EllipsizeMode.MIDDLE,
            margin_top=2,
            margin_bottom=2,
            )
        label.add_css_class("kusozako-blue-tag")
        self._raise("delta > add to container", label)

    def set_pixbuf(self, options):
        self._length(options)
        self._size(options)
        self._pages(options)

    def __init__(self, parent):
        self._parent = parent
