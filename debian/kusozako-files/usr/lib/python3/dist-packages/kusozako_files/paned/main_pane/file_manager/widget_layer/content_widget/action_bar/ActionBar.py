# (c) copyright 2023-2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako_files.const import WidgetSize
from .UnselectAll import DeltaUnselectAll
from .Label import DeltaLabel
from .operation_buttons.OperationButtons import EchoOperationButtons


class DeltaActionBar(Gtk.Box, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.append(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.HORIZONTAL)
        self.set_size_request(-1, WidgetSize.ACTION_BAR_HEIGHT)
        self.set_opacity(0.9)
        self.add_css_class("kusozako-primary-surface")
        DeltaUnselectAll(self)
        DeltaLabel(self)
        EchoOperationButtons(self)
        self._raise("delta > add overlay", self)
