# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .ShowHidden import DeltaShowHidden
from .DirectoryOnly import DeltaDirectoryOnly


class DeltaFilterModel(Gtk.FilterListModel, DeltaEntity):

    @classmethod
    def new_for_model(cls, parent, model):
        instance = cls(parent)
        instance.construct(model)
        return instance

    def _delta_call_add_filter(self, filter_):
        self._filter.append(filter_)

    def construct(self, model):
        self.set_model(model)

    def __init__(self, parent):
        self._parent = parent
        self._filter = Gtk.EveryFilter.new()
        DeltaShowHidden(self)
        DeltaDirectoryOnly(self)
        Gtk.FilterListModel.__init__(self, filter=self._filter)
