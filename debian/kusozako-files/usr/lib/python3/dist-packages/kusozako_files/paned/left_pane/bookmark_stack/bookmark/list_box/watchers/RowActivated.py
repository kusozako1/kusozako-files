# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako_files.const import MainWindowSignals


class DeltaRowActivated(DeltaEntity):

    def _on_row_activated(self, list_box, list_box_row):
        bookmark_item = list_box_row.get_child()
        gfile = bookmark_item.get_gio_file()
        user_data = MainWindowSignals.ADD_FILE_MANAGER, gfile
        self._raise("delta > main window signal", user_data)

    def __init__(self, parent):
        self._parent = parent
        list_box = self._enquiry("delta > list box")
        list_box.connect("row-activated", self._on_row_activated)
