# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .VisibleChildName import DeltaVisibleChildName
from .grid_view.GridView import DeltaGridView
from .list_view.ListView import DeltaListView


class DeltaViewers(Gtk.Stack, DeltaEntity):

    def _delta_call_add_to_stack(self, user_data):
        widget, name = user_data
        self.add_named(widget, name)

    def _delta_call_switch_stack_to(self, visible_name):
        self.set_visible_child_name(visible_name)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Stack.__init__(
            self,
            transition_type=Gtk.StackTransitionType.SLIDE_LEFT_RIGHT,
            vexpand=True,
            )
        DeltaGridView(self)
        DeltaListView(self)
        DeltaVisibleChildName(self)
        self._raise("delta > add to container", self)
