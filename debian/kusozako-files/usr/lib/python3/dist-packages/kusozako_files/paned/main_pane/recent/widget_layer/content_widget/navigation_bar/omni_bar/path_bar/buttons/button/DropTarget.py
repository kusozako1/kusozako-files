# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Gdk
from kusozako1.Entity import DeltaEntity
from kusozako_files.bravo.GetChildAtPosition import BravoGetChildAtPosition
from kusozako_files.file_manipulation import Move


class DeltaDropTarget(DeltaEntity, BravoGetChildAtPosition):

    def _try_move_files(self, directory_gfile, drop):
        source_gfiles = drop.get_files()
        if source_gfiles:
            Move.move_gfiles(directory_gfile, source_gfiles)

    def _on_drop(self, drop_target, drop, x, y):
        directory_gfile = self._enquiry("delta > button gfile")
        self._try_move_files(directory_gfile, drop)
        return Gdk.EVENT_PROPAGATE

    def __init__(self, parent):
        self._parent = parent
        drop_target = Gtk.DropTarget.new(Gdk.FileList, Gdk.DragAction.COPY,)
        drop_target.connect("drop", self._on_drop)
        self._raise("delta > add controller", drop_target)
