# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from kusozako1.Entity import DeltaEntity
from kusozako1.const import MainWindowSignals as KusozakoSignals
from kusozako_files.const import MainWindowSignals
from kusozako_files.const import ExtraMenuPages


class DeltaActivate(DeltaEntity):

    def _get_file_info(self, list_view, index):
        model = list_view.get_model()
        tree_list_row = model[index]
        item = tree_list_row.get_item()
        if isinstance(item, Gio.FileInfo):
            return item
        else:
            return item.get_item()

    def _open_directory(self, gfile):
        user_data = MainWindowSignals.ADD_FILE_MANAGER, gfile
        self._raise("delta > main window signal", user_data)

    def _open_file(self, gfile):
        param = ExtraMenuPages.OPEN_FILE_STANDALONE, gfile
        user_data = KusozakoSignals.SHOW_EXTRA_PRIMARY_MENU_WITH_PARAM, param
        self._raise("delta > main window signal", user_data)

    def _dispath(self, gfile, file_info):
        if file_info.get_content_type() == "inode/directory":
            self._open_directory(gfile)
        else:
            self._open_file(gfile)

    def _on_activate(self, list_view, index):
        file_info = self._get_file_info(list_view, index)
        path = file_info.get_attribute_string("kusozako1::path")
        gfile = Gio.File.new_for_path(path)
        if not gfile.query_exists():
            return
        self._dispath(gfile, file_info)

    def __init__(self, parent):
        self._parent = parent
        list_view = self._enquiry("delta > list view")
        list_view.connect("activate", self._on_activate)
