# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from kusozako_files.const import FileManagerSignals
from kusozako_files.alfa.Observer import AlfaObserver


class DeltaQueueIndex(AlfaObserver):

    REGISTRATION = "delta > register file manager object"
    SIGNAL = FileManagerSignals.QUEUE_INDEX

    def _queue_directory(self, gfile):
        user_data = FileManagerSignals.MOVE_DIRECTORY, gfile
        self._raise("delta > file manager signal", user_data)

    def _get_gfile(self, file_info):
        gfile = file_info.get_attribute_object("standard::file")
        if gfile.get_uri_scheme() != "trash":
            return gfile
        target_uri = file_info.get_attribute_string("standard::target-uri")
        return Gio.File.new_for_uri(target_uri)

    def _dispatch(self, selection_model, index, file_info):
        if file_info.get_file_type() != Gio.FileType.DIRECTORY:
            return
        gfile = self._get_gfile(file_info)
        self._queue_directory(gfile)

    def _on_received(self, index):
        selection_model = self._enquiry("delta > selection model")
        file_info = selection_model[index]
        self._dispatch(selection_model, index, file_info)
