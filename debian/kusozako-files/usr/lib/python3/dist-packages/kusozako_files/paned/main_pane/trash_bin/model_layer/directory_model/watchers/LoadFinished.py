# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako_files.const import FileManagerSignals


class DeltaLoadFinished(DeltaEntity):

    def _on_items_changed(self, directory_model, *args):
        if directory_model.is_loading():
            return
        n_items = directory_model.get_n_items()
        user_data = FileManagerSignals.LOAD_FINISHED, n_items
        self._raise("delta > file manager signal", user_data)

    def __init__(self, parent):
        self._parent = parent
        directory_model = self._enquiry("delta > directory model")
        # directory_model.connect("items-changed", self._on_items_changed)
        directory_model.connect("notify::loading", self._on_items_changed)
