# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Gdk
from gi.repository import GLib
from kusozako1.Entity import DeltaEntity
from kusozako_files.file_manipulation import Move


class DeltaDropTarget(DeltaEntity):

    def _on_drop(self, drop_target, file_list, x, y):
        directory_gfile = self._enquiry("delta > current gfile")
        Move.move_gfiles(directory_gfile, file_list.get_files())
        return True     # True means accept drop and handled data here.

    def _timeout(self, vadjustment, additional_value):
        vadjustment.props.value += additional_value
        return self._lock

    def _on_motion(self, drop_target, x, y):
        widget = drop_target.get_widget()
        vadjustment = widget.get_vadjustment()
        if 64 >= y:
            self._lock = GLib.SOURCE_CONTINUE
            GLib.timeout_add(100, self._timeout, vadjustment, -1)
        elif 64 >= (widget.get_height() - y):
            self._lock = GLib.SOURCE_CONTINUE
            GLib.timeout_add(100, self._timeout, vadjustment, 1)
        else:
            self._lock = GLib.SOURCE_REMOVE
        return Gdk.DragAction.MOVE

    def __init__(self, parent):
        self._parent = parent
        drop_target = Gtk.DropTarget.new(
            Gdk.FileList,
            Gdk.DragAction.COPY | Gdk.DragAction.MOVE
            )
        drop_target.connect("drop", self._on_drop)
        drop_target.connect("motion", self._on_motion)
        self._raise("delta > add controller", drop_target)
