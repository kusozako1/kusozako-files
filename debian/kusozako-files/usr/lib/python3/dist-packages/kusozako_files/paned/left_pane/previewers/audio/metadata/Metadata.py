# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.audio_metadata.AudioMetadata import FoxtrotAudioMetadata
from .Markup import FoxtrotMarkup


class DeltaMetadata(Gtk.Label, DeltaEntity):

    def _loaded(self, title, artist, album, duration, user_data=None):
        markup = self._markup.build_(title, artist, album, duration)
        self.set_markup(markup)

    def set_file_info_async(self, file_info):
        self._audio_metadata.load_async(file_info, self._loaded)

    def __init__(self, parent):
        self._parent = parent
        self._audio_metadata = FoxtrotAudioMetadata.get_default()
        self._markup = FoxtrotMarkup()
        Gtk.Label.__init__(
            self,
            wrap=True,
            margin_top=8,
            margin_bottom=16,
            margin_start=8,
            margin_end=8,
            use_markup=True,
            )
        self._raise("delta > add to container", self)
