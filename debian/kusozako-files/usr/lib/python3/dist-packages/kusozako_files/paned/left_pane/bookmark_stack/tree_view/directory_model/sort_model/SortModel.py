# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .FileType import DeltaFileType
from .CollateKey import DeltaCollateKey


class FoxtrotSortModel(Gtk.SortListModel, DeltaEntity):

    @classmethod
    def new_for_model(cls, model):
        instance = cls()
        instance.construct(model)
        return instance

    def _delta_call_add_sorter(self, sorter):
        self._sorter.append(sorter)

    def construct(self, model):
        self.set_model(model)

    def __init__(self):
        self._parent = None
        self._sorter = Gtk.MultiSorter()
        DeltaFileType(self)
        DeltaCollateKey(self)
        Gtk.SortListModel.__init__(self, sorter=self._sorter)
