# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .directory_model.DirectoryModel import DeltaDirectoryModel
from .filter_model.FilterModel import DeltaFilterModel
from .sort_model.SortModel import DeltaSortModel
from .SelectionModel import DeltaSelectionModel
from .operations.Operations import EchoOperations


class DeltaModelLayer(DeltaEntity):

    @classmethod
    def new_for_gfile(cls, parent, gfile):
        instance = cls(parent)
        instance.construct(gfile)
        return instance

    def _delta_info_current_gfile(self):
        return self._directory_model.get_file()

    def _delta_info_viewer_model(self):
        return self._viewer_model

    def _delta_info_selection_model(self):
        return self._selection_model

    def construct(self, gfile):
        self._directory_model.set_file(gfile)
        self._raise("delta > loopback model ready", self)

    def __init__(self, parent):
        self._parent = parent
        self._directory_model = DeltaDirectoryModel(self)
        filter_model = DeltaFilterModel.new(self, self._directory_model)
        sort_model = DeltaSortModel.new(self, filter_model)
        slice_model = Gtk.SliceListModel.new(sort_model, 0, 100)
        self._viewer_model = Gtk.SingleSelection.new(slice_model)
        self._selection_model = DeltaSelectionModel.new(self, slice_model)
        EchoOperations(self)
