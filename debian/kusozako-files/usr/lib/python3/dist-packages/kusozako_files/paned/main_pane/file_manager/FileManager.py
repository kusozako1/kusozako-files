# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib
from kusozako1.Entity import DeltaEntity
from kusozako1.Transmitter import FoxtrotTransmitter
from .PageSettings import DeltaPageSettings
from .model_layer.ModelLayer import DeltaModelLayer
from .widget_layer.WidgetLayer import DeltaWidgetLayer


class DeltaFileManager(DeltaEntity):

    @classmethod
    def new(cls, parent, gfile=None):
        instance = cls(parent)
        if gfile is None:
            current_directory = GLib.get_current_dir()
            gfile = Gio.File.new_for_path(current_directory)
        instance.construct(gfile)

    def _delta_call_loopback_model_ready(self, parent):
        DeltaWidgetLayer(parent)

    def _delta_call_register_file_manager_object(self, object_):
        self._transmitter.register_listener(object_)

    def _delta_call_file_manager_signal(self, user_data):
        self._transmitter.transmit(user_data)

    def _delta_info_page_settings(self, key):
        return self._page_settings[key]

    def construct(self, gfile):
        DeltaModelLayer.new_for_gfile(self, gfile)

    def __init__(self, parent):
        self._parent = parent
        self._transmitter = FoxtrotTransmitter()
        self._page_settings = DeltaPageSettings(self)
