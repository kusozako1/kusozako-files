# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Gio


class FoxtrotItem(Gtk.TreeExpander):

    def _get_file_info(self, tree_list_row):
        item = tree_list_row.get_item()
        if isinstance(item, Gio.FileInfo):
            return item
        else:
            return self._get_file_info(item)

    def bind(self, tree_list_row):
        file_info = self._get_file_info(tree_list_row)
        gicon = file_info.get_icon()
        self._image.set_from_gicon(gicon)
        display_name = file_info.get_display_name()
        self._label.set_label(display_name)
        self.set_list_row(tree_list_row)

    def unbind(self):
        pass

    def __init__(self):
        Gtk.TreeExpander.__init__(self)
        self._box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
        self._image = Gtk.Image()
        self._box.append(self._image)
        self._label = Gtk.Label(hexpand=True, xalign=0, margin_start=8)
        self._label.set_size_request(-1, 32)
        self._box.append(self._label)
        self.set_child(self._box)
