# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako1.const import MainWindowSignals
from kusozako_files.const import FileManagerSignals
from kusozako_files.alfa.Observer import AlfaObserver

PRIORITY = GLib.PRIORITY_DEFAULT_IDLE


class DeltaTrashSelection(AlfaObserver):

    REGISTRATION = "delta > register file manager object"
    SIGNAL = FileManagerSignals.TRASH_SELECTION

    def _trash_async(self, gfile, task):
        if not gfile.trash_finish(task):
            print("error !!", gfile.get_path())

    def _on_confirmed(self):
        model = self._enquiry("delta > selection model")
        for file_info in model.get_selected_file_infos():
            if not file_info.get_attribute_boolean("access::can-trash"):
                print("delta > can't trash ", file_info.get_name())
                continue
            gfile = file_info.get_attribute_object("standard::file")
            gfile.trash_async(PRIORITY, None, self._trash_async)
        model.unselect_all()
        user_data = MainWindowSignals.CLOSE_OVERLAY, None
        self._raise("delta > main window signal", user_data)

    def _on_received(self, param=None):
        user_data = MainWindowSignals.SHOW_DIALOG, self._dialog_model
        self._raise("delta > main window signal", user_data)

    def _on_initialize(self):
        buttons = [
            (_("Cancel"), None),
            (_("Remove"), self._on_confirmed)
            ]
        self._dialog_model = {
            "heading": _("Remove Selected Files ?"),
            "buttons": buttons
            }
