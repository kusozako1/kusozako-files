# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files.const import FileManagerSignals
from kusozako_files.viewer.ListView import AlfaListView
from .HeaderFactory import FoxtrotHeaderFactory
from .item.Item import DeltaItem
from .controllers.Controllers import EchoControllers


class DeltaListView(AlfaListView):

    __context_menu_signal__ = FileManagerSignals.CALL_FILE_CONTEXT_MENU

    def _get_list_item_widget(self, selection_model):
        return DeltaItem(selection_model)

    def _get_header_factory(self):
        return FoxtrotHeaderFactory()

    def _setup_controllers(self):
        EchoControllers(self)
