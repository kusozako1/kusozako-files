# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later


def _get_is_executable(file_info):
    if file_info.get_content_type() == "inode/directory":
        return None
    if not file_info.get_attribute_boolean("access::can-execute"):
        return None
    return _("EXECUTABLE"), (255/256, 228/256, 0/256, 0.8)


def _get_is_read_only(file_info):
    if not file_info.get_attribute_boolean("access::can-write"):
        return _("READ ONLY"), (255/256, 64/256, 0/256, 0.8)
    return _get_is_executable(file_info)


def _get_is_symlink(file_info):
    if file_info.get_attribute_boolean("standard::is-symlink"):
        return _("SYMLINK"), (103/256, 255/256, 0/256, 0.8)
    return _get_is_read_only(file_info)


def get_status(file_info):
    if not file_info.get_attribute_boolean("access::can-read"):
        return _("INACCESSIBLE"), (255/256, 0/256, 0/256, 0.8)
    return _get_is_symlink(file_info)
