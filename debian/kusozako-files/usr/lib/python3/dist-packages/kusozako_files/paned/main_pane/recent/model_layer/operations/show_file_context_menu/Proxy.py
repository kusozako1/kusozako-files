# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from kusozako1.Entity import DeltaEntity


class DeltaProxy(DeltaEntity):

    def get_selected_gfile(self):
        return self._gfile

    def get_selection_model(self):
        model = self._enquiry("delta > selection model")
        return model, self._index

    def set_index(self, index):
        self._index = index
        selection_model = self._enquiry("delta > selection model")
        file_info = selection_model[self._index]
        target_uri = file_info.get_attribute_string("standard::target-uri")
        self._gfile = Gio.File.new_for_uri(target_uri)

    def __init__(self, parent):
        self._parent = parent
        self._index = None
