# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files.const import FileManagerSignals
from .OperationButton import AlfaOperationButton


class DeltaTrashButton(AlfaOperationButton):

    __icon_name__ = "user-trash-symbolic"
    __tooltip_text__ = _("Send Files to Trash Bin")
    __file_namager_signal__ = FileManagerSignals.TRASH_SELECTION

    def _get_sensitive(self, selection_model):
        for file_info in selection_model.get_selected_file_infos():
            if file_info.get_attribute_boolean("access::can-trash"):
                return True
        return False
