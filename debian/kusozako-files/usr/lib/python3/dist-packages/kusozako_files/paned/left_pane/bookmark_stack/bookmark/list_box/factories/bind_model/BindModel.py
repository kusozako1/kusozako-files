# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from .BookmarkItem import FoxtrotBookmarkItem
from .trash_bin_button.TrashBinButton import DeltaTrashBinButton
from .RecentButton import DeltaRecentButton


class DeltaBindModel(DeltaEntity):

    def _get_places_item(self, file_info):
        uri = file_info.get_attribute_string("kusozako1::uri")
        if uri == "recent:///":
            return DeltaRecentButton(self)
        if uri == "trash:///":
            return DeltaTrashBinButton(self)

    def _create_widget_func(self, file_info):
        if file_info.get_attribute_string("kusozako1::type") == "places":
            return self._get_places_item(file_info)
        else:
            return FoxtrotBookmarkItem.new(file_info)

    def __init__(self, parent):
        self._parent = parent
        selection_model = self._enquiry("delta > model")
        list_box = self._enquiry("delta > list box")
        list_box.bind_model(selection_model, self._create_widget_func)
