# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako_files.const import SortType
from kusozako_files.alfa.Sorter import AlfaSorter


class DeltaLastAccessed(AlfaSorter):

    SORT_TYPE = SortType.LAST_ACCESSED

    def _get_compared(self, alfa, bravo):
        alfa_modified = alfa.get_attribute_int64("recent::modified")
        alfa_date = GLib.DateTime.new_from_unix_local(alfa_modified)
        bravo_modified = bravo.get_attribute_int64("recent::modified")
        bravo_date = GLib.DateTime.new_from_unix_local(bravo_modified)
        return bravo_date.compare(alfa_date)
