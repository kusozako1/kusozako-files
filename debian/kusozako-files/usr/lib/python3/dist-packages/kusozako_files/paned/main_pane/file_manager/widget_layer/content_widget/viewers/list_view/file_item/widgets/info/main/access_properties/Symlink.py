# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako1.util import HomeDirectory
from kusozako_files.utility.TagBuilder import FoxtrotTagBuilder


class DeltaSymlink(DeltaEntity):

    def bind(self, file_info, gfile):
        path = file_info.get_attribute_byte_string("standard::symlink-target")
        if path is None:
            return
        parent_gfile = gfile.get_parent()
        target_gfile = parent_gfile.resolve_relative_path(path)
        target_path = target_gfile.get_path()
        shorten_path = HomeDirectory.shorten(target_path)
        text = _("link to : {}".format(shorten_path))
        tag_label = FoxtrotTagBuilder.new(
            "symlink",
            "insert-link-symbolic",
            text,
            "kusozako-green-tag",
            )
        self._raise("delta > add tag label", tag_label)

    def __init__(self, parent):
        self._parent = parent
