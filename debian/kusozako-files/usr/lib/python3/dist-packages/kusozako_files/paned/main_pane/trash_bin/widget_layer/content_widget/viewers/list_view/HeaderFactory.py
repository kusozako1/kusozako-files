# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk


class FoxtrotHeaderFactory(Gtk.SignalListItemFactory):

    def _on_setup(self, factory, list_header):
        label = Gtk.Label()
        list_header.set_child(label)

    def _get_label_text(self, file_info):
        date_time = file_info.get_deletion_date()
        if date_time is None:
            return ""
        return date_time.format("%x")

    def _on_bind(self, factory, list_header):
        file_info = list_header.get_item()
        if file_info is None:
            return
        text = self._get_label_text(file_info)
        label = list_header.get_child()
        label.set_label(text)

    def __init__(self):
        Gtk.SignalListItemFactory.__init__(self)
        self.connect("setup", self._on_setup)
        self.connect("bind", self._on_bind)
