# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Gio


class FoxtrotBookmarkItem(Gtk.Box):

    @classmethod
    def new(cls, file_info):
        bookmark_item = cls()
        bookmark_item.construct(file_info)
        return bookmark_item

    def _get_image(self, file_info):
        icon = file_info.get_symbolic_icon()
        return Gtk.Image(
            icon_name=icon.get_names()[0],
            pixel_size=16,
            margin_top=8,
            margin_bottom=8,
            margin_start=8,
            )

    def get_gio_file(self):
        uri = self._file_info.get_attribute_string("kusozako1::uri")
        return Gio.File.parse_name(uri)

    def get_type(self):
        return self._file_info.get_attribute_string("kusozako1::type")

    def construct(self, file_info):
        self._file_info = file_info
        self.append(self._get_image(file_info))
        self.append(Gtk.Label(label=file_info.get_name()))

    def __init__(self):
        Gtk.Box.__init__(
            self,
            orientation=Gtk.Orientation.HORIZONTAL,
            spacing=8,
            )
        self.set_size_request(-1, 32)
        self.add_css_class("kusozako-primary-widget")
