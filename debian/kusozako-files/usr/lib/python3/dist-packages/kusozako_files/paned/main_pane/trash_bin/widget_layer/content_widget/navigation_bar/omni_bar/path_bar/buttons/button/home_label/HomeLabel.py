# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Pango
from kusozako1.Entity import DeltaEntity
from .Motion import DeltaMotion
from .CurrentGFile import DeltaCurrentGFile

CSS_CLASS = "kusozako-button-indicator-highlight"


class DeltaHomeLabel(Gtk.Box, DeltaEntity):

    def _delta_call_directory_moved(self, equal):
        if equal:
            self.add_css_class(CSS_CLASS)
        else:
            self.remove_css_class(CSS_CLASS)

    def _delta_call_extended_changed(self, extended):
        if extended or self._current_gfile.equal:
            self._label.set_ellipsize(Pango.EllipsizeMode.NONE)
        else:
            self._label.set_ellipsize(Pango.EllipsizeMode.END)

    def _delta_call_add_controller(self, controller):
        self.add_controller(controller)

    def __init__(self, parent):
        self._parent = parent
        gfile = self._enquiry("delta > button gfile")
        file_info = gfile.query_info("standard::symbolic-icon", 0)
        gicon = file_info.get_attribute_object("standard::symbolic-icon")
        Gtk.Box.__init__(
            self,
            orientation=Gtk.Orientation.HORIZONTAL,
            tooltip_text=gfile.get_basename(),
            spacing=4,
            )
        self.append(Gtk.Image.new_from_gicon(gicon))
        self._label = Gtk.Label(label=_("Trash"))
        self.append(self._label)
        self._current_gfile = DeltaCurrentGFile(self)
        self._current_gfile.start()
        DeltaMotion(self)
        self._raise("delta > add to container", self)
