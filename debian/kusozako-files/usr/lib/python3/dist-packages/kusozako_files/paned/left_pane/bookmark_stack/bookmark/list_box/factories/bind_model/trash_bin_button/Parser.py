# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib
from kusozako1.Entity import DeltaEntity


class DeltaParser(DeltaEntity):

    def _get_n_items(self, trash_directory):
        file_info = trash_directory.query_info("trash::item-count", 0)
        return file_info.get_attribute_uint32("trash::item-count")

    def _on_changed(self, monitor, alfa, bravo, event, trash_directory):
        n_items = self._get_n_items(trash_directory)
        self._raise("delta > parsed", n_items)

    def _timeout(self, trash_directory):
        n_items = self._get_n_items(trash_directory)
        self._raise("delta > parsed", n_items)
        return GLib.SOURCE_CONTINUE

    def __init__(self, parent):
        self._parent = parent
        trash_directory = Gio.File.new_for_uri("Trash:///")
        """
        n_items = self._get_n_items(trash_directory)
        self._raise("delta > parsed", n_items)
        self._monitor = trash_directory.monitor_directory(
            Gio.FileMonitorFlags.NONE,
            None
            )
        self._monitor.connect("changed", self._on_changed, trash_directory)
        """
        GLib.timeout_add_seconds(1, self._timeout, trash_directory)
