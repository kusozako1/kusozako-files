# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from .Attributes import FoxtrotAttributes


class FoxtrotBaseModel:

    @classmethod
    def new_for_gfile(cls, gfile):
        instance = cls()
        return instance.construct(gfile)

    def _on_items_changed(self, model, start, removed, added, parent_gfile):
        for index in range(start, start+added):
            file_info = model[index]
            gfile = parent_gfile.get_child(file_info.get_name())
            self._attributes.set_attributes(gfile, file_info)

    def construct(self, gfile):
        self._attributes = FoxtrotAttributes()
        base_model = Gtk.DirectoryList.new("*", gfile)
        base_model.connect("items-changed", self._on_items_changed, gfile)
        return base_model
