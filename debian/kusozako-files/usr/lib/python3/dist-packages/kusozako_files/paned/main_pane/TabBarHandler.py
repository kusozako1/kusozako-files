# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Adw
from kusozako1.Entity import DeltaEntity


class DeltaTabBarHandler(DeltaEntity):

    @classmethod
    def new(cls, parent, overview):
        instance = cls(parent)
        return instance.construct(overview)

    def _on_clicked(self, tab_button, overview):
        overview.set_open(True)

    def construct(self, overview):
        tab_view = self._enquiry("delta > tab view")
        tab_bar = Adw.TabBar(autohide=True, view=tab_view)
        tab_button = Adw.TabButton(view=tab_view)
        tab_button.connect("clicked", self._on_clicked, overview)
        tab_bar.set_start_action_widget(tab_button)
        return tab_bar

    def __init__(self, parent):
        self._parent = parent
