# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity


class DeltaProxy(DeltaEntity):

    def get_selected_gfile(self):
        return self._gfile

    def get_selection_model(self):
        selection_model = self._enquiry("delta > selection model")
        return selection_model, -1    # -1 as impossible index

    def set_gfile(self):
        self._gfile = self._enquiry("delta > current gfile")
        print("gfile", self._gfile.get_path())

    def __init__(self, parent):
        self._parent = parent
