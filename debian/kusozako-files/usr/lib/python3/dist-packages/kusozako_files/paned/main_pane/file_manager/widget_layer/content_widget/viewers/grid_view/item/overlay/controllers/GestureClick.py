# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Gdk
from kusozako1.Entity import DeltaEntity


class DeltaGestureClick(Gtk.GestureClick, DeltaEntity):

    def _on_released(self, gesture_click, *args):
        self._raise("delta > request context menu")

    def __init__(self, parent):
        self._parent = parent
        Gtk.GestureClick.__init__(
            self,
            button=Gdk.BUTTON_SECONDARY
            )
        self.connect("released", self._on_released)
        self._raise("delta > add controller", self)
