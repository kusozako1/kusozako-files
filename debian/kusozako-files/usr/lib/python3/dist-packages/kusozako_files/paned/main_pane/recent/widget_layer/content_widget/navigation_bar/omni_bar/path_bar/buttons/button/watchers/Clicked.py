# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako_files.const import FileManagerSignals


class DeltaClicked(DeltaEntity):

    def _on_clicked(self, button):
        gfile = self._enquiry("delta > button gfile")
        user_data = FileManagerSignals.MOVE_DIRECTORY, gfile
        self._raise("delta > file manager signal", user_data)

    def __init__(self, parent):
        self._parent = parent
        button = self._enquiry("delta > button")
        button.connect("clicked", self._on_clicked)
