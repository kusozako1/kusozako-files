# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .main.Main import DeltaMain
from .Properties import DeltaProperties


class DeltaInfo(Gtk.Box, DeltaEntity):

    def _on_layout(self, top_level, width, height):
        if self.get_width() >= 600:
            self.set_orientation(Gtk.Orientation.HORIZONTAL)
            self.set_homogeneous(True)
        else:
            self.set_orientation(Gtk.Orientation.VERTICAL)
            self.set_homogeneous(False)

    def _on_realize(self, widget):
        native = self.get_native()
        surface = native.get_surface()
        surface.connect("layout", self._on_layout)

    def _delta_call_add_to_container(self, widget):
        self.append(widget)

    def set_pixbuf_options(self, pixbuf_options):
        pass
        # self._main.set_pixbuf_options(pixbuf_options)

    def bind(self, gfile, file_info):
        pass
        # self._main.bind(file_info)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(
            self,
            orientation=Gtk.Orientation.VERTICAL,
            homogeneous=True,
            )
        DeltaMain(self)
        DeltaProperties(self)
        self.connect("realize", self._on_realize)
        self._raise("delta > add to container", self)
