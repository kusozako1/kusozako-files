# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from kusozako1.const import MainWindowSignals
from kusozako_files.const import FileManagerSignals
from kusozako_files.const import ExtraMenuPages
from kusozako_files.alfa.Observer import AlfaObserver


class DeltaQueueIndex(AlfaObserver):

    REGISTRATION = "delta > register file manager object"
    SIGNAL = FileManagerSignals.QUEUE_INDEX

    def _get_target_gfile(self, file_info):
        link = file_info.get_attribute_byte_string("standard::symlink-target")
        if link is None:
            return file_info.get_attribute_object("standard::file")
        else:
            child_gfile = file_info.get_attribute_object("standard::file")
            parent_gfile = child_gfile.get_parent()
            return parent_gfile.resolve_relative_path(link)

    def _queue_directory(self, file_info):
        if not file_info.get_attribute_boolean("access::can-read"):
            return
        gfile = self._get_target_gfile(file_info)
        user_data = FileManagerSignals.MOVE_DIRECTORY, gfile
        self._raise("delta > file manager signal", user_data)

    def _queue_file(self, file_info):
        gfile = file_info.get_attribute_object("standard::file")
        param = ExtraMenuPages.OPEN_FILE_STANDALONE, gfile
        user_data = MainWindowSignals.SHOW_EXTRA_PRIMARY_MENU_WITH_PARAM, param
        self._raise("delta > main window signal", user_data)

    def _dispatch(self, selection_model, index, file_info):
        content_type = file_info.get_content_type()
        if content_type == "inode/directory":
            self._queue_directory(file_info)
        else:
            selection_model.select_item(index, False)
            self._queue_file(file_info)

    def _on_received(self, index):
        try:
            selection_model = self._enquiry("delta > selection model")
            file_info = selection_model[index]
            self._dispatch(selection_model, index, file_info)
        except TypeError:
            return
