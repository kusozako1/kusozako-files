# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako_files.const import FileItemSignals


class DeltaNameLabel(Gtk.Inscription, DeltaEntity):

    def _bind(self, file_info):
        display_name = file_info.get_display_name()
        self.set_text(display_name)

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal != FileItemSignals.BINDED:
            return
        _, file_info, _ = param
        self._bind(file_info)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Inscription.__init__(
            self,
            hexpand=True,
            text_overflow=Gtk.InscriptionOverflow.ELLIPSIZE_MIDDLE,
            nat_lines=3,
            )
        self._raise("delta > add to container", self)
        self._raise("delta > register file item object", self)
