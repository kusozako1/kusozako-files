# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .watchers.RowActivated import DeltaRowActivated
from .factories.Factories import EchoFactories


class DeltaListBox(Gtk.ListBox, DeltaEntity):

    def _delta_info_list_box(self):
        return self

    def _delta_call_add_controller(self, controller):
        self.add_controller(controller)

    def __init__(self, parent):
        self._parent = parent
        scrolled_window = Gtk.ScrolledWindow()
        Gtk.ListBox.__init__(
            self,
            vexpand=True,
            selection_mode=Gtk.SelectionMode.NONE,
            )
        self.add_css_class("kusozako-content-area-medium")
        DeltaRowActivated(self)
        EchoFactories(self)
        scrolled_window.set_child(self)
        self._raise("delta > add to container", scrolled_window)
