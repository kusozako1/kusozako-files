# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .Size import DeltaSize
from .Type import DeltaType
from .DateTime import DeltaDateTime


class DeltaProperties(Gtk.Box, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.append(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(
            self,
            orientation=Gtk.Orientation.HORIZONTAL,
            homogeneous=True,
            spacing=8
            )
        DeltaSize(self)
        DeltaType(self)
        DeltaDateTime(self)
        self._raise("delta > add to container", self)
