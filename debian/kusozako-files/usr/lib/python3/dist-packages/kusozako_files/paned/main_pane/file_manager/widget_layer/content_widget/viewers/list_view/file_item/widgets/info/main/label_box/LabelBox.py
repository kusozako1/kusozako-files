# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako_files.const import FileItemSignals
from .PixbufProperties import DeltaPixbufProperties
from kusozako1.const import PixbufOptionKeys


class DeltaLabelBox(Gtk.Box, DeltaEntity):

    def _set_pixbuf_options(self, pixbuf):
        options = pixbuf.get_options()
        if not options:
            self._remove_all()
        uri = options.get(PixbufOptionKeys.URI, None)
        if uri == self._uri:
            return
        self._remove_all()
        self._uri = uri
        self._pixbuf_properties.set_pixbuf(options)

    def _remove_all(self):
        for widget in self:
            self.remove(widget)

    def _delta_call_add_to_container(self, widget):
        self.append(widget)

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal != FileItemSignals.PIXBUF_LOADED:
            return
        self._set_pixbuf_options(param)

    def __init__(self, parent):
        self._parent = parent
        self._uri = None
        Gtk.Box.__init__(
            self,
            hexpand=True,
            orientation=Gtk.Orientation.HORIZONTAL,
            spacing=8,
            )
        self._pixbuf_properties = DeltaPixbufProperties(self)
        self._raise("delta > add to container", self)
        self._raise("delta > register file item object", self)
