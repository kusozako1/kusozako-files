# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib

RGBA = {
    "image": (25/256, 125/256, 25/256, 0.4),
    "video": (0, 0, 0, 0.4),
    "audio": (25/256, 25/256, 256, 0.4),
    "application/pdf": (256, 25/256, 25/256, 0.4),
}


class FoxtrotFileInfoHandler:

    def get_markup(self):
        if self._file_info is None:
            return None
        basename = self._file_info.get_display_name()
        return GLib.markup_escape_text(basename, -1)

    def get_rgba(self):
        content_type = self._file_info.get_content_type()
        for key, value in RGBA.items():
            if content_type.startswith(key):
                return value
        return 25/256, 25/256, 112/256, 0.6

    def bind(self, file_info):
        self._file_info = file_info

    def __init__(self):
        self._file_info = None
