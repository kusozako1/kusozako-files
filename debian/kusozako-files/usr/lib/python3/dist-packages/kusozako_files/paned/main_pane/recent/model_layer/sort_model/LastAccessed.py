# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import GLib
from kusozako1.Entity import DeltaEntity


class DeltaLastAccessed(Gtk.CustomSorter, DeltaEntity):

    def _get_date_element(self, file_info):
        recent_modified = file_info.get_attribute_int64("recent::modified")
        date_time = GLib.DateTime.new_from_unix_local(recent_modified)
        # print(date_time.get_day_of_year())
        return [
            date_time.get_year(),
            # date_time.get_day_of_year()
            date_time.get_month(),
            # date_time.get_week_of_year(),
            date_time.get_day_of_month()
            ]

    def _sort_func(self, alfa, bravo, user_data=None):
        alfa_element = self._get_date_element(alfa)
        bravo_element = self._get_date_element(bravo)
        for index in range(0, 3):
            difference = bravo_element[index] - alfa_element[index]
            if difference != 0:
                return difference
        return 0

    def __init__(self, parent):
        self._parent = parent
        Gtk.CustomSorter.__init__(self)
        self.set_sort_func(self._sort_func)
