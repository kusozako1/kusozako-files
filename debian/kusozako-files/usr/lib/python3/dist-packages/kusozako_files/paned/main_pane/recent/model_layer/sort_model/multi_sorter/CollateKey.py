# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako_files.const import SortType
from kusozako_files.alfa.Sorter import AlfaSorter


class DeltaCollateKey(AlfaSorter):

    SORT_TYPE = SortType.FILE_NAME

    def _get_collate_key(self, file_info):
        display_name = file_info.get_display_name()
        return GLib.utf8_collate_key_for_filename(display_name, -1)

    def _get_compared(self, alfa, bravo):
        alfa_key = self._get_collate_key(alfa)
        bravo_key = self._get_collate_key(bravo)
        return 1 if alfa_key > bravo_key else -1
