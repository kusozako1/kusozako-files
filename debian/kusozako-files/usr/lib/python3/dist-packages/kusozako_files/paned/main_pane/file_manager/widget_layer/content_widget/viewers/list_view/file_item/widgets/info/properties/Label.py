# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Pango
from kusozako1.Entity import DeltaEntity
from kusozako_files.const import FileItemSignals


class AlfaLabel(Gtk.Label, DeltaEntity):

    def _bind(self, file_info):
        raise NotImplementedError()

    def _on_initialize(self):
        pass

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal != FileItemSignals.BINDED:
            return
        _, file_info, _ = param
        self._bind(file_info)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(self, ellipsize=Pango.EllipsizeMode.END, xalign=1)
        self._on_initialize()
        self.add_css_class("dim-label")
        self._raise("delta > register file item object", self)
        self._raise("delta > add to container", self)
