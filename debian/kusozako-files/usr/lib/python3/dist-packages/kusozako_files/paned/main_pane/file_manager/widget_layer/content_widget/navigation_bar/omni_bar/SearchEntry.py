# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import GLib
from kusozako1.Entity import DeltaEntity
from kusozako_files.const import FileManagerSignals


class DeltaSearchEntry(Gtk.SearchEntry, DeltaEntity):

    def _on_changed(self, editable):
        keyword = editable.get_text()
        user_data = FileManagerSignals.FILTER_KEYWORD_CHANGED, keyword
        self._raise("delta > file manager signal", user_data)
        return GLib.SOURCE_REMOVE

    def __init__(self, parent):
        self._parent = parent
        Gtk.SearchEntry.__init__(
            self,
            hexpand=True,
            margin_top=4,
            margin_bottom=4,
            margin_start=4,
            margin_end=4,
            )
        self.connect("search-changed", self._on_changed)
        user_data = self, "search-entry"
        self._raise("delta > add to stack", user_data)
