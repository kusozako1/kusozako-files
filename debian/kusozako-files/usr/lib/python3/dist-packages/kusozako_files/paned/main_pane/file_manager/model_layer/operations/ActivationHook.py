# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako1.Entity import DeltaEntity
from kusozako_files.const import FileManagerSignals


class DeltaActivationHook(DeltaEntity):

    def _timeout(self, index):
        if self._index == index:
            self._index = None
        return GLib.SOURCE_REMOVE

    def _set_index(self, index):
        self._index = index
        GLib.timeout_add(250, self._timeout, index)

    def _try_queue(self):
        if self._index is None:
            return
        user_data = FileManagerSignals.QUEUE_INDEX, self._index
        self._raise("delta > file manager signal", user_data)

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal == FileManagerSignals.TOGGLE_INDEX:
            self._set_index(param)
        if signal == FileManagerSignals.RELEASE_ACTIVATION_HOOK:
            self._try_queue()

    def __init__(self, parent):
        self._parent = parent
        self._index = None
        self._raise("delta > register file manager object", self)
