# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import GLib
from kusozako1.Entity import DeltaEntity


class DeltaSectionSorter(Gtk.CustomSorter, DeltaEntity):

    def _compare(self, alfa_ymd, bravo_ymd):
        for index in range(0, 3):
            compare = bravo_ymd[index] - alfa_ymd[index]
            if compare != 0:
                return compare
        return 0

    def _sort_func(self, alfa, bravo, user_data=None):
        if alfa is None:
            print("alfa is None")
            return 0
        alfa_modified = alfa.get_attribute_int64("recent::modified")
        alfa_date = GLib.DateTime.new_from_unix_local(alfa_modified)
        alfa_ymd = alfa_date.get_ymd()
        bravo_modified = bravo.get_attribute_int64("recent::modified")
        bravo_date = GLib.DateTime.new_from_unix_local(bravo_modified)
        bravo_ymd = bravo_date.get_ymd()
        return self._compare(alfa_ymd, bravo_ymd)

    def __init__(self, parent):
        self._parent = parent
        Gtk.CustomSorter.__init__(self)
        self.set_sort_func(self._sort_func)
