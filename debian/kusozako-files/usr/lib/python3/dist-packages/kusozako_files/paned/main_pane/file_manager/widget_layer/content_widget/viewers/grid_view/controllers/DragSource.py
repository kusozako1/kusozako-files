# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Gdk
from gi.repository import GObject
from kusozako1.Entity import DeltaEntity


class DeltaDragSource(Gtk.DragSource, DeltaEntity):

    def _on_prepare(self, drag_source, x, y):
        drag_source.set_actions(Gdk.DragAction.MOVE)
        selection_model = self._enquiry("delta > selection model")
        list_of_gfiles = [
            file_info.get_attribute_object("standard::file")
            for file_info
            in selection_model.get_selected_file_infos()
            ]
        if not list_of_gfiles:
            return None
        gdk_file_list = Gdk.FileList.new_from_list(list_of_gfiles)
        value = GObject.Value(Gdk.FileList, gdk_file_list)
        return Gdk.ContentProvider.new_for_value(value)

    def __init__(self, parent):
        self._parent = parent
        Gtk.DragSource.__init__(self)
        self.connect("prepare", self._on_prepare)
        self._raise("delta > add controller", self)
