# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako_files.const import FileManagerSignals
from kusozako_files.const import PageSettingsKeys


class DeltaShowHidden(Gtk.CustomFilter, DeltaEntity):

    def _match_func(self, file_info, user_data=None):
        if self._show_hidden:
            return True
        return not file_info.get_is_hidden()

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal != FileManagerSignals.PAGE_SETTINGS_CHANGED:
            return
        key, show_hidden = param
        if key != PageSettingsKeys.SHOW_HIDDEN:
            return
        self._show_hidden = show_hidden
        self.changed(Gtk.FilterChange.DIFFERENT)

    def __init__(self, parent):
        self._parent = parent
        self._show_hidden = False
        Gtk.CustomFilter.__init__(self)
        self.set_filter_func(self._match_func, None)
        self._raise("delta > add filter", self)
        self._raise("delta > register file manager object", self)
