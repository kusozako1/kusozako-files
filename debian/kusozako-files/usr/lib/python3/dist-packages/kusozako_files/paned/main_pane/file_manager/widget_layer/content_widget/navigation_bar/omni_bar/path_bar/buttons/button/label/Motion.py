# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import GLib
from kusozako1.Entity import DeltaEntity
from kusozako_files.const import MainWindowSignals


class DeltaMotion(Gtk.EventControllerMotion, DeltaEntity):

    def _idle(self, signal, extended, file_info=None):
        user_data = signal, file_info
        self._raise("delta > main window signal", user_data)
        self._raise("delta > extended changed", extended)
        return GLib.SOURCE_REMOVE

    def _on_enter(self, controller, x, y, file_info):
        args = MainWindowSignals.SHOW_FILE_PROPERTY, True, file_info
        GLib.timeout_add(1, self._idle, *args)

    def _on_leave(self, controller):
        args = MainWindowSignals.BACK_TO_BOOKMARK, False, None
        GLib.timeout_add(1, self._idle, *args)

    def _get_file_info(self):
        gfile = self._enquiry("delta > button gfile")
        file_info = gfile.query_info("*", 0)
        file_info.set_attribute_object("standard::file", gfile)
        return file_info

    def __init__(self, parent):
        self._parent = parent
        Gtk.EventControllerMotion.__init__(self)
        file_info = self._get_file_info()
        self.connect("enter", self._on_enter, file_info)
        self.connect("leave", self._on_leave)
        self._raise("delta > add controller", self)
