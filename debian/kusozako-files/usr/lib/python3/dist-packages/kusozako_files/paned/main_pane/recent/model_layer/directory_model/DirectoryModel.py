# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako_files.const import FileManagerSignals
from .watchers.Watchers import EchoWatchers


class DeltaDirectoryModel(Gtk.DirectoryList, DeltaEntity):

    def _delta_info_directory_model(self):
        return self

    def receive_transmission(self, user_data):
        signal, gfile = user_data
        if signal != FileManagerSignals.MOVE_DIRECTORY:
            return
        self.set_file(gfile)

    def __init__(self, parent):
        self._parent = parent
        Gtk.DirectoryList.__init__(
            self,
            attributes="*",
            )
        EchoWatchers(self)
        self._raise("delta > register file manager object", self)
