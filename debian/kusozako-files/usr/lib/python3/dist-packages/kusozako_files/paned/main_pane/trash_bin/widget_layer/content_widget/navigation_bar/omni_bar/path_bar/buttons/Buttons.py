# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib
from kusozako1.Entity import DeltaEntity
from .button.Button import DeltaButton

PATH = GLib.build_filenamev([GLib.get_user_data_dir(), "Trash", "files"])


class DeltaButtons(DeltaEntity):

    def _set_button(self, gfile):
        if gfile.get_path() == PATH:
            gfile = Gio.File.new_for_uri("Trash:///")
        button = DeltaButton.new_for_gio_file(self, gfile)
        uri = gfile.get_uri()
        self._buttons_dict[uri] = button
        self._raise("delta > add to container", button)
        parent = gfile.get_parent()
        if parent is not None:
            self._set_button(parent)

    def _clear_buttons(self):
        for button in self._buttons_dict.values():
            self._raise("delta > remove from container", button)
        self._buttons_dict.clear()

    def set_gfile(self, gfile):
        self._clear_buttons()
        self._set_button(gfile)

    def remove(self, gfile):
        uri = gfile.get_uri()
        if uri in self._buttons_dict:
            button = self._buttons_dict.pop(uri)
            self._raise("delta > remove from container", button)

    def __init__(self, parent):
        self._parent = parent
        self._buttons_dict = {}
        gfile = self._enquiry("delta > current gfile")
        self.set_gfile(gfile)
