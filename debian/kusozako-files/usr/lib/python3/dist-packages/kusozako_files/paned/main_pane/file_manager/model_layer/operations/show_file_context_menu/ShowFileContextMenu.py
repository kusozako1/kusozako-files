# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako1.const import MainWindowSignals
from kusozako_files.const import FileManagerSignals
from kusozako_files.const import ExtraOverlayPages
from .Proxy import DeltaProxy


class DeltaShowFileContextMenu(DeltaEntity):

    __signal__ = FileManagerSignals.SHOW_FILE_CONTEXT_MENU

    def _signal_received(self, param=None):
        viewer_model = self._enquiry("delta > viewer model")
        selected_index = viewer_model.get_selected()
        selection_model = self._enquiry("delta > selection model")
        selection_model.select_item(selected_index, False)
        self._proxy.set_index(selected_index)
        param = ExtraOverlayPages.FILE_CONTEXT_MENU, self._proxy
        user_data = MainWindowSignals.SHOW_EXTRA_PRIMARY_MENU_WITH_PARAM, param
        self._raise("delta > main window signal", user_data)

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal != self.__signal__:
            return
        self._signal_received(param)

    def _initialize(self):
        self._proxy = DeltaProxy(self)

    def __init__(self, parent):
        self._parent = parent
        self._initialize()
        self._raise("delta > register file manager object", self)
