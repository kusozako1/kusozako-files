# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files.alfa.Observer import AlfaObserver
from kusozako_files.const import MainWindowSignals


class DeltaAddFileManager(AlfaObserver):

    REGISTRATION = "delta > register main window signal object"
    SIGNAL = MainWindowSignals.ADD_FILE_MANAGER

    def _on_received(self, directory_gfile):
        self._raise("delta > add file manager", directory_gfile)
