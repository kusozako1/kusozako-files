# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.icon import Icon
from kusozako1.Entity import DeltaEntity
from kusozako_files.const import ViewerTypes
from kusozako_files.const import FileManagerSignals
from kusozako_files.const import PageSettingsKeys as Keys


class DeltaIcon(Gtk.Image, DeltaEntity):

    def _reset(self, setting):
        if setting == ViewerTypes.ICON_VIEW:
            self.add_css_class("kusozako-button-indicator-highlight")
        else:
            self.remove_css_class("kusozako-button-indicator-highlight")

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal != FileManagerSignals.PAGE_SETTINGS_CHANGED:
            return
        key, setting = param
        if key != Keys.VIEWER_TYPE:
            return
        self._reset(setting)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Image.__init__(self, vexpand=True)
        paintable = Icon.get_paintable_for_name("view-grid-symbolic")
        self.set_from_paintable(paintable)
        setting = self._enquiry("delta > page settings", Keys.VIEWER_TYPE)
        self._reset(setting)
        self._raise("delta > add to container", self)
        self._raise("delta > register file manager object", self)
