# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from kusozako1.Entity import DeltaEntity


class DeltaProxy(DeltaEntity):

    def _get_gfile(self, file_info):
        gfile = file_info.get_attribute_object("standard::file")
        if gfile.get_uri_scheme() != "trash":
            return gfile
        target_uri = file_info.get_attribute_string("standard::target-uri")
        return Gio.File.new_for_uri(target_uri)

    def get_selected_gfile(self):
        return self._gfile

    def get_selection_model(self):
        model = self._enquiry("delta > selection model")
        return model, self._index

    def set_index(self, index):
        self._index = index
        selection_model = self._enquiry("delta > selection model")
        file_info = selection_model[self._index]
        self._gfile = self._get_gfile(file_info)

    def __init__(self, parent):
        self._parent = parent
        self._index = None
