# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako_files.utility.TagBuilder import FoxtrotTagBuilder


class DeltaExecutable(DeltaEntity):

    def _add_label(self, label):
        tag_label = FoxtrotTagBuilder.new(
            "executable",
            "application-x-executable-symbolic",
            label,
            "kusozako-orange-tag",
            )
        self._raise("delta > add tag label", tag_label)

    def bind(self, file_info):
        if file_info.get_content_type() == "inode/directory":
            return
        if file_info.get_attribute_boolean("access::can-execute"):
            self._add_label("executable")

    def __init__(self, parent):
        self._parent = parent
