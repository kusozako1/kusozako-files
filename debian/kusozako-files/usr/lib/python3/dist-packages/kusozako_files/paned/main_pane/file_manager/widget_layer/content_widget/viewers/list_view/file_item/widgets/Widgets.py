# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .CheckButton import DeltaCheckButton
from .Image import DeltaImage
from .info.Info import DeltaInfo


class EchoWidgets:

    def __init__(self, parent):
        DeltaCheckButton(parent)
        DeltaImage(parent)
        DeltaInfo(parent)
