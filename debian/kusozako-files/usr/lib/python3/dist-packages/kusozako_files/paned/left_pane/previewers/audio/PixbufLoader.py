# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib
from kusozako1.Entity import DeltaEntity
from kusozako1.audio_cover_art.AudioCoverArt import FoxtrotAudioCoverArt


class DeltaPixbufLoader(DeltaEntity):

    def _get_uri(self, file_info):
        gfile = file_info.get_attribute_object("standard::file")
        if gfile.get_uri_scheme() == "file":
            return gfile.get_uri()
        target_uri = file_info.get_attribute_string("standard::target-uri")
        gfile = Gio.new_for_uri(target_uri)
        return gfile.get_uri()

    def _idle(self, file_info):
        uri = self._get_uri(file_info)
        cover_art = self._cover_art.get_for_uri(uri)
        if cover_art is not None:
            self._raise("delta > pixbuf loaded", cover_art)
        return GLib.SOURCE_REMOVE

    def load_async(self, file_info):
        GLib.idle_add(self._idle, file_info)

    def __init__(self, parent):
        self._parent = parent
        self._cover_art = FoxtrotAudioCoverArt.new_for_maximum_size(300)
