# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files.alfa.grid_view_item_overlay.GridViewItemOverlay import (
    AlfaGridViewItemOverlay
    )
from .controllers.Controllers import EchoControllers
from .widgets.Widgets import EchoWidgets


class DeltaOverlay(AlfaGridViewItemOverlay):

    def _on_setup(self):
        EchoControllers(self)
        EchoWidgets(self)
