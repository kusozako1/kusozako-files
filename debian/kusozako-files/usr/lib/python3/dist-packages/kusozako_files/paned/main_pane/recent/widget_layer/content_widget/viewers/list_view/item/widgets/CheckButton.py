# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako_files.const import FileItemSignals


class DeltaCheckButton(Gtk.CheckButton, DeltaEntity):

    def receive_transmission(self, user_data):
        signal, selected = user_data
        if signal != FileItemSignals.SELECTION_CHANGED:
            return
        self.set_active(selected)

    def __init__(self, parent):
        self._parent = parent
        Gtk.CheckButton.__init__(self)
        self._raise("delta > register file item object", self)
        self._raise("delta > add to container", self)
