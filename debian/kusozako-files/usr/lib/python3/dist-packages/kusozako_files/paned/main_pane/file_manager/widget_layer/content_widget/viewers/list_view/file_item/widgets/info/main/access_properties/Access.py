# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako_files.utility.TagBuilder import FoxtrotTagBuilder


class DeltaAccess(DeltaEntity):

    def _add_label(self, type_, label, css):
        tag_label = FoxtrotTagBuilder.new(
            type_,
            "action-unavailable-symbolic",
            label,
            css,
            )
        self._raise("delta > add tag label", tag_label)

    def bind(self, file_info):
        if not file_info.get_attribute_boolean("access::can-read"):
            self._add_label("access", "inaccessible", "kusozako-red-tag")
            return
        if not file_info.get_attribute_boolean("access::can-write"):
            self._add_label("access", "read-only", "kusozako-tomato-tag")

    def __init__(self, parent):
        self._parent = parent
