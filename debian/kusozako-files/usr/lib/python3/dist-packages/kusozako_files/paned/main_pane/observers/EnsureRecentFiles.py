# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files.alfa.Observer import AlfaObserver
from kusozako_files.const import MainWindowSignals


class DeltaEnsureRecentFiles(AlfaObserver):

    REGISTRATION = "delta > register main window signal object"
    SIGNAL = MainWindowSignals.ENSURE_RECENT_FILES

    def _on_received(self, param=None):
        tab_view = self._enquiry("delta > tab view")
        for page in tab_view.get_pages():
            content_widget = page.get_child()
            type_, _ = content_widget.get_state()
            if type_ == "recent":
                tab_view.set_selected_page(page)
                return
        self._raise("delta > add recent")
