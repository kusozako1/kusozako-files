# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import GLib
from kusozako1.Entity import DeltaEntity
from kusozako_files.const import Timeout


class DeltaRevealer(Gtk.Revealer, DeltaEntity):

    def _timeout(self, model):
        revealed = True if model.get_valid_selection_size() > 0 else False
        self.set_reveal_child(revealed)
        return GLib.SOURCE_REMOVE

    def _on_event(self, model, *args):
        if model.get_valid_selection_size() > 0:
            # to wait double clock.
            GLib.timeout_add(
                Timeout.WAIT_FOR_DOUBLE_CLICK,
                self._timeout, model
                )
        else:
            self.set_reveal_child(False)

    def _on_notify_reveal_child(self, revealer, param_spec):
        revealed = self.get_reveal_child()
        self.set_can_target(revealed)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Revealer.__init__(
            self,
            transition_type=Gtk.RevealerTransitionType.CROSSFADE,
            transition_duration=Timeout.WAIT_FOR_DOUBLE_CLICK,
            can_target=False,
            )
        self.set_valign(Gtk.Align.END)
        self.connect("notify::reveal-child", self._on_notify_reveal_child)
        model = self._enquiry("delta > selection model")
        model.connect("selection-changed", self._on_event)
        model.connect("items-changed", self._on_event)
