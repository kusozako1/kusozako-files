# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from kusozako1.Entity import DeltaEntity
from kusozako_files.const import FileManagerSignals


class DeltaIcon(DeltaEntity):

    def _reset(self, gfile=None):
        if gfile is None:
            return
        file_info = gfile.query_info("*", 0)
        if not file_info.get_attribute_boolean("access::can-write"):
            gicon = Gio.Icon.new_for_string("dialog-warning-symbolic")
        else:
            gicon = file_info.get_symbolic_icon()
        self._raise("delta > icon changed", gicon)

    def receive_transmission(self, user_data):
        signal, gfile = user_data
        if signal == FileManagerSignals.DIRECTORY_MOVED:
            self._reset(gfile)

    def __init__(self, parent):
        self._parent = parent
        gfile = self._enquiry("delta > current gfile")
        self._reset(gfile)
        self._raise("delta > register file manager object", self)
