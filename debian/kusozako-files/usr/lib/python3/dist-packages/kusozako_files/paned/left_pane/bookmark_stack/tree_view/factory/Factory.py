# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .item.Item import FoxtrotItem


class DeltaFactory(Gtk.SignalListItemFactory, DeltaEntity):

    def _on_setup(self, factory, list_item):
        item = FoxtrotItem()
        list_item.set_child(item)

    def _on_bind(self, factory, list_item):
        widget = list_item.get_child()
        tree_list_row = list_item.get_item()
        widget.bind(tree_list_row)

    def _on_unbind(self, factory, list_item):
        pass

    def __init__(self, parent):
        self._parent = parent
        Gtk.SignalListItemFactory.__init__(self)
        self.connect("setup", self._on_setup)
        self.connect("bind", self._on_bind)
        self.connect("unbind", self._on_unbind)
