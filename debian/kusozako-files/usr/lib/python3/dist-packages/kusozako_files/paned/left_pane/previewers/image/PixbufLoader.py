# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib
from gi.repository import GdkPixbuf
from kusozako1.Entity import DeltaEntity
from kusozako1.util import WebpToPixbuf


class DeltaPixbufLoader(DeltaEntity):

    def _get_path(self, file_info):
        gfile = file_info.get_attribute_object("standard::file")
        if gfile.get_uri_scheme() == "file":
            return gfile.get_path()
        target_uri = file_info.get_attribute_string("standard::target-uri")
        gfile = Gio.File.new_for_uri(target_uri)
        return gfile.get_path()

    def _idle(self, file_info):
        path = self._get_path(file_info)
        content_type = file_info.get_content_type()
        if content_type == "image/webp":
            pixbuf = WebpToPixbuf.from_path(path)
        else:
            pixbuf = GdkPixbuf.Pixbuf.new_from_file(path)
        self._raise("delta > pixbuf loaded", pixbuf)
        return GLib.SOURCE_REMOVE

    def load_async(self, file_info):
        GLib.idle_add(self._idle, file_info)

    def __init__(self, parent):
        self._parent = parent
