# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .bind_model.BindModel import DeltaBindModel
from .SetHeader import DeltaSetHeader


class EchoFactories:

    def __init__(self, parent):
        DeltaBindModel(parent)
        DeltaSetHeader(parent)
