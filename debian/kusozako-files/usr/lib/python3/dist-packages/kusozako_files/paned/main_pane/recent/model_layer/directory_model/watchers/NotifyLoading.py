# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako_files.const import FileManagerSignals


class DeltaNotifyLoading(DeltaEntity):

    def _on_notify(self, directory_model, param_spec):
        loading = directory_model.is_loading()
        user_data = FileManagerSignals.NOTIFY_LOADING, loading
        self._raise("delta > file manager signal", user_data)

    def __init__(self, parent):
        self._parent = parent
        directory_model = self._enquiry("delta > directory model")
        directory_model.connect("notify::loading", self._on_notify)
