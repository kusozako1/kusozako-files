# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako_files.const import ExtraOverlayPages
from .viewers.Viewers import DeltaViewers
from .LoadingState import DeltaLoadingState
from .action_bar.ActionBar import DeltaActionBar
from .navigation_bar.NavigationBar import DeltaNavigationBar


class DeltaContentWidget(Gtk.Overlay, DeltaEntity):

    def _delta_call_add_overlay(self, overlay):
        self.add_overlay(overlay)

    def _delta_call_add_to_container(self, widget):
        self.set_child(widget)

    def _on_get_child_position(self, overlay, widget, rectangle):
        rectangle.height = 48
        if widget.__class__.__name__ == "DeltaNavigationBar":
            return True, rectangle
        if widget.__class__.__name__ == "DeltaActionBar":
            rectangle.y = self.get_allocated_height() - 48
            return True, rectangle

    def _delta_info_sort_order_page(self):
        return ExtraOverlayPages.SORT_ORDER_TRASH_BIN

    def get_state(self):
        gfile = self._enquiry("delta > current gfile")
        return "trash-bin", gfile

    def __init__(self, parent):
        self._parent = parent
        Gtk.Overlay.__init__(self)
        DeltaViewers(self)
        DeltaNavigationBar(self)
        DeltaActionBar(self)
        DeltaLoadingState(self)
        self.connect("get-child-position", self._on_get_child_position)
