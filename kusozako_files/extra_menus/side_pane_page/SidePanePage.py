# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.overlay_item.sub_page.SubPage import AlfaSubPage
from kusozako_files.const import ExtraMenuPages
from kusozako1.overlay_item.Label import DeltaLabel
from kusozako1.overlay_item.Separator import DeltaSeparator
from .BookmarkButton import DeltaBookmarkButton
from .TreeViewButton import DeltaTreeViewButton
from .ShowFilesButton import DeltaShowFilesButton
from .ShowHiddenButton import DeltaShowHiddenButton


class DeltaSidePanePage(AlfaSubPage):

    PAGE_NAME = ExtraMenuPages.SIDE_PANE

    def _on_initialize(self):
        DeltaLabel.new_for_label(self, _("Type"))
        DeltaBookmarkButton(self)
        DeltaTreeViewButton(self)
        DeltaSeparator(self)
        DeltaLabel.new_for_label(self, _("Tree View"))
        DeltaShowFilesButton(self)
        DeltaShowHiddenButton(self)
