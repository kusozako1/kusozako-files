# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files.const import SidePaneTypes
from .SidePaneButton import AlfaSidePaneButton


class DeltaTreeViewButton(AlfaSidePaneButton):

    LABEL = _("Tree View")
    MATCH_VALUE = SidePaneTypes.TREE_VIEW
