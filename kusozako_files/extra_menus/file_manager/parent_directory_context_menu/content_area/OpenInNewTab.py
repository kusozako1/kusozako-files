# (c) copyright 2022-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.overlay_item.button.Button import AlfaButton
from kusozako1.const import MainWindowSignals
from kusozako_files.const import MainWindowSignals as Signals
from kusozako_files.const import ExtraOverlayPages


class DeltaOpenInNewTab(AlfaButton):

    LABEL = _("Open In New Tab")

    def _on_clicked(self, button):
        user_data = Signals.ADD_FILE_MANAGER, self._gfile.dup()
        self._raise("delta > main window signal", user_data)

    def receive_transmission(self, user_data):
        signal, signal_param = user_data
        if signal != MainWindowSignals.SHOW_EXTRA_PRIMARY_MENU_WITH_PARAM:
            return
        page_name, proxy = signal_param
        if page_name != ExtraOverlayPages.PARENT_DIRECTORY_CONTEXT_MENU:
            return
        self._gfile = proxy.get_selected_gfile()
        file_info = self._gfile.query_info("*", 0)
        content_type = file_info.get_content_type()
        self.set_visible(content_type == "inode/directory")

    def _on_initialize(self):
        self._raise("delta > register main window signal object", self)
