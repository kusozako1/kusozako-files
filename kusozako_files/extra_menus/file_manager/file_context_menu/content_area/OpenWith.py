# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.overlay_item.button.Button import AlfaButton
from kusozako1.const import MainWindowSignals
from kusozako_files.const import ExtraOverlayPages


class DeltaOpenWith(AlfaButton):

    LABEL = _("Open With...")
    END_ICON = "go-next-symbolic"

    def _on_clicked(self, button):
        param = ExtraOverlayPages.OPEN_WITH
        user_data = MainWindowSignals.MOVE_PRIMARY_MENU_PAGE, param
        self._raise("delta > main window signal", user_data)
