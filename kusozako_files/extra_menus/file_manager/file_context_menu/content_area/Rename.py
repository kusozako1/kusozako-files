# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.overlay_item.button.Button import AlfaButton
from kusozako1.const import MainWindowSignals
from kusozako_files.const import ExtraOverlayPages


class DeltaRename(AlfaButton):

    LABEL = _("Raname")
    END_ICON = "go-next-symbolic"

    def _on_clicked(self, button):
        gfile = self._enquiry("delta > selected gfile")
        signal_param = ExtraOverlayPages.RENAME, gfile
        user_data = MainWindowSignals.SHOW_EXTRA_OVERLAY, signal_param
        self._raise("delta > main window signal", user_data)
