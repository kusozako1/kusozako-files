# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.const import MainWindowSignals
from kusozako1.widget.overlay_item.Spacer import DeltaSpacer
from kusozako_files.const import ExtraOverlayPages
from .ProxySetter import DeltaProxySetter
from .content_area.ContentArea import DeltaContentArea


class DeltaFileContextMenu(Gtk.Box, DeltaEntity):

    def _delta_info_selection_model(self):
        return self._proxy.get_selection_model()

    def _delta_info_selected_gfile(self):
        return self._proxy.get_selected_gfile()

    def _delta_call_proxy_changed(self, proxy):
        self._proxy = proxy

    def _delta_call_add_to_container(self, widget):
        self.append(widget)

    def __init__(self, parent):
        self._parent = parent
        DeltaProxySetter(self)
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.HORIZONTAL)
        DeltaSpacer(self)
        DeltaContentArea(self)
        param = self, ExtraOverlayPages.FILE_CONTEXT_MENU_RECENT
        user_data = MainWindowSignals.ADD_EXTRA_PRIMARY_MENU, param
        self._raise("delta > main window signal", user_data)
