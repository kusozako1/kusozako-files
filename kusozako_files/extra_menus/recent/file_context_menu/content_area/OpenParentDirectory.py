# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.overlay_item.button.Button import AlfaButton
from kusozako_files.const import MainWindowSignals
from kusozako1.const import MainWindowSignals as Signals


class DeltaOpenParentDirectory(AlfaButton):

    LABEL = _("Open Parent Directory")

    def _close(self):
        user_data = Signals.CLOSE_OVERLAY, None
        self._raise("delta > main window signal", user_data)

    def _on_clicked(self, button):
        gfile = self._enquiry("delta > selected gfile")
        parent_gfile = gfile.get_parent()
        user_data = MainWindowSignals.ADD_FILE_MANAGER, parent_gfile
        self._raise("delta > main window signal", user_data)
        self._close()
