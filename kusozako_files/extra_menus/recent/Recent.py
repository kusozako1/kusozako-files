# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .file_context_menu.FileContextMenu import DeltaFileContextMenu
from .open_with_page.OpenWithPage import DeltaOpenWithPage


class EchoRecent:

    def __init__(self, parent):
        DeltaFileContextMenu(parent)
        DeltaOpenWithPage(parent)
