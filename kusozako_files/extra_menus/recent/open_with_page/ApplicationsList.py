# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files.const import ExtraOverlayPages
from kusozako_files.menu_widget.applications_list.ApplicationsList import (
    AlfaApplicationsList
    )


class DeltaApplicationsList(AlfaApplicationsList):

    TARGET_PAGE = ExtraOverlayPages.FILE_CONTEXT_MENU_RECENT
