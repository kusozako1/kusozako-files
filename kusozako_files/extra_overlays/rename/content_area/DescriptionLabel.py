# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.const import MainWindowSignals as Signals
from kusozako_files.const import ExtraOverlayPages as Pages

TEMPLATE = "original name : <i>{}</i>"


class DeltaDescriptionLabel(Gtk.Label, DeltaEntity):

    def _reset(self, gfile):
        basename = gfile.get_basename()
        self.set_markup(TEMPLATE.format(basename))

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal != Signals.SHOW_EXTRA_PRIMARY_MENU_WITH_PARAM:
            return
        page_name, proxy = param
        if page_name == Pages.FILE_CONTEXT_MENU:
            self._reset(proxy.get_selected_gfile())

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(self, label="don't look at me")
        self.set_size_request(-1, 32)
        self._raise("delta > add to container", self)
        self._raise("delta > register main window signal object", self)
