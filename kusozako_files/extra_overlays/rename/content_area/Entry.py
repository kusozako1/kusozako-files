# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.const import MainWindowSignals as Signals
from kusozako_files.const import ExtraOverlayPages as Pages


class DeltaEntry(Gtk.Entry, DeltaEntity):

    def _on_changed(self, entry):
        self._raise("delta > change name", entry.get_text())

    def _on_activate(self, entry):
        self._raise("delta > try rename")

    def _reset(self, gfile):
        self.grab_focus()
        basename = gfile.get_basename()
        stem = basename.rsplit(".", 1)[0]
        self.set_text(basename)
        self.set_position(len(stem))
        self.select_region(0, len(stem))

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal != Signals.SHOW_EXTRA_PRIMARY_MENU_WITH_PARAM:
            return
        page_name, proxy = param
        if page_name == Pages.FILE_CONTEXT_MENU:
            self._reset(proxy.get_selected_gfile())

    def __init__(self, parent):
        self._parent = parent
        box = Gtk.Box(
            orientation=Gtk.Orientation.HORIZONTAL,
            homogeneous=True,
            spacing=16
            )
        Gtk.Entry.__init__(self, margin_bottom=16)
        box.append(Gtk.Box())
        box.append(self)
        box.append(Gtk.Box())
        self._raise("delta > add to container", box)
        self._raise("delta > register main window signal object", self)
        self.connect("changed", self._on_changed)
        self.connect("activate", self._on_activate)
