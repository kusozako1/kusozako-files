# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files.const import ExtraOverlayPages
from kusozako_files.menu_widget.sort_order.SortOrder import AlfaSortOrder
from .DeletionDate import DeltaDeletionDate


class DeltaSortOrderTrashBin(AlfaSortOrder):

    TARGET_PAGE = ExtraOverlayPages.SORT_ORDER_TRASH_BIN

    def _delta_call_loopback_add_extra_sort_types(self, user_data):
        parent, proxy = user_data
        DeltaDeletionDate.new_for_proxy(parent, proxy)
