# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files.const import SortType
from .SortTypeButton import AlfaSortTypeButton


class DeltaDeletionDate(AlfaSortTypeButton):

    LABEL = _("Deletion Date")
    MATCH = SortType.DELETION_DATE
