# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from pymediainfo import MediaInfo
from gi.repository import Gio
from kusozako1.const import PixbufOptionKeys
from kusozako1.thumbnailer.Thumbnailer import FoxtrotThumbnailer
from kusozako1.Entity import DeltaEntity
from kusozako_files.utility.Checksum import FoxtrotChecksum


class DeltaPixbufSaver(DeltaEntity):

    def _reload_thumbnail(self, gfile, uri):
        fallback_gfile = Gio.File.new_for_uri(uri)
        tmp_gfile, _ = Gio.File.new_tmp("XXXXXXXX.png")
        gfile.move(tmp_gfile, Gio.FileCopyFlags.OVERWRITE)
        tmp_gfile.copy(fallback_gfile, Gio.FileCopyFlags.OVERWRITE)

    def _get_options(self, uri, file_info, video_track):
        options = {}
        options[PixbufOptionKeys.URI] = uri
        options[PixbufOptionKeys.SIZE] = str(file_info.get_size())
        mtime = file_info.get_attribute_uint64("time::modified")
        options[PixbufOptionKeys.MTIME] = str(mtime)
        options[PixbufOptionKeys.MIMETYPE] = file_info.get_content_type()
        options[PixbufOptionKeys.SOFTWARE] = PixbufOptionKeys.SOFTWARE_NAME
        options[PixbufOptionKeys.WIDTH] = str(video_track.width)
        options[PixbufOptionKeys.HEIGHT] = str(video_track.height)
        options[PixbufOptionKeys.LENGTH] = str(video_track.duration)
        return options

    def save(self, pixbuf):
        gfile = self._enquiry("delta > current gfile")
        media_info = MediaInfo.parse(gfile.get_path())
        video_track = media_info.video_tracks[0]
        uri = gfile.get_uri()
        file_info = gfile.query_info("*", 0)
        options = self._get_options(uri, file_info, video_track)
        path = self._checksum.get_thumbnail_path_for_uri(uri)
        pixbuf.savev(path, "png", list(options.keys()), list(options.values()))
        self._thumbnailer.change_thumbnail_cache(path, pixbuf)
        self._reload_thumbnail(gfile, uri)

    def __init__(self, parent):
        self._parent = parent
        self._checksum = FoxtrotChecksum.get_default()
        self._thumbnailer = FoxtrotThumbnailer.get_default()
