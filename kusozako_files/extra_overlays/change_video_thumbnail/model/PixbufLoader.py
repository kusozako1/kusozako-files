# (c) copyright 2022-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from pymediainfo import MediaInfo
from gi.repository import Gio
from gi.repository import GLib
from gi.repository import GdkPixbuf
from kusozako1.Entity import DeltaEntity


class DeltaPixbufLoader(DeltaEntity):

    def _create_pixbuf(self, success, destination_path):
        if not success:
            return
        pixbuf = GdkPixbuf.Pixbuf.new_from_file(destination_path)
        self._raise("delta > pixbuf loaded", pixbuf)

    def _finished(self, subprocess, task, destination_path):
        try:
            success, _, _ = subprocess.communicate_utf8_finish(task)
            self._create_pixbuf(success, destination_path)
        except GLib.Error:
            # when cancellable executed.
            return

    def _exec_command(self, position, source_path, size):
        gfile, _ = Gio.File.new_tmp("XXXXXX.jpeg")
        destination_path = gfile.get_path()
        command = [
            "ffmpegthumbnailer",
            "-i", source_path,
            "-o", destination_path,
            "-t", "{}%".format(position/100),
            "-s", size,
            ]
        subprocess = Gio.Subprocess.new(command, Gio.SubprocessFlags.NONE)
        subprocess.communicate_utf8_async(
            None,
            self._cancellable,
            self._finished,
            destination_path
            )

    def load_async(self):
        self._cancellable.cancel()
        self._cancellable = Gio.Cancellable()
        source_path = self._enquiry("delta > current path")
        media_info = MediaInfo.parse(source_path)
        video_track = media_info.video_tracks[0]
        edge_length = max(video_track.width, video_track.height)
        scale = 128/min(video_track.width, video_track.height)
        size = str(edge_length*scale)
        start_position = GLib.random_int_range(0, 500)
        for position in range(start_position, 10000, 500):
            self._exec_command(position, source_path, size)

    def __init__(self, parent):
        self._parent = parent
        self._cancellable = Gio.Cancellable()
