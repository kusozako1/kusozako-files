# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from .DrawingArea import DeltaDrawingArea


class FoxtrotListItemWidget(Gtk.AspectFrame):

    def bind(self, position, pixbuf):
        # self._status_label.bind(file_info)
        self._drawing_area.bind(pixbuf)

    def _on_get_child_position(self, overlay, widget, rectangle):
        rectangle.y = 0
        rectangle.x = 0
        rectangle.height = 32
        return True, rectangle

    def __init__(self, selection_model):
        Gtk.AspectFrame.__init__(
            self,
            margin_start=4,
            margin_end=4,
            margin_top=4,
            margin_bottom=4,
            )
        self._overlay = Gtk.Overlay(overflow=Gtk.Overflow.HIDDEN)
        self._overlay.add_css_class("card")
        self._drawing_area = DeltaDrawingArea()
        self._overlay.set_child(self._drawing_area)
        self._overlay.connect(
            "get-child-position",
            self._on_get_child_position
            )
        self.set_child(self._overlay)
