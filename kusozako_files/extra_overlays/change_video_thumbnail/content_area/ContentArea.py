# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .header.Header import DeltaHeader
from .grid_view.GridView import DeltaGridView
from .button_box.ButtonBox import DeltaButtonBox


class DeltaContentArea(Gtk.Box, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.append(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL)
        DeltaHeader(self)
        DeltaGridView(self)
        DeltaButtonBox(self)
        self.add_css_class("osd")
        self.set_size_request(-1, 600)
        self._raise("delta > add to container", self)
