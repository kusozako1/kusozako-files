# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity


class DeltaReloadButton(Gtk.Button, DeltaEntity):

    def _on_clicked(self, button):
        self._raise("delta > reload")

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(
            self,
            label=_("Reload"),
            )
        self.connect("clicked", self._on_clicked)
        self.add_css_class("kusozako-primary-widget")
        self._raise("delta > add to container", self)
