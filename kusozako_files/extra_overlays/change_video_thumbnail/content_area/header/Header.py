# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .Label import DeltaLabel
from .CloseButton import DeltaCloseButton


class DeltaHeader(Gtk.Overlay, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.set_child(widget)

    def _delta_call_add_overlay(self, widget):
        self.add_overlay(widget)

    def _on_get_child_position(self, overlay, widget, rectangle):
        rectangle.width = 32
        rectangle.height = 32
        rectangle.x = overlay.get_allocated_width() - 48
        rectangle.y = 16
        return True, rectangle

    def __init__(self, parent):
        self._parent = parent
        Gtk.Overlay.__init__(self)
        DeltaLabel(self)
        DeltaCloseButton(self)
        self.connect("get-child-position", self._on_get_child_position)
        self._raise("delta > add to container", self)
