# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity


class DeltaLabel(Gtk.Label, DeltaEntity):

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(
            self,
            label=_("Select New Thumbnail"),
            margin_top=32,
            margin_bottom=32,
            )
        self.add_css_class("kusozako-large-title")
        self._raise("delta > add to container", self)
