# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.const import MainWindowSignals as Signals
from kusozako_files.const import ExtraOverlayPages as Pages

TEMPLATE = "parent directory : <i>{}</i>"


class DeltaDescriptionLabel(Gtk.Label, DeltaEntity):

    def _reset(self, gfile):
        path = gfile.get_path()
        self.set_markup(TEMPLATE.format(path))

    def receive_transmission(self, user_data):
        signal, signal_param = user_data
        if signal != Signals.SHOW_EXTRA_OVERLAY:
            return
        page_name, gfile = signal_param
        if page_name == Pages.MAKE_DIRECTORY:
            self._reset(gfile)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(self, label="don't look at me")
        self.set_size_request(-1, 32)
        self._raise("delta > add to container", self)
        self._raise("delta > register main window signal object", self)
