# (c) copyright 2022-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

SORT_ORDER = "sort-order"
SORT_ORDER_RECENT = "sort-order-recent"
SORT_ORDER_TRASH_BIN = "sort-order-trash-bin"
RENAME = "rename"
MAKE_DIRECTORY = "make-directory"
CHANGE_VIDEO_THUMBNAIL = "change-video-thumbnail"
OPEN_WITH = "open-with"
OPEN_WITH_RECENT = "open-with-recent"
FILE_CONTEXT_MENU = "file-context-menu"
FILE_CONTEXT_MENU_RECENT = "file-context-menu-recent"
FILE_CONTEXT_MENU_TRASH_BIN = "file-context-menu-trash-bin"
PARENT_DIRECTORY_CONTEXT_MENU = "parent-directory-context-menu"
