# (c) copyright 2022-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

MOVE_DIRECTORY = "move-directory"                   # gfile
DIRECTORY_MOVED = "directory-moved"                 # gfile
SELECT_ALL = "select-all"                           # None
UNSELECT_ALL = "unselect-all"                       # None
INVERT_SELECTION = "invert-selection"               # None
TRASH_SELECTION = "trash-selection"                 # None
COPY_SELECTION = "copy-selection"                   # None
MOVE_SELECTION = "move-selection"                   # None
TOGGLE_VIEWER_SELECTION = "toggle-viwer-selection"  # None
RESTORE_SELECTION = "restore-selection"             # None, for trash-bin only
DELETE_SELECTION = "delete-selection"               # None, for trash-bin only
TOGGLE_PAGE_SETTING = "toggle-page-setting"         # key
CHANGE_PAGE_SETTING = "change-page-setting"         # key, value (any)
PAGE_SETTINGS_CHANGED = "page-settings-changed"     # key, value (any)
TOGGLE_INDEX = "toggle-index"                       # int
QUEUE_INDEX = "queue-index"                         # int
FILTER_KEYWORD_CHANGED = "filter-keyword-changed"   # str

# EXPERIMENTALs
TOGGLE_SELECTED = "toggle-selected"                 # None
# toggle index that selected by viewer model.
RELEASE_ACTIVATION_HOOK = "release-activation-hook"          # None
LONG_PRESS_CANCEL_HOOK = "long-press-cancel-hook"   # None
CALL_FILE_CONTEXT_MENU = "call-file-context-menu"   # None
CALL_DIRECTORY_CONTEXT_MENU = "call-directory-context-menu"  # None
SHOW_FILE_CONTEXT_MENU = "show-file-context-menu"   # None
SHOW_DIRECTORY_CONTEXT_MENU = "show-directory-context-menu"  # None
NOTIFY_LOADING = "notify-loading"                   # bool
LOAD_FINISHED = "load-finished"                     # n_items as int

# DEPRECATED
FILE_CREATED = "file-created"                       # file_info
FILE_DELETED = "file-deleted"                       # gfile
CURRENT_DIRECTORY_DELETED = "current-directory-deleted"      # gfile
