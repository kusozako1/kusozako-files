# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

FILE_NAME = "file-name"
LAST_MODIFIED = "last-modified"
LAST_ACCESSED = "last-accessed"
MIME_TYPE = "mime-type"
FILE_TYPE = "file-type"
FILE_SIZE = "file-size"
EXTENSION = "extension"
DELETION_DATE = "deletion-date"
