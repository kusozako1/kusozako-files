# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

SHOW_HIDDEN = "show-hidden"         # boolean
VIEWER_TYPE = "viwer-type"          # ViewerTypes as str
SORT_TYPE = "sort-type"             # SortType as str
SORT_ORDER = "sort-order"           # SortOrder as int (1 or -1)
SHOW_SEARCH = "show-search"         # boolean

ALL = (SHOW_HIDDEN, VIEWER_TYPE, SORT_TYPE, SORT_ORDER, SHOW_SEARCH)
