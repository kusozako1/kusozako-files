# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from . import GFile


def _on_finished(source_gfile, task, user_data=None):
    success = source_gfile.wait_finish(task)
    if not success:
        print("copy-failed", source_gfile.get_path())


def _get_destination_gfile(directory_gfile, file_info):
    copy_name = file_info.get_attribute_string("standard::copy-name")
    destination_gfile = directory_gfile.get_child(copy_name)
    return GFile.get_safe_destination_gfile(destination_gfile)


def copy_file_infos(directory_gfile, file_infos):
    for file_info in file_infos:
        source_gfile = file_info.get_attribute_object("standard::file")
        source_path = source_gfile.get_path()
        destination_gfile = _get_destination_gfile(directory_gfile, file_info)
        destination_path = destination_gfile.get_path()
        command = ["cp", "-r", source_path, destination_path]
        subprocess = Gio.Subprocess.new(command, Gio.SubprocessFlags.NONE)
        subprocess.wait_async(None, _on_finished, "")
