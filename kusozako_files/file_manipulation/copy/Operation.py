# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from kusozako1.Entity import DeltaEntity
from kusozako1.util import SafePath


class AlfaOperation(DeltaEntity):

    def _on_finished(self, gfile, task, user_data):
        raise NotImplementedError

    def _get_safe_gfile(self, unsafe_gfile):
        if not unsafe_gfile.query_exists(None):
            return unsafe_gfile
        unsafe_path = unsafe_gfile.get_path()
        safe_path = SafePath.get_path(unsafe_path)
        return Gio.File.new_for_path(safe_path)

    def copy_async(self, directory_gfile, file_info, file_infos):
        raise NotImplementedError

    def __init__(self, parent):
        self._parent = parent
