# (c) copyright 2022-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako_files.const import FileManagerSignals as Signals
from kusozako_files.const import PageSettingsKeys as Keys
from .observers.Observers import EchoObservers


class AlfaPageSettings(DeltaEntity):

    __own_group__ = "define own group here."

    def __getitem__(self, key):
        return self._settings[key]

    def _delta_call_change_setting(self, user_data):
        key, value = user_data
        if value == self._settings[key]:
            return
        self._settings[key] = value
        user_data = Signals.PAGE_SETTINGS_CHANGED, (key, value)
        self._on_page_settings_changed(key, value)
        self._raise("delta > file manager signal", user_data)

    def _on_page_settings_changed(self, key, value):
        group = "view" if key != Keys.SORT_TYPE else self.__own_group__
        settings = group, key, value
        self._raise("delta > settings", settings)

    def _on_initialize(self):
        self._settings = {}
        for key in Keys.ALL:
            group = "view" if key != Keys.SORT_TYPE else self.__own_group__
            query = group, key
            self._settings[key] = self._enquiry("delta > settings", query)

    def __init__(self, parent):
        self._parent = parent
        self._on_initialize()
        EchoObservers(self)
