# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .ChangePageSetting import DeltaChangePageSetting
from .TogglePageSetting import DeltaTogglePageSetting


class EchoObservers:

    def __init__(self, parent):
        DeltaChangePageSetting(parent)
        DeltaTogglePageSetting(parent)
