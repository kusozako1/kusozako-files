# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .PixbufHandler import DeltaPixbufHandler
from .FileInfoHandler import DeltaFileInfoHandler


class AlfaTooltip(DeltaEntity):

    __widget_query__ = "define widget query here."

    def _on_query_tooltip(self, widget, x, y, keyboard_mode, tooltip):
        box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=16)
        box.set_size_request(-1, 128)
        pixbuf_widget = self._pixbuf_handler.get_widget()
        if pixbuf_widget is not None:
            box.append(pixbuf_widget)
        file_info_widget = self._file_info_handler.get_widget()
        if file_info_widget is not None:
            box.append(file_info_widget)
        tooltip.set_custom(box)
        return True

    def __init__(self, parent):
        self._parent = parent
        self._pixbuf_handler = DeltaPixbufHandler(self)
        self._file_info_handler = DeltaFileInfoHandler(self)
        overlay = self._enquiry(self.__widget_query__)
        overlay.connect("query-tooltip", self._on_query_tooltip)
