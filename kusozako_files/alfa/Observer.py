# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity


class AlfaObserver(DeltaEntity):

    REGISTRATION = ""
    SIGNAL = None

    def _on_received(self, param=None):
        raise NotImplementedError

    def _on_initialize(self):
        pass

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal == self.SIGNAL:
            self._on_received(param)

    def __init__(self, parent):
        self._parent = parent
        self._on_initialize()
        self._raise(self.REGISTRATION, self)
