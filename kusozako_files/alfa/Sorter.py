# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako_files.const import PageSettingsKeys as Keys
from kusozako_files.const import FileManagerSignals as Signals


class AlfaSorter(DeltaEntity):

    SORT_TYPE = "define sort type here."

    def _get_compared(self, alfa, bravo):
        raise NotImplementedError

    def _sort_func(self, alfa, bravo, user_data=None):
        if not self._activated:
            return 0
        return self._get_compared(alfa, bravo) * self._order

    def _on_settings_changed(self, key, value):
        if key == Keys.SORT_TYPE:
            self._activated = (value == self.SORT_TYPE)
        if key == Keys.SORT_ORDER:
            self._order = value

    def receive_transmission(self, user_data):
        signal, signal_param = user_data
        if signal == Signals.PAGE_SETTINGS_CHANGED:
            self._on_settings_changed(*signal_param)

    def __init__(self, parent):
        self._parent = parent
        sort_type = self._enquiry("delta > page settings", Keys.SORT_TYPE)
        self._activated = (sort_type == self.SORT_TYPE)
        self._order = self._enquiry("delta > page settings", Keys.SORT_ORDER)
        self._sorter = Gtk.CustomSorter.new(self._sort_func)
        self._raise("delta > append", self._sorter)
        self._raise("delta > register file manager object", self)
