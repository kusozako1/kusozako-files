# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Gio
from gi.repository import GLib
from kusozako1.Entity import DeltaEntity
from kusozako_files.const import SidePaneTypes
from .watchers.Activate import DeltaActivate
from .factory.Factory import DeltaFactory
from .directory_model.DirectoryModel import DeltaDirectoryModel


class DeltaTreeView(Gtk.ListView, DeltaEntity):

    def _delta_info_list_view(self):
        return self

    def __init__(self, parent):
        self._parent = parent
        scrolled_window = Gtk.ScrolledWindow(hexpand=True, vexpand=True)
        gfile = Gio.File.new_for_path(GLib.get_home_dir())
        Gtk.ListView.__init__(
            self,
            model=DeltaDirectoryModel.new_for_gfile(self, gfile),
            factory=DeltaFactory(self),
            single_click_activate=True,
            )
        DeltaActivate(self)
        self.add_css_class("kusozako-osd")
        # self.add_css_class("kusozako-content-area-medium")
        scrolled_window.set_child(self)
        user_data = scrolled_window, SidePaneTypes.TREE_VIEW
        self._raise("delta > add to stack", user_data)
