# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity


class DeltaCollateKey(Gtk.CustomSorter, DeltaEntity):

    def _sort_func(self, alfa, bravo, user_data=None):
        alfa_key = alfa.get_attribute_string("kusozako1::collate-key")
        bravo_key = bravo.get_attribute_string("kusozako1::collate-key")
        return 1 if alfa_key >= bravo_key else -1

    def __init__(self, parent):
        self._parent = parent
        Gtk.CustomSorter.__init__(self)
        self.set_sort_func(self._sort_func)
        self._raise("delta > add sorter", self)
