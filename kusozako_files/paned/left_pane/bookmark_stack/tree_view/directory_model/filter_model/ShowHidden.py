# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity


class DeltaShowHidden(Gtk.CustomFilter, DeltaEntity):

    def _filter_func(self, file_info, user_data=None):
        if self._disabled:
            return True
        return not file_info.get_is_hidden()

    def receive_transmission(self, user_data):
        group, key, value = user_data
        if group != "side_pane_tree_view" or key != "show_hidden":
            return
        self._disabled = value
        self.changed(Gtk.FilterChange.DIFFERENT)

    def _setup_setting(self):
        query = "side_pane_tree_view", "show_hidden", False
        self._disabled = self._enquiry("delta > settings", query)
        self._raise("delta > register settings object", self)

    def __init__(self, parent):
        self._parent = parent
        Gtk.CustomFilter.__init__(self)
        self.set_filter_func(self._filter_func)
        self._setup_setting()
        self._raise("delta > add filter", self)
