# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .base_model.BaseModel import FoxtrotBaseModel
from .sort_model.SortModel import FoxtrotSortModel
from .filter_model.FilterModel import DeltaFilterModel
from .TreeModel import DeltaTreeModel


class DeltaDirectoryModel(DeltaEntity):

    @classmethod
    def new_for_gfile(cls, parent, gfile):
        instance = cls(parent)
        return instance.construct(gfile)

    def construct(self, gfile):
        base_model = FoxtrotBaseModel.new_for_gfile(gfile)
        sort_model = FoxtrotSortModel.new_for_model(base_model)
        filter_model = DeltaFilterModel.new_for_model(self, sort_model)
        tree_model = DeltaTreeModel.new_for_model(
            self,
            filter_model,
            self.__class__
            )
        return Gtk.MultiSelection.new(tree_model)

    def __init__(self, parent):
        self._parent = parent
