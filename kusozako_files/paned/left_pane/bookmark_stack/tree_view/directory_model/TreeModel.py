# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Gio
from kusozako1.Entity import DeltaEntity


class DeltaTreeModel(DeltaEntity):

    @classmethod
    def new_for_model(cls, parent, model, model_class):
        instance = cls(parent)
        return instance.construct(model, model_class)

    def _get_file_info(self, object_):
        if isinstance(object_, Gio.FileInfo):
            return object_
        return object_.get_item()

    def _create_model_func(self, object_, model_class):
        file_info = self._get_file_info(object_)
        if file_info.get_file_type() != Gio.FileType.DIRECTORY:
            return
        path = file_info.get_attribute_string("kusozako1::path")
        new_gfile = Gio.File.new_for_path(path)
        return model_class.new_for_gfile(self, new_gfile)

    def construct(self, base_model, model_class):
        tree_model = Gtk.TreeListModel.new(
            base_model,
            False,
            False,
            self._create_model_func,
            model_class,
            )
        return tree_model

    def __init__(self, parent):
        self._parent = parent
