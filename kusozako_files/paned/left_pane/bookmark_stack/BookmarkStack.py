# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako_files.const import SidePaneTypes
from .Settings import DeltaSettings
from .bookmark.Bookmark import DeltaBookmark
from .tree_view.TreeView import DeltaTreeView


class DeltaBookmarkStack(Gtk.Stack, DeltaEntity):

    def _delta_call_add_to_stack(self, user_data):
        widget, name = user_data
        self.add_named(widget, name)

    def _delta_call_switch_stack_to(self, page_name):
        self.set_visible_child_name(page_name)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Stack.__init__(self)
        DeltaBookmark(self)
        DeltaTreeView(self)
        DeltaSettings(self)
        user_data = self, SidePaneTypes.BOOKMARK
        self._raise("delta > add to stack", user_data)
