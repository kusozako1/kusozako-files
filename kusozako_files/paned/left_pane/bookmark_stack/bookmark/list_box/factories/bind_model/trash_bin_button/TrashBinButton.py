# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.icon import Icon
from kusozako1.Entity import DeltaEntity
from kusozako_files.const import MainWindowSignals
from .Parser import DeltaParser


class DeltaTrashBinButton(Gtk.Button, DeltaEntity):

    def _on_clicked(self, button):
        user_data = MainWindowSignals.ENSURE_TRASH_BIN, None
        self._raise("delta > main window signal", user_data)

    def _delta_call_parsed(self, n_items):
        label = "({})".format(n_items) if n_items > 0 else _("Empty")
        self._count_label.set_label(label)

    def _get_content_box(self):
        box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=8)
        image = Icon.get_image_for_name("user-trash-symbolic")
        box.append(image)
        label = Gtk.Label(label=_("Trash"), hexpand=True, xalign=0)
        box.append(label)
        self._count_label = Gtk.Label(label="?", hexpand=True, xalign=1)
        box.append(self._count_label)
        return box

    def get_type(self):
        return "places"

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(
            self,
            has_frame=False
            )
        self.add_css_class("kusozako-primary-widget")
        box = self._get_content_box()
        self.set_child(box)
        self.connect("clicked", self._on_clicked)
        DeltaParser(self)
