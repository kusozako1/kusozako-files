# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.icon import Icon
from kusozako1.Entity import DeltaEntity
from kusozako_files.const import MainWindowSignals


class DeltaRecentButton(Gtk.Button, DeltaEntity):

    def _on_clicked(self, button):
        user_data = MainWindowSignals.ENSURE_RECENT_FILES, None
        self._raise("delta > main window signal", user_data)

    def _get_content_box(self):
        box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=8)
        image = Icon.get_image_for_name("document-open-recent-symbolic")
        box.append(image)
        label = Gtk.Label(label=_("Recent"), hexpand=True, xalign=0)
        box.append(label)
        return box

    def get_type(self):
        return "places"

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(
            self,
            has_frame=False
            )
        self.add_css_class("kusozako-primary-widget")
        box = self._get_content_box()
        self.set_child(box)
        self.connect("clicked", self._on_clicked)
