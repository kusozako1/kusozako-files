# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .list_box.ListBox import DeltaListBox
from .model.Model import DeltaModel


class DeltaBookmark(Gtk.Box, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.append(widget)

    def _delta_info_model(self):
        return self._model

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL)
        self.add_css_class("kusozako-osd")
        # self.add_css_class("kusozako-content-area-medium")
        self._model = DeltaModel(self)
        DeltaListBox(self)
        self._raise("delta > add to stack", (self, "bookmark"))
