# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from kusozako1.Entity import DeltaEntity
from .Places import DeltaPlaces
from .XdgUserDirs import DeltaXdgUserDirs
from .GtkBookmark import DeltaGtkBookmark


class DeltaModel(Gio.ListStore, DeltaEntity):

    def _compare_func(self, alfa, bravo, user_data):
        alfa_type = alfa.get_attribute_string("kusozako1::type")
        bravo_type = bravo.get_attribute_string("kusozako1::type")
        if alfa_type != bravo_type:
            return 1 if alfa_type > bravo_type else -1
        return 1 if alfa.get_name() > bravo.get_name() else -1

    def _delta_call_bookmark_found(self, alfa):
        for bravo in self:
            alfa_path = alfa.get_attribute_string("kusozako1::uri")
            bravo_path = bravo.get_attribute_string("kusozako1::uri")
            if alfa_path == bravo_path:
                return
        self.insert_sorted(alfa, self._compare_func, "")

    def _delta_call_remove_xdg_user_dir(self, path_to_remove):
        for file_info in self:
            path = file_info.get_attribute_string("kusozako1::path")
            if path == path_to_remove:
                _, position = self.find(file_info)
                self.remove(position)
                break

    def __init__(self, parent):
        self._parent = parent
        Gio.ListStore.__init__(self, item_type=Gio.FileInfo)
        DeltaPlaces(self)
        DeltaXdgUserDirs(self)
        DeltaGtkBookmark(self)
