# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib
from kusozako1.Entity import DeltaEntity
from kusozako_files.const import MainWindowSignals


class DeltaXdgUserDirs(DeltaEntity):

    def _append(self, path):
        gfile = Gio.File.new_for_path(path)
        if not gfile.query_exists():
            return
        file_info = gfile.query_info("*", 0)
        file_info.set_attribute_string("kusozako1::path", path)
        file_info.set_attribute_string("kusozako1::uri", gfile.get_uri())
        file_info.set_attribute_string("kusozako1::type", "pm-xdg-user-dirs")
        self._raise("delta > bookmark found", file_info)

    def _set_all(self):
        for index in range(0, GLib.UserDirectory.N_DIRECTORIES):
            path = GLib.get_user_special_dir(index)
            self._append(path)

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal != MainWindowSignals.XDG_USER_DIRS_CHANGED:
            return
        path_to_remove, path_to_append = param
        self._raise("delta > remove xdg user dir", path_to_remove)
        self._append(path_to_append)

    def __init__(self, parent):
        self._parent = parent
        GLib.reload_user_special_dirs_cache()
        self._set_all()
        self._raise("delta > register main window signal object", self)
