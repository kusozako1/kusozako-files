# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib
from kusozako1.Entity import DeltaEntity


class DeltaGtkBookmark(DeltaEntity):

    def _read_line(self, line):
        gfile = Gio.File.new_for_uri(line)
        if not gfile.query_exists():
            return
        file_info = gfile.query_info("*", 0)
        file_info.set_attribute_string("kusozako1::path", gfile.get_path())
        file_info.set_attribute_string("kusozako1::uri", gfile.get_uri())
        file_info.set_attribute_string("kusozako1::type", "ps-gtk-bookmark")
        self._raise("delta > bookmark found", file_info)

    def _parse(self, path):
        gio_file = Gio.File.new_for_path(path)
        stream = Gio.DataInputStream.new(gio_file.read(None))
        while True:
            line, _ = stream.read_line_utf8(None)
            if line is None:
                break
            self._read_line(line)

    def __init__(self, parent):
        self._parent = parent
        for version in ["gtk-2.0", "gtk-3.0", "gtk-4.0"]:
            names = [GLib.get_user_config_dir(), version, "bookmarks"]
            path = GLib.build_filenamev(names)
            if GLib.file_test(path, GLib.FileTest.EXISTS):
                self._parse(path)
