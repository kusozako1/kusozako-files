# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from kusozako1.Entity import DeltaEntity

URIS = "Recent://", "Trash://"


class DeltaPlaces(DeltaEntity):

    def __init__(self, parent):
        self._parent = parent
        for uri in URIS:
            gfile = Gio.File.new_for_uri(uri)
            file_info = gfile.query_info("*", 0)
            file_info.set_attribute_string("kusozako1::path", "")
            file_info.set_attribute_string("kusozako1::uri", gfile.get_uri())
            file_info.set_attribute_string("kusozako1::type", "places")
            self._raise("delta > bookmark found", file_info)
