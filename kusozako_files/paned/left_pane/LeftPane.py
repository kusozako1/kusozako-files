# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako_files.const import MainWindowSignals
from .bookmark_stack.BookmarkStack import DeltaBookmarkStack
from .previewers.Previewers import DeltaPreviewers


class DeltaLeftPane(Gtk.Stack, DeltaEntity):

    def _delta_call_add_to_stack(self, user_data):
        widget, name = user_data
        self.add_named(widget, name)

    def _delta_call_switch_stack_to(self, page_name):
        self.set_visible_child_name(page_name)

    def receive_transmission(self, user_data):
        signal, _ = user_data
        if signal == MainWindowSignals.BACK_TO_BOOKMARK:
            self.set_visible_child_name("bookmark")

    def __init__(self, parent):
        self._parent = parent
        Gtk.Stack.__init__(self)
        DeltaBookmarkStack(self)
        DeltaPreviewers(self)
        self._raise("delta > add to container start", self)
        self._raise("delta > register main window signal object", self)
