# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .FileType import DeltaFileType
from .FileName import DeltaFileName


class CharlieSortModel(Gtk.SortListModel, DeltaEntity):

    @classmethod
    def new_for_model(cls, base_model):
        instance = cls()
        instance.set_model(base_model)
        return instance

    def _delta_call_add_sorter(self, sorter):
        self._multi_sorter.append(sorter)

    def __init__(self):
        self._parent = None
        Gtk.SortListModel.__init__(self, incremental=True)
        self._multi_sorter = Gtk.MultiSorter()
        self.set_sorter(self._multi_sorter)
        DeltaFileType(self)
        DeltaFileName(self)
