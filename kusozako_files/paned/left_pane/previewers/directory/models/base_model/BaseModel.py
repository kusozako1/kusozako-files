# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity


class DeltaBaseModel(Gtk.DirectoryList, DeltaEntity):

    def _has_item(self, gfile):
        for info in gfile.enumerate_children("*", 0):
            self._raise("delta > has item", True)
            return
        self._raise("delta > has item", False)

    def _get_uri(self, file_info):
        gfile = file_info.get_attribute_object("standard::file")
        if gfile.get_uri_scheme() == "file":
            return gfile.get_uri()
        if gfile.get_uri_scheme() == "trash":
            return "Trash://"
        target_uri = file_info.get_attribute_string("standard::target-uri")
        gfile = Gio.File.new_for_uri(target_uri)
        return gfile.get_uri()

    def set_file_info(self, file_info):
        uri = self._get_uri(file_info)
        gfile = Gio.File.new_for_uri(uri)
        self._has_item(gfile)
        self.set_file(gfile)

    def __init__(self, parent):
        self._parent = parent
        Gtk.DirectoryList.__init__(self, attributes="*")
