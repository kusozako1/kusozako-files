# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib

TEMPLATE = {
    "title": "<span>Title : {}</span>\n\n",
    "artist": "<span>Artist : {}\n\n</span>",
    "album": "<span>Album : {}\n\n</span>",
    }


class FoxtrotMarkup:

    def _build_line(self, key, unescaped_text):
        if not unescaped_text:
            return ""
        escaped_text = GLib.markup_escape_text(unescaped_text, -1)
        template = TEMPLATE[key]
        return template.format(escaped_text, -1)

    def _get_readable(self, total_seconds):
        hours = total_seconds // 3600
        minutes = total_seconds % 3600 // 60
        seconds = total_seconds % 60
        if hours == 0:
            return "Duration : {}:{:02}".format(minutes, seconds)
        return "Duration : {}:{:02}:{:02}".format(hours, minutes, seconds)

    def build_(self, title, artist, album, duration):
        markup = self._build_line("title", title)
        markup += self._build_line("artist", artist)
        markup += self._build_line("album", album)
        markup += self._get_readable(duration)
        return markup
