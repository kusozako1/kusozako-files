# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako_files.const import MainWindowSignals
from kusozako_files.const import PreviewerTypes

MESSAGES = {
    "inode/directory": PreviewerTypes.DIRECTORY,
    "application/pdf": PreviewerTypes.PDF,
    "image": PreviewerTypes.IMAGE,
    "text": PreviewerTypes.TEXT,
    "audio": PreviewerTypes.AUDIO,
    "video": PreviewerTypes.VIDEO,
    }


class DeltaDispatch(DeltaEntity):

    def _dispatch(self, file_info, content_type):
        for key, previewer_type in MESSAGES.items():
            if content_type.startswith(key):
                user_data = previewer_type, file_info
                self._raise("delta > show preview", user_data)
                return
        user_data = MainWindowSignals.BACK_TO_BOOKMARK, None
        self._raise("delta > main window signal", user_data)

    def receive_transmission(self, user_data):
        signal, file_info = user_data
        if signal != MainWindowSignals.SHOW_FILE_PROPERTY:
            return
        content_type = file_info.get_content_type()
        self._dispatch(file_info, content_type)

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register main window signal object", self)
