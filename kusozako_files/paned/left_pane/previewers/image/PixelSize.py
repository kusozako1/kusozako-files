# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity


class DeltaPixelSize(DeltaEntity, Gtk.Label):

    def set_pixbuf(self, pixbuf):
        width = pixbuf.get_width()
        height = pixbuf.get_height()
        label = "{} x {}".format(width, height)
        self.set_label(label)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(self)
        self.set_size_request(-1, 48)
        self._raise("delta > add to container", self)
