# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GtkSource


class FoxtrotBuffer(GtkSource.Buffer):

    def guess_language_for_uri(self, uri):
        language_manager = GtkSource.LanguageManager.get_default()
        language = language_manager.guess_language(uri, None)
        self.set_language(language)

    def __init__(self):
        GtkSource.Buffer.__init__(self)
        style_manager = GtkSource.StyleSchemeManager.get_default()
        style_scheme = style_manager.get_scheme("oblivion")
        self.set_style_scheme(style_scheme)
