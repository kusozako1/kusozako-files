# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib
from gi.repository import GtkSource


class FoxtrotLoader:

    def _loaded(self, loader, task, user_data=None):
        try:
            loader.load_finish(task)
        except GLib.Error:
            pass
            # print("loading is cancelled")

    def load_async_for_uri(self, uri, buffer_):
        self._cancellable.cancel()
        self._cancellable = Gio.Cancellable()
        gfile = Gio.File.new_for_uri(uri)
        source_file = GtkSource.File(location=gfile)
        loader = GtkSource.FileLoader.new(buffer_, source_file)
        loader.load_async(
            GLib.PRIORITY_DEFAULT,
            self._cancellable,          # cancellable
            None,                       # progress_callback
            None,                       # progress_callback data
            self._loaded,               # callback
            None,                       # callback data
            )

    def __init__(self):
        self._cancellable = Gio.Cancellable()
