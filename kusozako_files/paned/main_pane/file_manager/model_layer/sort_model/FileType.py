# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity


class DeltaFileType(Gtk.CustomSorter, DeltaEntity):

    def _sort_func(self, alfa, bravo, user_data=None):
        return int(bravo.get_file_type()) - int(alfa.get_file_type())

    def __init__(self, parent):
        self._parent = parent
        Gtk.CustomSorter.__init__(self)
        self.set_sort_func(self._sort_func)
