# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity


class DeltaLastAccessed(Gtk.CustomSorter, DeltaEntity):

    def _get_date_element(self, date_time):
        return [
            date_time.get_year(),
            date_time.get_month(),
            date_time.get_week_of_year(),
            date_time.get_day_of_month()
            ]

    def _sort_func(self, alfa, bravo, user_data=None):
        alfa_date_time = alfa.get_access_date_time()
        alfa_element = self._get_date_element(alfa_date_time)
        bravo_date_time = bravo.get_access_date_time()
        bravo_element = self._get_date_element(bravo_date_time)
        for index in range(0, 4):
            difference = alfa_element[index] - bravo_element[index]
            if difference != 0:
                return difference
        return 0

    def __init__(self, parent):
        self._parent = parent
        Gtk.CustomSorter.__init__(self)
        self.set_sort_func(self._sort_func)
