# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .NotifyFile import DeltaNotifyFile
from .NotifyLoading import DeltaNotifyLoading
from .LoadFinished import DeltaLoadFinished


class EchoWatchers:

    def __init__(self, parent):
        DeltaNotifyFile(parent)
        DeltaNotifyLoading(parent)
        DeltaLoadFinished(parent)
