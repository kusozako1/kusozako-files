# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako1.Entity import DeltaEntity
from kusozako_files.const import FileManagerSignals


class DeltaContextMenuHook(DeltaEntity):

    def _timeout(self):
        self._file_context_menu_called = False
        return GLib.SOURCE_REMOVE

    def _set_called(self):
        self._file_context_menu_called = True
        GLib.timeout_add(250, self._timeout)

    def _try_queue(self):
        if self._file_context_menu_called:
            user_data = FileManagerSignals.SHOW_FILE_CONTEXT_MENU, None
        else:
            user_data = FileManagerSignals.SHOW_DIRECTORY_CONTEXT_MENU, None
        self._raise("delta > file manager signal", user_data)

    def receive_transmission(self, user_data):
        signal, _ = user_data
        if signal == FileManagerSignals.CALL_FILE_CONTEXT_MENU:
            self._set_called()
        if signal == FileManagerSignals.CALL_DIRECTORY_CONTEXT_MENU:
            self._try_queue()

    def __init__(self, parent):
        self._parent = parent
        self._file_context_menu_called = False
        self._raise("delta > register file manager object", self)
