# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity


class DeltaProxy(DeltaEntity):

    def get_selected_gfile(self):
        return self._gfile

    def get_selection_model(self):
        model = self._enquiry("delta > selection model")
        return model, self._index

    def set_index(self, index):
        self._index = index
        selection_model = self._enquiry("delta > selection model")
        file_info = selection_model[self._index]
        self._gfile = file_info.get_attribute_object("standard::file")

    def __init__(self, parent):
        self._parent = parent
        self._index = None
