# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.file_dialog_2.FileDialog import DeltaFileDialog
from kusozako_files.const import MainWindowSignals
from kusozako_files.const import FileManagerSignals
from kusozako_files.alfa.Observer import AlfaObserver
from kusozako_files.file_manipulation.copy.Copy import DeltaCopy


class DeltaCopySelection(AlfaObserver):

    REGISTRATION = "delta > register file manager object"
    SIGNAL = FileManagerSignals.COPY_SELECTION

    def _try_open_destination_directory(self, parent_gfile, directory_gfile):
        if not parent_gfile.equal(directory_gfile):
            user_data = MainWindowSignals.ENSURE_FILE_MANAGER, directory_gfile
            self._raise("delta > main window signal", user_data)

    def _copy_files(self, parent_gfile, directory_gfile):
        selection_model = self._enquiry("delta > selection model")
        file_infos = selection_model.get_selected_file_infos()
        self._copy.copy_file_infos(directory_gfile, file_infos)

    def _delta_call_dialog_response(self, directory_gfile):
        parent_gfile = self._enquiry("delta > current gfile")
        self._copy_files(parent_gfile, directory_gfile)
        self._try_open_destination_directory(parent_gfile, directory_gfile)

    def _on_received(self, param=None):
        DeltaFileDialog.select_directory(
            self,
            title=_("Select Directory"),
            default_directory=self._enquiry("delta > current gfile"),
            )

    def _on_initialize(self):
        self._copy = DeltaCopy.get_default()
