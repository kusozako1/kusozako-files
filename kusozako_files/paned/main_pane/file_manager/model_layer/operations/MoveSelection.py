# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from kusozako_files.const import MainWindowSignals
from kusozako1.util import SafePath
from kusozako1.file_dialog_2.FileDialog import DeltaFileDialog
from kusozako_files.const import FileManagerSignals
from kusozako_files.alfa.Observer import AlfaObserver

PRIORITY = 0
COPY_FLAGS = Gio.FileCopyFlags


class DeltaMoveSelection(AlfaObserver):

    REGISTRATION = "delta > register file manager object"
    SIGNAL = FileManagerSignals.MOVE_SELECTION

    def _get_destination_gfile(self, directory_gfile, source_gfile):
        source_basename = source_gfile.get_basename()
        child_gfile = directory_gfile.get_child(source_basename)
        safe_target_path = SafePath.get_path(child_gfile.get_path())
        return Gio.File.new_for_path(safe_target_path)

    def _move_finished(self, source_gfile, task, user_data):
        success = source_gfile.move_finish(task)
        if success:
            pass
            # self._raise("delta > move finished", source_gfile.get_path())
        else:
            # self._raise("delta > move failed")
            pass

    def _move_file(self, directory_gfile, file_info):
        parent_gfile = self._enquiry("delta > current gfile")
        filename = file_info.get_name()
        source_gfile = parent_gfile.get_child(filename)
        source_gfile.move_async(
            self._get_destination_gfile(directory_gfile, source_gfile),
            COPY_FLAGS.ALL_METADATA | COPY_FLAGS.NOFOLLOW_SYMLINKS,
            PRIORITY,
            None,                   # cancellable
            None,                   # progress callback
            None,                   # data for progress callback
            self._move_finished,
            (directory_gfile,),
            )

    def _move_files(self, directory_gfile):
        model = self._enquiry("delta > selection model")
        for file_info in model.get_selected_file_infos():
            if not file_info.get_attribute_boolean("access::can-rename"):
                print("delta > can't move ", file_info.get_name())
            else:
                self._move_file(directory_gfile, file_info)

    def _delta_call_dialog_response(self, directory_gfile):
        parent_gfile = self._enquiry("delta > current gfile")
        if parent_gfile.equal(directory_gfile):
            return
        self._move_files(directory_gfile)
        user_data = MainWindowSignals.ENSURE_FILE_MANAGER, directory_gfile
        self._raise("delta > main window signal", user_data)

    def _on_received(self, param=None):
        DeltaFileDialog.select_directory(
            self,
            title=_("Select Directory"),
            default_directory=self._enquiry("delta > current gfile"),
            )
