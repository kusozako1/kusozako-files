# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files.alfa.tooltip.Tooltip import AlfaTooltip


class DeltaTooltip(AlfaTooltip):

    __widget_query__ = "delta > list view file item"
