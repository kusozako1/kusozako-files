# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako_files.const import FileItemSignals
from .Symlink import DeltaSymlink
from .Access import DeltaAccess
from .Executable import DeltaExecutable


class DeltaAccessProperties(Gtk.Box, DeltaEntity):

    def _bind(self, file_info, gfile):
        if self._file_info == file_info:
            return
        self._file_info = file_info
        self._symlink.bind(file_info, gfile)
        self._access.bind(file_info)
        self._executable.bind(file_info)

    def _remove_all(self):
        for widget in self:
            self.remove(widget)

    def _delta_call_add_tag_label(self, tag_label):
        tag = tag_label.get_tag()
        for item in self:
            if item.get_tag() == tag:
                self.remove(item)
        self.prepend(tag_label)

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal != FileItemSignals.BINDED:
            return
        _, file_info, gfile = param
        self._bind(file_info, gfile)

    def __init__(self, parent):
        self._parent = parent
        self._file_info = None
        Gtk.Box.__init__(
            self,
            hexpand=True,
            orientation=Gtk.Orientation.HORIZONTAL,
            spacing=8,
            )
        self._symlink = DeltaSymlink(self)
        self._access = DeltaAccess(self)
        self._executable = DeltaExecutable(self)
        self._raise("delta > add to container", self)
        self._raise("delta > register file item object", self)
