# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from .Label import AlfaLabel


class DeltaSize(AlfaLabel):

    def _bind(self, file_info):
        content_type = file_info.get_content_type()
        if content_type == "inode/directory":
            self.set_label("-")
        else:
            size = GLib.format_size(file_info.get_size())
            self.set_label("{}".format(size))
