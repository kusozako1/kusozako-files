# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import PangoCairo
from kusozako1.Entity import DeltaEntity
from kusozako1.util.LayoutFactory import FoxtrotLayoutFactory
from kusozako_files.const import FileItemSignals
from .FileInfoHandler import FoxtrotFileInfoHandler


class DeltaNameLabel(Gtk.DrawingArea, DeltaEntity):

    def _draw_func(self, drawing_area, cairo_context, width, height):
        markup = self._file_info_handler.get_markup()
        if markup is None:
            return
        layout = self._layout_factory.build_(cairo_context, 128, 128*0.5)
        layout.set_markup(markup)
        _, layout_height = layout.get_pixel_size()
        rgba = self._file_info_handler.get_rgba()
        cairo_context.set_source_rgba(*rgba)
        x = 0
        rectangle_height = layout_height+4*2
        y = height-rectangle_height
        cairo_context.rectangle(x, y, width, rectangle_height)
        cairo_context.fill()
        cairo_context.set_source_rgba(1, 1, 1, 1)
        cairo_context.move_to(4, 128-layout_height-4)
        PangoCairo.update_layout(cairo_context, layout)
        PangoCairo.show_layout(cairo_context, layout)

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal != FileItemSignals.BINDED:
            return
        _, file_info, _ = param
        self._file_info_handler.bind(file_info)
        self.queue_draw()

    def __init__(self, parent):
        self._parent = parent
        self._file_info_handler = FoxtrotFileInfoHandler()
        self._layout_factory = FoxtrotLayoutFactory.get_default()
        Gtk.DrawingArea.__init__(self)
        self.set_draw_func(self._draw_func)
        self._raise("delta > add overlay", self)
        self._raise("delta > register file item object", self)
