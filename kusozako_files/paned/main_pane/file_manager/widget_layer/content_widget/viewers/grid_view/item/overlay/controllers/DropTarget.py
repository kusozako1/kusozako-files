# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Gdk
from kusozako1.Entity import DeltaEntity
from kusozako_files.const import FileItemSignals
from kusozako_files.file_manipulation import Move


class DeltaDropTarget(Gtk.DropTarget, DeltaEntity):

    def _on_accept(self, drop_target, drop):
        # returns accept drop or not, before drop event handler.
        file_info = self._gfile.query_info("*", 0)
        if file_info.get_content_type() != "inode/directory":
            return False
        if not file_info.get_attribute_boolean("access::can-write"):
            return False
        return True

    def _on_drop(self, drop_target, file_list, x, y):
        Move.move_gfiles(self._gfile, file_list.get_files())
        return True

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal != FileItemSignals.BINDED:
            return
        _, _, gfile = param
        self._gfile = gfile.dup()

    def __init__(self, parent):
        self._parent = parent
        Gtk.DropTarget.__init__(
            self,
            formats=Gdk.ContentFormats.new_for_gtype(Gdk.FileList),
            actions=Gdk.DragAction.COPY | Gdk.DragAction.MOVE,
            )
        self.connect("accept", self._on_accept)
        self.connect("drop", self._on_drop)
        self._raise("delta > add controller", self)
        self._raise("delta > register file item object", self)
