# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .Key import DeltaKey
from .Click import DeltaClick
from .LongPress import DeltaLongPress
from .RightClick import DeltaRightClick
from .DragSource import DeltaDragSource
from .DropTarget import DeltaDropTarget


class EchoControllers:

    def __init__(self, parent):
        DeltaKey(parent)
        DeltaClick(parent)
        DeltaLongPress(parent)
        DeltaRightClick(parent)
        DeltaDragSource(parent)
        DeltaDropTarget(parent)
