# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .GestureClick import DeltaGestureClick
from .DropTarget import DeltaDropTarget


class EchoControllers:

    def __init__(self, parent):
        self._parent = parent
        DeltaGestureClick(parent)
        DeltaDropTarget(parent)
