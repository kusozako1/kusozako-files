# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .status_label.StatusLabel import DeltaStatusLabel
from .name_label.NameLabel import DeltaNameLabel
from .drawing_area.DrawingArea import DeltaDrawingArea


class EchoWidgets:

    def __init__(self, parent):
        DeltaDrawingArea(parent)
        DeltaStatusLabel(parent)
        DeltaNameLabel(parent)
