# (c) copyright 2022-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .omni_bar.OmniBar import DeltaOmniBar
from .view_buttons.ViewButtons import EchoViewButtons


class DeltaNavigationBar(Gtk.Box, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.append(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.HORIZONTAL)
        self.set_size_request(-1, 48)
        self.add_css_class("kusozako-primary-surface")
        DeltaOmniBar(self)
        EchoViewButtons(self)
        self._raise("delta > add overlay", self)
