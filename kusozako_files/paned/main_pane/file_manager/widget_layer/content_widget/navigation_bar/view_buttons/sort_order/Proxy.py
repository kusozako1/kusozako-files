# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako_files.const import FileManagerSignals as Signals


class DeltaProxy(DeltaEntity):

    def set_callback(self, callback):
        self._callbacks.append(callback)

    def change_settings(self, key, value):
        user_data = Signals.CHANGE_PAGE_SETTING, (key, value)
        self._raise("delta > file manager signal", user_data)

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal != Signals.PAGE_SETTINGS_CHANGED:
            return
        for callback in self._callbacks:
            callback(param)

    def __getitem__(self, key):
        return self._enquiry("delta > page settings", key)

    def __init__(self, parent):
        self._parent = parent
        self._callbacks = []
        self._raise("delta > register file manager object", self)
