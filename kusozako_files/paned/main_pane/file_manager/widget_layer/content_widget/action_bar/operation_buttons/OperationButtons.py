# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .CopyButton import DeltaCopyButton
from .MoveButton import DeltaMoveButton
from .TrashButton import DeltaTrashButton


class EchoOperationButtons:

    def __init__(self, parent):
        DeltaCopyButton(parent)
        DeltaMoveButton(parent)
        DeltaTrashButton(parent)
