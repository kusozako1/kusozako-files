# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.icon import Icon
from kusozako1.Entity import DeltaEntity
from kusozako_files.const import FileManagerSignals


class AlfaOperationButton(Gtk.Button, DeltaEntity):

    __icon_name__ = "define icon name here."
    __tooltip_text__ = "define tooltip text here."
    __file_namager_signal__ = "define file manager signal here."

    def _get_sensitive(self, selection_model):
        raise NotImplementedError()

    def _on_selection_changed(self, model, position, n_items):
        sensitive = self._get_sensitive(model)
        self.set_sensitive(sensitive)

    def _on_clicked(self, button):
        user_data = self.__file_namager_signal__, None
        self._raise("delta > file manager signal", user_data)

    def receive_transmission(self, user_data):
        signal, _ = user_data
        if signal == FileManagerSignals.LOAD_FINISHED:
            self.set_sensitive(False)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(
            self,
            has_frame=False,
            sensitive=False,
            tooltip_text=self.__tooltip_text__,
            )
        image = Icon.get_image_for_name(self.__icon_name__)
        self.set_child(image)
        self.add_css_class("kusozako-primary-widget")
        self.connect("clicked", self._on_clicked)
        self._raise("delta > add to container", self)
        model = self._enquiry("delta > selection model")
        model.connect("selection-changed", self._on_selection_changed)
        self._raise("delta > register file manager object", self)
