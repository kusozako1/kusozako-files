# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from .observers.Observers import EchoObservers


class DeltaTabPageHandler(DeltaEntity):

    @classmethod
    def new(cls, parent, tab_page):
        instance = cls(parent)
        instance.construct(tab_page)

    def _delta_call_label_changed(self, label):
        self._tab_page.set_title(label)

    def _delta_call_icon_changed(self, gicon):
        self._tab_page.set_icon(gicon)

    def construct(self, tab_page):
        self._tab_page = tab_page
        EchoObservers(self)

    def __init__(self, parent):
        self._parent = parent
