# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Adw
from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .file_manager.FileManager import DeltaFileManager
from .trash_bin.TrashBin import DeltaTrashBin
from .recent.Recent import DeltaRecent
from .observers.Observers import EchoObservers
from .OverviewHandler import DeltaOverviewHandler
from .TabBarHandler import DeltaTabBarHandler


class DeltaMainPane(Gtk.Box, DeltaEntity):

    def _delta_info_tab_view(self):
        return self._tab_view

    def _delta_call_add_file_manager(self, gfile=None):
        DeltaFileManager.new(self, gfile)

    def _delta_call_add_trash_bin(self):
        DeltaTrashBin.new(self)

    def _delta_call_add_recent(self):
        DeltaRecent.new(self)

    def __init__(self, parent):
        self._parent = parent
        overlay = Gtk.Overlay(hexpand=True, vexpand=True)
        Gtk.Box.__init__(
            self,
            orientation=Gtk.Orientation.VERTICAL,
            hexpand=True,
            vexpand=True,
            )
        self._tab_view = Adw.TabView.new()
        self.prepend(self._tab_view)
        overview = DeltaOverviewHandler.new(self)
        overlay.add_overlay(overview)
        tab_bar = DeltaTabBarHandler.new(self, overview)
        self.prepend(tab_bar)
        DeltaFileManager.new(self)
        EchoObservers(self)
        overlay.set_child(self)
        self._raise("delta > add to container end", overlay)
