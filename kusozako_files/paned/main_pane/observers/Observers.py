# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .AddFileManager import DeltaAddFileManager
from .FileOpened import DeltaFileOpened
from .EnsureFileManager import DeltaEnsureFileManager
from .EnsureTrashBin import DeltaEnsureTrashBin
from .EnsureRecentFiles import DeltaEnsureRecentFiles


class EchoObservers:

    def __init__(self, parent):
        DeltaFileOpened(parent)         # kusozako1 signal handler
        DeltaAddFileManager(parent)
        DeltaEnsureFileManager(parent)
        DeltaEnsureTrashBin(parent)
        DeltaEnsureRecentFiles(parent)
