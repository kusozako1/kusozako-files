# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Adw
from kusozako1.Entity import DeltaEntity


class DeltaOverviewHandler(DeltaEntity):

    @classmethod
    def new(cls, parent):
        instance = cls(parent)
        return instance.construct()

    def _on_notify_open(self, overview, param_spec):
        is_opened = overview.get_open()
        overview.set_can_target(is_opened)

    def _on_create_tab(self, overview):
        self._raise("delta > add file manager", None)

    def construct(self):
        overview = Adw.TabOverview(
            view=self._enquiry("delta > tab view"),
            can_target=False,
            show_end_title_buttons=False,
            enable_new_tab=True,
            )
        overview.connect("notify::open", self._on_notify_open)
        overview.connect("create-tab", self._on_create_tab)
        return overview

    def __init__(self, parent):
        self._parent = parent
