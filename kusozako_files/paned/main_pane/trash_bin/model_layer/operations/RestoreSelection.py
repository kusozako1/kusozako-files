# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from kusozako1.util import SafePath
from kusozako_files.const import FileManagerSignals
from kusozako_files.alfa.Observer import AlfaObserver


class DeltaRestoreSelection(AlfaObserver):

    REGISTRATION = "delta > register file manager object"
    SIGNAL = FileManagerSignals.RESTORE_SELECTION

    def _on_received(self, param=None):
        model = self._enquiry("delta > selection model")
        file_infos = model.get_selected_file_infos()
        for file_info in file_infos:
            target_uri = file_info.get_attribute_string("standard::target-uri")
            source_gfile = Gio.File.new_for_uri(target_uri)
            orig_path = file_info.get_attribute_byte_string("trash::orig-path")
            safe_path = SafePath.get_path(orig_path)
            orig_gfile = Gio.File.new_for_path(safe_path)
            source_gfile.move(
                orig_gfile,
                Gio.FileCopyFlags.ALL_METADATA,
                None,
                None,
                None
                )
