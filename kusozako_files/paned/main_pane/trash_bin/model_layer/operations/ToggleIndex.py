# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files.const import FileManagerSignals
from kusozako_files.alfa.Observer import AlfaObserver
from .LongPressCancelHook import DeltaLongPressCancelHook


class DeltaToggleIndex(AlfaObserver):

    REGISTRATION = "delta > register file manager object"
    SIGNAL = FileManagerSignals.TOGGLE_INDEX

    def _toggle(self, index):
        model = self._enquiry("delta > selection model")
        if model.is_selected(index):
            model.unselect_item(index)
        else:
            model.select_item(index, False)

    def _on_received(self, index):
        if self._long_press_cancel_hook.try_release_hook():
            return
        self._toggle(index)

    def _on_initialize(self):
        self._long_press_cancel_hook = DeltaLongPressCancelHook(self)
