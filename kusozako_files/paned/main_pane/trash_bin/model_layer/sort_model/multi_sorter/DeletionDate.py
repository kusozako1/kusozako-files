# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files.const import SortType
from kusozako_files.alfa.Sorter import AlfaSorter


class DeltaDeletionDate(AlfaSorter):

    SORT_TYPE = SortType.DELETION_DATE

    def _get_compared(self, alfa, bravo):
        alfa_date_time = alfa.get_deletion_date()
        if alfa_date_time is None:
            return 0
        alfa_time = alfa_date_time.to_unix()
        bravo_time = bravo.get_deletion_date().to_unix()
        result = alfa_time-bravo_time
        return result*-1
