# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity


class DeltaSectionSorter(Gtk.CustomSorter, DeltaEntity):

    def _compare(self, alfa_ymd, bravo_ymd):
        for index in range(0, 3):
            compare = bravo_ymd[index] - alfa_ymd[index]
            if compare != 0:
                return compare
        return 0

    def _sort_func(self, alfa, bravo, user_data=None):
        alfa_date = alfa.get_deletion_date()
        if alfa_date is None:
            return 0
        bravo_date = bravo.get_deletion_date()
        alfa_ymd = alfa_date.get_ymd()
        bravo_ymd = bravo_date.get_ymd()
        return self._compare(alfa_ymd, bravo_ymd)

    def __init__(self, parent):
        self._parent = parent
        Gtk.CustomSorter.__init__(self)
        self.set_sort_func(self._sort_func)
