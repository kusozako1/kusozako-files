# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako_files.const import FileManagerSignals


class DeltaLabel(DeltaEntity):

    def _reset(self, gfile=None):
        if gfile is not None:
            label = gfile.get_basename()
            self._raise("delta > label changed", label)

    def receive_transmission(self, user_data):
        signal, gfile = user_data
        if signal == FileManagerSignals.DIRECTORY_MOVED:
            self._reset(gfile)

    def __init__(self, parent):
        self._parent = parent
        gfile = self._enquiry("delta > current gfile")
        self._reset(gfile)
        self._raise("delta > register file manager object", self)
