# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from kusozako1.Entity import DeltaEntity
from .observers.Observers import EchoObservers


class DeltaTabPageHandler(DeltaEntity):

    @classmethod
    def new(cls, parent, tab_page):
        instance = cls(parent)
        instance.construct(tab_page)

    def _delta_call_label_changed(self, label):
        if label == "/":
            label = _("Trash Bin")
        self._tab_page.set_title(label)

    def construct(self, tab_page):
        self._tab_page = tab_page
        gicon = Gio.Icon.new_for_string("user-trash-symbolic")
        self._tab_page.set_icon(gicon)
        EchoObservers(self)

    def __init__(self, parent):
        self._parent = parent
