# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako_files.const import FileManagerSignals


class DeltaLoadingState(Gtk.Revealer, DeltaEntity):

    def _signal_received(self, loading):
        self.set_reveal_child(loading)
        self.set_can_target(loading)
        label = _("NOW LOADING...") if loading else ""
        self._label.set_label(label)

    def receive_transmission(self, user_data):
        signal, loading = user_data
        if signal != FileManagerSignals.NOTIFY_LOADING:
            return
        self._signal_received(loading)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Revealer.__init__(
            self,
            can_target=False,
            transition_type=Gtk.RevealerTransitionType.CROSSFADE,
            )
        self.set_reveal_child(True)
        self._label = Gtk.Label()
        self._label = Gtk.Label(label=_("NOW LOADING..."))
        self._label.add_css_class("kusozako-large-title")
        self._label.add_css_class("osd")
        self.set_child(self._label)
        self._raise("delta > add overlay", self)
        self._raise("delta > register file manager object", self)
