# (c) copyright 2022-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.const import MainWindowSignals as Signals
from .Proxy import DeltaProxy
from .Icon import DeltaIcon


class DeltaSortOrder(Gtk.Button, DeltaEntity):

    def _on_clicked(self, button):
        sort_order_page = self._enquiry("delta > sort order page")
        param = sort_order_page, DeltaProxy(self)
        user_data = Signals.SHOW_EXTRA_OVERLAY, param
        self._raise("delta > main window signal", user_data)

    def _delta_call_add_to_container(self, widget):
        self._box.append(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(
            self,
            has_frame=False,
            tooltip_text=_("Change Sort Order"),
            can_focus=False,
            vexpand=False,
            )
        self._box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        DeltaIcon(self)
        self.set_child(self._box)
        self.add_css_class("kusozako-primary-widget")
        self.connect("clicked", self._on_clicked)
        self._raise("delta > add to container", self)
