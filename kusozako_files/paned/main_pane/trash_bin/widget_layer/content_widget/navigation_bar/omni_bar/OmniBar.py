# (c) copyright 2022-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako_files.const import FileManagerSignals
from kusozako_files.const import PageSettingsKeys
from .path_bar.PathBar import DeltaPathBar
from .SearchEntry import DeltaSearchEntry
from .toggle_button.ToggleButton import DeltaToggleButton


class DeltaOmniBar(Gtk.Stack, DeltaEntity):

    def _change_page(self, show_search):
        if show_search:
            self.set_visible_child_name("search-entry")
        else:
            self.set_visible_child_name("path-bar")

    def _delta_call_add_to_stack(self, user_data):
        widget, page_name = user_data
        self.add_named(widget, page_name)

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal != FileManagerSignals.PAGE_SETTINGS_CHANGED:
            return
        key, show_search = param
        if key != PageSettingsKeys.SHOW_SEARCH:
            return
        self._change_page(show_search)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Stack.__init__(self, hexpand=True)
        self._raise("delta > add to container", self)
        DeltaPathBar(self)
        DeltaSearchEntry(self)
        DeltaToggleButton(self)
        self._raise("delta > register file manager object", self)
