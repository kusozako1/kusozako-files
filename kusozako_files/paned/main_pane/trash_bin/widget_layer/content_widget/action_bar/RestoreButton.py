# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako_files.const import FileManagerSignals


class DeltaRestoreButton(Gtk.Button, DeltaEntity):

    def _get_sensitive(self, model):
        for file_info in model.get_selected_file_infos():
            if file_info.get_attribute_boolean("access::can-delete"):
                return True
        return False

    def _on_selection_changed(self, model, position, n_items):
        sensitive = self._get_sensitive(model)
        self.set_sensitive(sensitive)

    def _on_clicked(self, button):
        user_data = FileManagerSignals.RESTORE_SELECTION, None
        self._raise("delta > file manager signal", user_data)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(
            self,
            has_frame=False,
            sensitive=False,
            tooltip_text=_("Restore"),
            icon_name="edit-undo-symbolic",
            )
        self.add_css_class("kusozako-primary-widget")
        self.connect("clicked", self._on_clicked)
        self._raise("delta > add to container", self)
        model = self._enquiry("delta > selection model")
        model.connect("selection-changed", self._on_selection_changed)
