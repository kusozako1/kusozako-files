# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files.alfa.grid_view_item_overlay.GridViewItemOverlay import (
    AlfaGridViewItemOverlay
    )
from .status_label.StatusLabel import DeltaStatusLabel
from .name_label.NameLabel import DeltaNameLabel
from .drawing_area.DrawingArea import DeltaDrawingArea
from .GestureClick import DeltaGestureClick


class DeltaOverlay(AlfaGridViewItemOverlay):

    def _on_setup(self):
        DeltaGestureClick(self)
        DeltaDrawingArea(self)
        DeltaStatusLabel(self)
        DeltaNameLabel(self)
