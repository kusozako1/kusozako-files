# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files.const import FileManagerSignals
from kusozako_files.viewer.GridView import AlfaGridView
from .item.Item import TangoItem
from .controllers.Controllers import EchoControllers


class DeltaGridView(AlfaGridView):

    __context_menu_signal__ = FileManagerSignals.CALL_FILE_CONTEXT_MENU

    def _get_list_item_widget(self, selection_model):
        return TangoItem(selection_model)

    def _setup_controllers(self):
        EchoControllers(self)
