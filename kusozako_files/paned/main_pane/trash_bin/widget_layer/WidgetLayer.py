# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from .content_widget.ContentWidget import DeltaContentWidget
from .tab_page_handler.TabPageHandler import DeltaTabPageHandler


class DeltaWidgetLayer(DeltaEntity):

    def _delta_call_close_tab(self):
        self._raise("delta > detach tab", self._content_widget)

    def __init__(self, parent):
        self._parent = parent
        self._content_widget = DeltaContentWidget(self)
        tab_view = self._enquiry("delta > tab view")
        tab_page = tab_view.append(self._content_widget)
        tab_view.set_selected_page(tab_page)
        DeltaTabPageHandler.new(self, tab_page)
