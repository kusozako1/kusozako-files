# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from kusozako1.Entity import DeltaEntity


class DeltaTabPageHandler(DeltaEntity):

    @classmethod
    def new(cls, parent, tab_page):
        instance = cls(parent)
        instance.construct(tab_page)

    def construct(self, tab_page):
        gicon = Gio.Icon.new_for_string("document-open-recent-symbolic")
        tab_page.set_icon(gicon)
        tab_page.set_title(_("Recent"))

    def __init__(self, parent):
        self._parent = parent
