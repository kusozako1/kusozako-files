# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.icon import Icon
from kusozako1.Entity import DeltaEntity
from kusozako_files.const import FileManagerSignals as Signals
from kusozako_files.const import PageSettingsKeys as Keys
from kusozako_files.const import SortOrder


class DeltaIcon(Gtk.Image, DeltaEntity):

    def _reset(self, sort_order):
        if sort_order == SortOrder.ASCENDING:
            icon_name = "view-sort-ascending-symbolic"
            self.remove_css_class("kusozako-button-indicator-warning")
        else:
            icon_name = "view-sort-descending-symbolic"
            self.add_css_class("kusozako-button-indicator-warning")
        paintable = Icon.get_paintable_for_name(icon_name)
        self.set_from_paintable(paintable)

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal != Signals.PAGE_SETTINGS_CHANGED:
            return
        key, sort_order = param
        if key != Keys.SORT_ORDER:
            return
        self._reset(sort_order)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Image.__init__(self, vexpand=True)
        sort_order = self._enquiry("delta > page settings", Keys.SORT_ORDER)
        self._reset(sort_order)
        self._raise("delta > add to container", self)
        self._raise("delta > register file manager object", self)
