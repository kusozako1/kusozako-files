# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako_files.const import FileManagerSignals as Signals
from kusozako_files.const import PageSettingsKeys as Keys
from kusozako_files.const import ViewerTypes
from .Icon import DeltaIcon


class DeltaListViewButton(Gtk.Button, DeltaEntity):

    def _on_clicked(self, button):
        signal_param = Keys.VIEWER_TYPE, ViewerTypes.LIST_VIEW
        user_data = Signals.CHANGE_PAGE_SETTING, signal_param
        self._raise("delta > file manager signal", user_data)

    def _delta_call_add_to_container(self, widget):
        self.set_child(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(
            self,
            has_frame=False,
            tooltip_text=_("List View"),
            can_focus=False,
            vexpand=False,
            )
        DeltaIcon(self)
        self.add_css_class("kusozako-primary-widget")
        self.connect("clicked", self._on_clicked)
        self._raise("delta > add to container", self)
