# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Pango
from kusozako1.Entity import DeltaEntity


class DeltaLabel(Gtk.Label, DeltaEntity):

    def _on_selection_changed(self, model, position, n_items):
        selection = model.get_selection()
        size = selection.get_size()
        if size == 1:
            label = _("1 item selected.")
        else:
            label = _("{} items selected.".format(size))
        self.set_label(label)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(
            self,
            hexpand=True,
            ellipsize=Pango.EllipsizeMode.END
            )
        self._raise("delta > add to container", self)
        model = self._enquiry("delta > selection model")
        model.connect("selection-changed", self._on_selection_changed)
