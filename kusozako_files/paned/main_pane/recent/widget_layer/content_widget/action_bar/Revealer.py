# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import GLib
from kusozako1.Entity import DeltaEntity
from kusozako_files.const import FileManagerSignals


class DeltaRevealer(Gtk.Revealer, DeltaEntity):

    def _timeout(self, model):
        selection = model.get_selection()
        if selection.get_size() > 0:
            self.set_reveal_child(True)
        return GLib.SOURCE_REMOVE

    def _on_selection_changed(self, model, position, n_items):
        selection = model.get_selection()
        if selection.get_size() > 0:
            GLib.timeout_add(250, self._timeout, model)
        else:
            self.set_reveal_child(False)

    def _on_items_changed(self, model, position, removed, added):
        selection = model.get_selection()
        if selection.get_size() > 0:
            GLib.timeout_add(250, self._timeout, model)
        else:
            self.set_reveal_child(False)

    def receive_transmission(self, user_data):
        signal, gfile = user_data
        if signal == FileManagerSignals.DIRECTORY_MOVED:
            self.set_reveal_child(False)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Revealer.__init__(self)
        self.set_valign(Gtk.Align.END)
        model = self._enquiry("delta > selection model")
        model.connect("selection-changed", self._on_selection_changed)
        model.connect("items-changed", self._on_items_changed)
        self._raise("delta > register file manager object", self)
