# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako_files.const import FileManagerSignals

FILE_MANAGER_SIGNALS = {
    "Escape": FileManagerSignals.UNSELECT_ALL,
    "Ctrl+A": FileManagerSignals.SELECT_ALL,
    "Delete": FileManagerSignals.TRASH_SELECTION,
    "Ctrl+C": FileManagerSignals.COPY_SELECTION,
    "Ctrl+X": FileManagerSignals.MOVE_SELECTION,
    "Space": FileManagerSignals.TOGGLE_SELECTED,
    "Return": FileManagerSignals.RELEASE_ACTIVATION_HOOK,
    }


class DeltaKey(Gtk.EventControllerKey, DeltaEntity):

    def _on_key_released(self, controller, keyval, keycode, modifier):
        accel = Gtk.accelerator_get_label(keyval, modifier)
        signal = FILE_MANAGER_SIGNALS.get(accel, None)
        if signal is None:
            return
        user_data = signal, None
        self._raise("delta > file manager signal", user_data)

    def __init__(self, parent):
        self._parent = parent
        Gtk.EventControllerKey.__init__(self)
        self.connect("key-released", self._on_key_released)
        self._raise("delta > add controller", self)
