# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako_files.alfa.GridViewFileItem import AlfaGridViewFileItem
from .overlay.Overlay import DeltaOverlay


class TangoItem(AlfaGridViewFileItem):

    def _on_released(self, gesture_click, *args):
        self.emit("context-menu-called")

    def _on_setup(self):
        gesture_click = Gtk.GestureClick(button=3)
        gesture_click.connect("released", self._on_released)
        self._overlay = DeltaOverlay(self)
        self._overlay.add_controller(gesture_click)
