# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import GLib


class FoxtrotHeaderFactory(Gtk.SignalListItemFactory):

    def _on_setup(self, factory, list_header):
        label = Gtk.Label()
        if list_header.get_child() is None:
            list_header.set_child(label)

    def _get_label_text(self, file_info):
        recent_modified = file_info.get_attribute_int64("recent::modified")
        if recent_modified is None:
            print("if you see this, something goes wrong")
            return ""
        date_time = GLib.DateTime.new_from_unix_local(recent_modified)
        return date_time.format("%x")

    def _on_bind(self, factory, list_header):
        file_info = list_header.get_item()
        if file_info is None:
            return
        text = self._get_label_text(file_info)
        label = list_header.get_child()
        label.set_label(text)

    def __init__(self):
        Gtk.SignalListItemFactory.__init__(self)
        self.connect("setup", self._on_setup)
        self.connect("bind", self._on_bind)
