# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files.alfa.ListViewFileItem import AlfaListViewFileItem
from .widgets.Widgets import EchoWidgets


class DeltaItem(AlfaListViewFileItem):

    def _on_setup(self):
        EchoWidgets(self)
