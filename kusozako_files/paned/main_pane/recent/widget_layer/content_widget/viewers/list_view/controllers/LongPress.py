# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako_files.const import FileManagerSignals
from kusozako_files.const import MainWindowSignals


class DeltaLongPress(Gtk.GestureLongPress, DeltaEntity):

    def _call_long_press_cancel_hook(self):
        user_data = FileManagerSignals.LONG_PRESS_CANCEL_HOOK, None
        self._raise("delta > file manager signal", user_data)

    def _on_event(self, *args):
        self._call_long_press_cancel_hook()
        viewer_model = self._enquiry("delta > viewer model")
        file_info = viewer_model.get_selected_item()
        user_data = MainWindowSignals.SHOW_FILE_PROPERTY, file_info
        self._raise("delta > main window signal", user_data)

    def __init__(self, parent):
        self._parent = parent
        Gtk.GestureLongPress.__init__(self)
        self.connect("pressed", self._on_event)
        self._raise("delta > add controller", self)
