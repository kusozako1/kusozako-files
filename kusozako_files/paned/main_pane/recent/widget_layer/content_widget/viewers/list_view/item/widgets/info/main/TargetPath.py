# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Gio
from gi.repository import Pango
from kusozako1.util import HomeDirectory
from kusozako1.Entity import DeltaEntity
from kusozako_files.const import FileItemSignals


class DeltaTargetPath(Gtk.Label, DeltaEntity):

    def _bind(self, file_info):
        target_uri = file_info.get_attribute_string("standard::target-uri")
        gfile = Gio.File.new_for_uri(target_uri)
        path = gfile.get_path()
        shorten_path = HomeDirectory.shorten(path)
        text = "Path : {}".format(shorten_path)
        self.set_label(text)

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal != FileItemSignals.BINDED:
            return
        _, file_info, _ = param
        self._bind(file_info)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(
            self,
            ellipsize=Pango.EllipsizeMode.MIDDLE,
            xalign=0,
            )
        self.add_css_class("dim-label")
        self.add_css_class("caption")
        self._raise("delta > register file item object", self)
        self._raise("delta > add to container", self)
