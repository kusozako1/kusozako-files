# (c) copyright 2022-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files.alfa.page_settings.PageSettings import AlfaPageSettings


class DeltaPageSettings(AlfaPageSettings):

    __own_group__ = "view.recent"
