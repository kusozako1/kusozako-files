# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako_files.const import FileManagerSignals


class DeltaKeyword(Gtk.CustomFilter, DeltaEntity):

    def _match_func(self, file_info, user_data=None):
        if not self._keyword:
            return True
        return self._keyword in file_info.get_display_name()

    def receive_transmission(self, user_data):
        signal, keyword = user_data
        if signal != FileManagerSignals.FILTER_KEYWORD_CHANGED:
            return
        self._keyword = keyword
        self.changed(Gtk.FilterChange.DIFFERENT)

    def __init__(self, parent):
        self._parent = parent
        self._keyword = ""
        Gtk.CustomFilter.__init__(self)
        self.set_filter_func(self._match_func, None)
        self._raise("delta > add filter", self)
        self._raise("delta > register file manager object", self)
