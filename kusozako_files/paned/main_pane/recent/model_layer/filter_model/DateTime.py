# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import GLib
from kusozako1.Entity import DeltaEntity


class DeltaDateTime(Gtk.CustomFilter, DeltaEntity):

    def _match_func(self, file_info, user_data=None):
        now = GLib.DateTime.new_now_local()
        modified_int64 = file_info.get_attribute_int64("recent::modified")
        file_time = GLib.DateTime.new_from_unix_local(modified_int64)
        time_difference = now.difference(file_time)
        if GLib.TIME_SPAN_DAY*7 > time_difference:
            return True
        return False

    def __init__(self, parent):
        self._parent = parent
        self._keyword = ""
        Gtk.CustomFilter.__init__(self)
        self.set_filter_func(self._match_func, None)
        self._raise("delta > add filter", self)
