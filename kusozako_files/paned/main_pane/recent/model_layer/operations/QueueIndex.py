# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from kusozako1.const import MainWindowSignals
from kusozako_files.const import FileManagerSignals
from kusozako_files.const import ExtraMenuPages
from kusozako_files.alfa.Observer import AlfaObserver


class DeltaQueueIndex(AlfaObserver):

    REGISTRATION = "delta > register file manager object"
    SIGNAL = FileManagerSignals.QUEUE_INDEX

    def _queue_directory(self, gfile):
        user_data = FileManagerSignals.MOVE_DIRECTORY, gfile
        self._raise("delta > file manager signal", user_data)

    def _queue_file(self, gfile):
        param = ExtraMenuPages.OPEN_FILE_STANDALONE, gfile
        user_data = MainWindowSignals.SHOW_EXTRA_PRIMARY_MENU_WITH_PARAM, param
        self._raise("delta > main window signal", user_data)

    def _dispatch(self, selection_model, index, file_info):
        target_uri = file_info.get_attribute_string("standard::target-uri")
        gfile = Gio.File.new_for_uri(target_uri)
        if file_info.get_file_type() == Gio.FileType.DIRECTORY:
            self._queue_directory(gfile)
        else:
            selection_model.select_item(index, False)
            self._queue_file(gfile)

    def _on_received(self, index):
        selection_model = self._enquiry("delta > selection model")
        file_info = selection_model[index]
        self._dispatch(selection_model, index, file_info)
