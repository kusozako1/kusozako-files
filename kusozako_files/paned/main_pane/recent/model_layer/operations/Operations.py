# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .UnselectAll import DeltaUnselectAll
from .SelectAll import DeltaSelectAll
from .ToggleSelected import DeltaToggleSelected
from .ToggleIndex import DeltaToggleIndex
from .QueueIndex import DeltaQueueIndex
from .ActivationHook import DeltaActivationHook
from .ContextMenuHook import DeltaContextMenuHook
from .show_file_context_menu.ShowFileContextMenu import (
    DeltaShowFileContextMenu
    )
from .show_directory_context_menu.ShowDirectoryContextMenu import (
    DeltaShowDirectoryContextMenu
    )


class EchoOperations:

    def __init__(self, parent):
        DeltaUnselectAll(parent)
        DeltaSelectAll(parent)
        DeltaToggleSelected(parent)
        DeltaToggleIndex(parent)
        DeltaQueueIndex(parent)
        DeltaActivationHook(parent)
        DeltaContextMenuHook(parent)
        DeltaShowFileContextMenu(parent)
        DeltaShowDirectoryContextMenu(parent)
