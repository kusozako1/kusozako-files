# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files.const import FileManagerSignals
from kusozako_files.alfa.Observer import AlfaObserver
from kusozako_files.const import MainWindowSignals


class DeltaLongPressCancelHook(AlfaObserver):

    REGISTRATION = "delta > register file manager object"
    SIGNAL = FileManagerSignals.LONG_PRESS_CANCEL_HOOK

    def _on_received(self, param=None):
        self._has_hook = True

    def _on_initialize(self):
        self._has_hook = False

    def try_release_hook(self):
        if not self._has_hook:
            return False
        self._has_hook = False
        user_data = MainWindowSignals.BACK_TO_BOOKMARK, None
        self._raise("delta > main window signal", user_data)
        return True
