# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .multi_sorter.MultiSorter import DeltaMultiSorter
from .SectionSorter import DeltaSectionSorter


class DeltaSortModel(Gtk.SortListModel, DeltaEntity):

    @classmethod
    def new(cls, parent, parent_model):
        instance = cls(parent)
        instance.construct(parent_model)
        return instance

    def construct(self, parent_model):
        self.set_model(parent_model)

    def __init__(self, parent):
        self._parent = parent
        Gtk.SortListModel.__init__(
            self,
            sorter=DeltaMultiSorter(self),
            section_sorter=DeltaSectionSorter(self),
            incremental=False,
            )
