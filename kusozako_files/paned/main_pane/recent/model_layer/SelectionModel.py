# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files.alfa.SelectionModel import AlfaSelectionModel


class DeltaSelectionModel(AlfaSelectionModel):

    def enumerate_selection(self):
        selection = self.get_selection()
        for nth in range(0, selection.get_size()):
            index = selection.get_nth(nth)
            yield self[index]

    def get_valid_selection_size(self):
        selection = self.get_selection()
        return selection.get_size()
