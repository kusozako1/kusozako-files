# (c) copyright 2022-2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .left_pane.LeftPane import DeltaLeftPane
from .main_pane.MainPane import DeltaMainPane

DEFAULT_POSITION = 184


class DeltaPaned(Gtk.Paned, DeltaEntity):

    def _on_notify(self, paned, param_spec):
        settings = "view", "paned_position", self.get_position()
        self._raise("delta > settings", settings)

    def _delta_call_add_to_container_start(self, widget):
        self.set_start_child(widget)

    def _delta_call_add_to_container_end(self, widget):
        self.set_end_child(widget)
        query = "view", "paned_position", DEFAULT_POSITION
        settings = self._enquiry("delta > settings", query)
        self.set_position(settings)
        self.connect("notify::position", self._on_notify)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Paned.__init__(
            self,
            orientation=Gtk.Orientation.HORIZONTAL,
            hexpand=True,
            vexpand=True,
            wide_handle=True,
            resize_start_child=False,
            )
        DeltaLeftPane(self)
        DeltaMainPane(self)
        self._raise("delta > add to container", self)
