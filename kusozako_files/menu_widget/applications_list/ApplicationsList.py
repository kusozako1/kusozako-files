# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from .model.Model import DeltaModel
from .list_view.ListView import DeltaListView


class AlfaApplicationsList(DeltaEntity):

    TARGET_PAGE = "define-target-page-here."

    def _delta_info_target_page(self):
        return self.TARGET_PAGE

    def _delta_info_selection_model(self):
        return self._model.get_selection_model()

    def _delta_info_current_gfile(self):
        return self._model.get_current_gfile()

    def __init__(self, parent):
        self._parent = parent
        self._model = DeltaModel(self)
        DeltaListView(self)
