# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .ProxySetter import DeltaProxySetter


class DeltaModel(Gio.ListStore, DeltaEntity):

    def _set_gfile(self, proxy):
        if isinstance(proxy, Gio.File):
            self._gfile = proxy
        else:
            self._gfile = proxy.get_selected_gfile()

    def _delta_call_proxy_changed(self, proxy):
        self.remove_all()
        self._set_gfile(proxy)
        file_info = self._gfile.query_info("*", 0)
        content_type = file_info.get_content_type()
        for desktop_app_info in Gio.AppInfo.get_all_for_type(content_type):
            self.append(desktop_app_info)

    def get_selection_model(self):
        return Gtk.SingleSelection.new(self)

    def get_current_gfile(self):
        return self._gfile

    def __init__(self, parent):
        self._parent = parent
        Gio.ListStore.__init__(self, item_type=Gio.DesktopAppInfo)
        DeltaProxySetter(self)
