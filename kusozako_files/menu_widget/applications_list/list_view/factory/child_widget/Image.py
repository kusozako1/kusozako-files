# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity


class DeltaImage(Gtk.Image, DeltaEntity):

    def bind(self, desktop_app_info):
        gicon = desktop_app_info.get_icon()
        if gicon is not None:
            self.set_from_gicon(gicon)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Image.__init__(self, pixel_size=48)
        self._raise("delta > add to container", self)
