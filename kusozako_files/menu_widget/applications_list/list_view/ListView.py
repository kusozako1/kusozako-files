# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.const import MainWindowSignals
from kusozako1.Entity import DeltaEntity
from .factory.Factory import DeltaFactory


class DeltaListView(Gtk.ListView, DeltaEntity):

    def _on_activate(self, list_view, index):
        model = list_view.get_model()
        desktop_app_info = model[index]
        gfile = self._enquiry("delta > current gfile")
        desktop_app_info.launch([gfile], None)
        user_data = MainWindowSignals.CLOSE_OVERLAY, None
        self._raise("delta > main window signal", user_data)

    def __init__(self, parent):
        self._parent = parent
        scrolled_window = Gtk.ScrolledWindow(hexpand=True, vexpand=True)
        Gtk.ListView.__init__(
            self,
            model=self._enquiry("delta > selection model"),
            factory=DeltaFactory(self),
            single_click_activate=True,
            )
        self.add_css_class("kusozako-osd")
        self.connect("activate", self._on_activate)
        scrolled_window.set_child(self)
        self._raise("delta > add to container", scrolled_window)
