# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.overlay_item.button.Button import AlfaButton


class DeltaSelectAll(AlfaButton):

    LABEL = _("Select All")
    SHORTCUT = _("Ctrl+A")

    def _on_clicked(self, button):
        model, _ = self._enquiry("delta > selection model")
        model.select_all()
