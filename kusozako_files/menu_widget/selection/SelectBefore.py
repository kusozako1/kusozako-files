# (c) copyright 2022-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.overlay_item.button.Button import AlfaButton


class DeltaSelectBefore(AlfaButton):

    LABEL = _("Select Before")

    def _on_clicked(self, button):
        model, index = self._enquiry("delta > selection model")
        model.select_range(0, index+1, False)
