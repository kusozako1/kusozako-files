# (c) copyright 2022-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.overlay_item.button.Button import AlfaButton


class DeltaUnselectAfter(AlfaButton):

    LABEL = _("Unselect After")

    def _on_clicked(self, button):
        model, index = self._enquiry("delta > selection model")
        length = len(model)
        model.unselect_range(index, length-index)
