# (c) copyright 2022-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.overlay_item.button.Button import AlfaButton


class DeltaUnselectBefore(AlfaButton):

    LABEL = _("Unselect Before")

    def _on_clicked(self, button):
        model, index = self._enquiry("delta > selection model")
        model.unselect_range(0, index+1)
