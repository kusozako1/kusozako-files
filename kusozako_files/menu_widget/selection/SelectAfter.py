# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.overlay_item.button.Button import AlfaButton


class DeltaSelectAfter(AlfaButton):

    LABEL = _("Select After")

    def _on_clicked(self, button):
        model, index = self._enquiry("delta > selection model")
        length = len(model)
        model.select_range(index, length-index, False)
