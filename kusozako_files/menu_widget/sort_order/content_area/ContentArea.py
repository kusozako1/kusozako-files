# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.widget.overlay_item.Separator import DeltaSeparator
from .ExitButton import DeltaExitButton
from .data_items.DataItems import DeltaDataItems


class DeltaContentArea(Gtk.Box, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.append(widget)

    def __init__(self, parent):
        self._parent = parent
        self._settings_proxy = None
        scrolled_window = Gtk.ScrolledWindow(propagate_natural_width=True)
        Gtk.Box.__init__(
            self,
            orientation=Gtk.Orientation.VERTICAL,
            hexpand=False,
            )
        self.set_size_request(320, -1)
        self.add_css_class("osd")
        DeltaExitButton(self)
        DeltaSeparator(self)
        DeltaDataItems(self)
        scrolled_window.set_child(self)
        self._raise("delta > add to container", scrolled_window)
