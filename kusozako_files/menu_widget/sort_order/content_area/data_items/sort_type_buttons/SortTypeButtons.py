# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .FileName import DeltaFileName
from .MimeType import DeltaMimeType
from .LastModified import DeltaLastModified


class EchoSortTypeButtons:

    @classmethod
    def new_for_proxy(cls, parent, proxy):
        DeltaFileName.new_for_proxy(parent, proxy)
        DeltaMimeType.new_for_proxy(parent, proxy)
        DeltaLastModified.new_for_proxy(parent, proxy)
