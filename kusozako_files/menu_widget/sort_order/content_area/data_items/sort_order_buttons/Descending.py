# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files.const import SortOrder
from .SortOrderButton import AlfaSortOrderButton


class DeltaDescending(AlfaSortOrderButton):

    LABEL = _("Descending")
    MATCH = SortOrder.DESCENDING
