# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.widget.overlay_item.Separator import DeltaSeparator
from .ShowExtraOverlay import DeltaShowExtraOverlay
from .sort_type_buttons.SortTypeButtons import EchoSortTypeButtons
from .sort_order_buttons.SortOrderButtons import EchoSortOrderButtons


class DeltaDataItems(DeltaEntity):

    def _delta_call_reset(self, proxy):
        for widget in self._widgets:
            self._box.remove(widget)
        self._widgets.clear()
        user_data = self, proxy
        self._raise("delta > loopback add extra sort types", user_data)
        EchoSortTypeButtons.new_for_proxy(self, proxy)
        DeltaSeparator(self)
        EchoSortOrderButtons.new_for_proxy(self, proxy)

    def _delta_call_add_to_container(self, widget):
        self._box.append(widget)
        self._widgets.append(widget)

    def __init__(self, parent):
        self._parent = parent
        self._widgets = []
        self._box = Gtk.Box(
            orientation=Gtk.Orientation.VERTICAL,
            hexpand=True,
            )
        self._raise("delta > add to container", self._box)
        DeltaShowExtraOverlay(self)
