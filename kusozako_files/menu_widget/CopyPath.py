# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gdk
from kusozako1.const import MainWindowSignals
from kusozako1.overlay_item.button.Button import AlfaButton


class DeltaCopyPath(AlfaButton):

    LABEL = _("Copy Path")

    def _on_clicked(self, button):
        gfile = self._enquiry("delta > selected gfile")
        path = gfile.get_path()
        display = Gdk.Display.get_default()
        clipboard = display.get_clipboard()
        clipboard.set(path)
        user_data = MainWindowSignals.CLOSE_OVERLAY, None
        self._raise("delta > main window signal", user_data)
