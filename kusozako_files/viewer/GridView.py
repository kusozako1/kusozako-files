# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako_files.const import ViewerTypes
from kusozako_files.viewer.viewer.Viewer import AlfaViewer


class AlfaGridView(AlfaViewer):

    __viewer_type__ = ViewerTypes.ICON_VIEW
    __css__ = "kusozako-content-area"

    def _get_viewer(self, factory):
        return Gtk.GridView(
            model=self._enquiry("delta > viewer model"),
            single_click_activate=True,
            factory=factory,
            max_columns=999,    # as infinity
            # orientation=Gtk.Orientation.HORIZONTAL,
            )
