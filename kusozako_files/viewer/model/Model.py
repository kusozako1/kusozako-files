# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako1.thumbnailer.Thumbnailer import FoxtrotThumbnailer
from kusozako_files.const import FileItemSignals
from .SelectionModelWatcher import DeltaSelectionModelWatcher


class DeltaModel(DeltaEntity):

    @classmethod
    def new(cls, parent, selection_model):
        instance = cls(parent)
        instance.construct(selection_model)
        return instance

    def _loaded(self, pixbuf, state):
        user_data = FileItemSignals.PIXBUF_LOADED, pixbuf
        self._raise("delta > file item signal", user_data)

    def bind(self, selected, file_info):
        gfile = file_info.get_attribute_object("standard::file")
        if not gfile.query_exists():
            print("binded-file not exists", gfile.get_path())
            return
        self._watcher.bind(selected, file_info)
        self._thumbnailer.load_from_file_info_async_2(file_info, self._loaded)
        param = selected, file_info, gfile
        user_data = FileItemSignals.BINDED, param
        self._raise("delta > file item signal", user_data)

    def unbind(self):
        self._watcher.unbind()

    def construct(self, selection_model):
        self._watcher = DeltaSelectionModelWatcher.new(self, selection_model)

    def __init__(self, parent):
        self._parent = parent
        self._thumbnailer = FoxtrotThumbnailer.get_default()
