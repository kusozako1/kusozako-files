# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako_files.const import MainWindowSignals


class DeltaQueryTooltip(DeltaEntity):

    def _close_file_property(self):
        user_data = MainWindowSignals.BACK_TO_BOOKMARK, None
        self._raise("delta > main window signal", user_data)

    def _show_file_info(self, child):
        index = child.get_index()
        model = self._enquiry("delta > model")
        file_info = model[index]
        user_data = MainWindowSignals.SHOW_FILE_PROPERTY, file_info
        self._raise("delta > main window signal", user_data)

    def _on_query_tooltip(self, viewer, x, y, mode, tooltip):
        viewer_model = self._enquiry("delta > viewer model")
        file_info = viewer_model.get_selected_item()
        tooltip.set_text(file_info.get_name())
        print(file_info.get_name())
        # user_data = MainWindowSignals.SHOW_FILE_PROPERTY, file_info
        # self._raise("delta > main window signal", user_data)
        return True

    def __init__(self, parent):
        self._parent = parent
        viewer = self._enquiry("delta > viewer")
        viewer.connect("query-tooltip", self._on_query_tooltip)
