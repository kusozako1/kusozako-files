# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako_files.const import PageSettingsKeys as Keys


class DeltaItemsChanged(DeltaEntity):

    def _is_focus_requested(self, viewer):
        viewer_type = self._enquiry("delta > page settings", Keys.VIEWER_TYPE)
        if viewer_type != viewer.__viewer_type__:
            return False
        if self._enquiry("delta > page settings", Keys.SHOW_SEARCH):
            return False
        return True

    def _focus(self):
        viewer = self._enquiry("delta > viewer")
        if not self._is_focus_requested(viewer):
            return
        root = viewer.get_root()
        if root is None:
            return
        root.set_focus(viewer)

    """
    def _timeout(self, index):
        if self._index != index:
            self._focus()
        return GLib.SOURCE_CONTINUE
    """

    def _on_event(self, model, *args):
        # self._index += 1
        # index = self._index
        self._focus()
        # GLib.timeout_add(150, self._timeout, index)

    def __init__(self, parent):
        self._parent = parent
        # self._index = 0
        model = self._enquiry("delta > viewer model")
        model.connect("items-changed", self._on_event)
