# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .VerticalAdjustment import DeltaVerticalAdjustment


class DeltaFactory(Gtk.SignalListItemFactory, DeltaEntity):

    def _on_setup(self, factory, list_item):
        self._vertical_adjustment.connect(list_item)
        list_item_widget = self._enquiry("delta > list item widget")
        list_item.set_child(list_item_widget)

    def _on_bind(self, factory, list_item):
        selection_model = self._enquiry("delta > selection model")
        item_index = list_item.get_position()
        selected = selection_model.is_selected(item_index)
        file_info = list_item.get_item()
        position = list_item.get_position()
        file_info.set_attribute_int32("kusozako1::position", position)
        list_item_widget = list_item.get_child()
        list_item_widget.bind(selected, file_info)

    def _on_unbind(self, factory, list_item):
        list_item_widget = list_item.get_child()
        list_item_widget.unbind()

    def _on_teardown(self, factory, list_item):
        pass
        # print("teardown")

    def __init__(self, parent):
        self._parent = parent
        self._vertical_adjustment = DeltaVerticalAdjustment(self)
        Gtk.SignalListItemFactory.__init__(self)
        self.connect("setup", self._on_setup)
        self.connect("bind", self._on_bind)
        self.connect("unbind", self._on_unbind)
        self.connect("teardown", self._on_teardown)
