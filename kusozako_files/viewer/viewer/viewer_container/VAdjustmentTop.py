# (c) copyright 2024-2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako_files.const import FileManagerSignals
from kusozako_files.const import WidgetSize


class DeltaVAdjustmentTop(DeltaEntity):

    def _on_value_changed(self, vadjustment):
        margin = max(0, WidgetSize.ACTION_BAR_HEIGHT-vadjustment.props.value)
        viewer = self._enquiry("delta > viewer")
        viewer.set_margin_top(margin)

    def receive_transmission(self, user_data):
        signal, _ = user_data
        if signal != FileManagerSignals.LOAD_FINISHED:
            return
        viewer = self._enquiry("delta > viewer")
        viewer.set_margin_top(WidgetSize.ACTION_BAR_HEIGHT)
        vadjustment = viewer.get_vadjustment()
        vadjustment.set_value(WidgetSize.ACTION_BAR_HEIGHT)

    def __init__(self, parent):
        self._parent = parent
        viewer = self._enquiry("delta > viewer")
        vadjustment = viewer.get_vadjustment()
        vadjustment.connect("value-changed", self._on_value_changed)
        self._raise("delta > register file manager object", self)
