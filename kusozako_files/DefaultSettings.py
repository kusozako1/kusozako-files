# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako_files.const import ViewerTypes
from kusozako_files.const import PageSettingsKeys
from kusozako_files.const import SortOrder
from kusozako_files.const import SortType

DEFAULT = (
    ("view", PageSettingsKeys.SHOW_HIDDEN, False),
    ("view", PageSettingsKeys.VIEWER_TYPE, ViewerTypes.ICON_VIEW),
    ("view", PageSettingsKeys.SORT_ORDER, SortOrder.ASCENDING),
    ("view", PageSettingsKeys.SHOW_SEARCH, False),
    ("view.filemanager", PageSettingsKeys.SORT_TYPE, SortType.FILE_NAME),
    ("view.recent", PageSettingsKeys.SORT_TYPE, SortType.LAST_ACCESSED),
    ("view.trashbin", PageSettingsKeys.SORT_TYPE, SortType.DELETION_DATE),
)


class DeltaDefaultSettigs(DeltaEntity):

    def __init__(self, parent):
        self._parent = parent
        for query in DEFAULT:
            _ = self._enquiry("delta > settings", query)
