# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib


class FoxtrotReadableTime:

    @classmethod
    def get_default(cls):
        if "_default_instance" not in dir(cls):
            cls._default_instance = cls()
        return cls._default_instance

    def _get_label_for_this_year(self, date_time):
        if date_time.get_day_of_year() != self._today.get_day_of_year():
            _, month, day = date_time.get_ymd()
            base_format = "{} / {} (%a)".format(month, day)
            return date_time.format(base_format)
        return date_time.format("%r")

    def get_label(self, date_time):
        if date_time.get_year() > self._today.get_year():
            year, month, day = date_time.get_ymd()
            base_format = "{} / {} / {} (%a)".format(year, month, day)
            return date_time.format(base_format)
        else:
            return self._get_label_for_this_year(date_time)

    def __init__(self):
        self._today = GLib.DateTime.new_now_local()
