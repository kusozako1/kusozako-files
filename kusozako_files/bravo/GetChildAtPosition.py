# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk


class BravoGetChildAtPosition:

    def _get_child_at_position(self, gesture, x, y):
        widget = gesture.get_widget()
        if isinstance(widget, Gtk.FlowBox):
            return widget.get_child_at_pos(x, y)
        if isinstance(widget, Gtk.ListBox):
            return widget.get_row_at_y(y)

    def _get_index_at_position(self, gesture, x, y):
        child = self._get_child_at_position(gesture, x, y)
        if child is None:
            return None, None
        index = child.get_index()
        model = self._enquiry("delta > model")
        return index, model

    def _get_index_force_select(self, gesture, x, y):
        child = self._get_child_at_position(gesture, x, y)
        if child is None:
            return None, None
        index = child.get_index()
        model = self._enquiry("delta > model")
        if not model.is_selected(index):
            model.select_item(index, False)
        return index, model
