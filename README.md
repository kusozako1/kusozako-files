# kusozako-files

file manager for linux

![](screenshot/screenshot_2.png)

![](screenshot/screenshot_3.png)

## License

This software is licensed under GPL v3 or any later version.

See LICENSE.md for more detail.
